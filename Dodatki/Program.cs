﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Dodatki
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            string path = Environment.CurrentDirectory + @"\" + "PdfSharp.dll";
            if ( File.Exists(path) )
            {
                Application.Run(new Base());
            }else
            {
                MessageBox.Show("Відсутній файл PdfSharp.dll!");
            }
        }
    }
}
