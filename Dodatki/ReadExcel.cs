﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows.Forms;

namespace Dodatki
{
    public partial class ReadExcel : Form
    {
        ReadFromExcel from;
        BackgroundWorker worker;

        public ReadExcel(object[,] paper1Value, object[,] paper2Value, int klass)
        {
            InitializeComponent();
            from = new ReadFromExcel(paper1Value, paper2Value, klass);
            worker = from.work;
            worker.RunWorkerCompleted += RunWorkerCompleted;
            from.Run();
        }
        void RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if ( e.Error != null )
            {
                MessageBox.Show(e.Error.Message);
            }
            else if ( e.Cancelled )
            {
                MessageBox.Show("Відмінено!");
            }
            else
            {
                Base.pupils = from.pupils;
                DialogResult = DialogResult.OK;
            }
        }
        private void Stop_Click(object sender, EventArgs e)
        {
            from.Stop();
            DialogResult = DialogResult.Abort;
        }
    }

    class ReadFromExcel
    {
        public List<Pupil> pupils;
        public BackgroundWorker work;
        int rows, cols, klass;
        object[,] range = null;
        object[,] extraRange;
        SortedList<int, string> subjects;
        public ReadFromExcel(object[,] rng, object[,] range, int klass)
        {
            rows = range.GetLength(0);
            cols = range.GetLength(1);
            this.range = range;
            this.klass = klass;
            extraRange = rng;
            subjects = new SortedList<int, string>();
            pupils = new List<Pupil>();
            work = new BackgroundWorker()
            {
                WorkerSupportsCancellation = true,
                WorkerReportsProgress = false
            };
            work.DoWork += new DoWorkEventHandler(Read);

        }
        public void Run()
        {
            try
            {
                work.RunWorkerAsync();
            }
            catch (InvalidOperationException e)
            {
                ErrorMessage.LogError(e);
                MessageBox.Show(e.Message, "Помилка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        public void Stop()
        {
            Base.pupils.Clear();
            work.CancelAsync();
        }
        private void Read(object sender, DoWorkEventArgs e)
        {
            System.Threading.Thread.Sleep(1000);
            try
            {
                if (IsFill(out string str, 2, 2, extraRange))
                {
                    double d = double.Parse(str);
                    Pupil.date = DateTime.FromOADate(d);
                }
                if (IsFill(out str, 2, 3, extraRange))
                {
                    Pupil.year = str;
                }
                if (IsFill(out str, 2, 4, extraRange))
                {
                    Pupil.director = str;
                }
                if (IsFill(out str, 2, 5, extraRange))
                {
                    Pupil.firstSt = str;
                }
                if (IsFill(out str, 2, 6, extraRange))
                {
                    Pupil.secondSt = str;
                }
                if (IsFill(out str, 2, 7, extraRange))
                {
                    Pupil.thirdSt = str;
                }
                ReadSubjects();
                int row, col;
                row = 4;
                for (; row <= rows; ++row)
                {
                    Pupil pupil = new Pupil();
                    col = 1;
                    if (!ReadFirst(ref col, row, ref pupil))
                    {
                        if (pupil.FName == null)
                        {
                            break;
                        }
                        else
                        {
                            throw new Exception();
                        }
                    }
                    ReadSubject(ref col, row, ref pupil);
                    col++;
                    ReadDpa(ref col, row, ref pupil);
                    if (IsFill(out str, ++col, row, range))
                    {
                        if ( str.Contains(".") )
                        {
                            str = str.Replace(".", ",");
                        }
                        if (!str.Contains(","))
                        {
                            str += ",0";
                        }
                        pupil.AvScore = str;
                    }
                    col++;
                    ReadFacult(ref col, row, ref pupil);
                    if (IsFill(out str, col++, row, range))
                    {
                        pupil.Honor = str;
                    }
                    if (IsFill(out str, col++, row, range))
                    {
                        pupil.Study = str;
                    }
                    if (IsFill(out str, col++, row, range))
                    {
                       pupil.Medal = str;
                    }
                    if (IsFill(out str, col, row, range))
                    {
                        pupil.HonorSubject = str;
                    }
                    pupils.Add(pupil);
                }
            }
            catch (Exception ex)
            {
				ErrorMessage.LogError(ex);
                MessageBox.Show(ex.Message, "Помилка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private bool ReadFirst(ref int col, int row, ref Pupil pupil)
        {
            bool flag = true;
            try
            {
                //
                if (!IsFill(out string index, col++, row, range))
                {
                    return false;
                }
                //
                if (!IsFill(out string name, col++, row, range))
                {
                    return false;
                }
                string[] t = name.Split(' ');
                pupil.FName = t[0];
                pupil.SName = t[1];
                pupil.LName = t[2];
                //
                if (!IsFill(out string sex, col++, row, range))
                {
                    return false;
                }
                if (!IsFill(out string series, col++, row, range))
                {
                    return false;
                }
                if (!IsFill(out string number, col++, row, range))
                {
                    return false;
                }
                pupil.IndexNumber = index;
                pupil.Sex = sex;
                pupil.SSertif = series;
                pupil.NumSertif = number;
            }
            catch (IndexOutOfRangeException)
            {
                flag = false;
            }
            return flag;
        }
        private void ReadSubject(ref int col, int row, ref Pupil pupil)
        {
            if (subjects.Count == 0)
            {
                throw new Exception();
            }
            while (true)
            {
                if (IsFill(out string mark, col, row, range))
                {
                    if (mark.Equals("*"))
                    {
                        break;
                    }
                    if (IsNumber(mark))
                    {
                        pupil.AddToSubjects(new KeyValuePair<string, string>(subjects[col], mark));
                    }
                    else
                    {
                        string subject = subjects[col];
                        switch (mark)
                        {
                            case "НА":
                                pupil.AddToSubjects(new KeyValuePair<string, string>(subject, "не атестован" + Pupil.WithSex(pupil.Sex, 2)));
                                break;
                            case "зар":
                                pupil.AddToSubjects(new KeyValuePair<string, string>(subject, "зараховано"));
                                break;
                            case "зв":
                                pupil.AddToSubjects(new KeyValuePair<string, string>(subject, "звільнен" + Pupil.WithSex(pupil.Sex, 2)));
                                break;
                            default:
                                throw new Exception();
                        }
                    }
                }
                col++;
            }
        }
        private void ReadDpa(ref int col, int row, ref Pupil pupil)
        {
            while (true)
            {
                if (IsFill(out string mark, col, row, range))
                {
                    if (mark.Equals("*"))
                    {
                        break;
                    }
                    if (IsNumber(mark))
                    {
                        pupil.AddToDpa(new KeyValuePair<string, string>(subjects[col], mark));
                    }
                    else
                    {
                        if (mark.Contains('*'))
                        {
                            string[] arg = mark.Split('*');
                            foreach (string s in arg)
                            {
                                pupil.AddToDpa(new KeyValuePair<string, string>(s, ""));
                            }
                        }
                        else
                        {
                            string subject = subjects[col];
                            pupil.AddToDpa(new KeyValuePair<string, string>(mark, ""));
                        }
                    }
                }
                col++;
            }
        }
        private void ReadFacult(ref int col, int row, ref Pupil pupil)
        {
            int i = 0;
            while (i < 5)
            {
                if (IsFill(out string str, col, row, range))
                {
                    pupil.AddToOptionals(str);
                }
                i++;
                col++;
            }
        }
        private void ReadSubjects()
        {
            const int cRow = 3;
            int cCol = 6;
            string subject = null;

            try
            {
                while (true)
                {
                    if (IsFill(out subject, cCol, cRow, range))
                    {
                        if (subject.Equals("*"))
                        {
                            break;
                        }
                        subjects.Add(cCol, subject);
                    }
                    cCol++;
                }
                cCol++;
                while (true)
                {
                    if (IsFill(out subject, cCol, cRow, range))
                    {
                        if (subject.Equals("*"))
                        {
                            break;
                        }
                        subjects.Add(cCol, subject);
                    }
                    cCol++;
                }
            }
            catch (Exception exc)
            {
                throw exc;
            }
        }
        private bool IsNumber(string str)
        {
            bool isNum = int.TryParse(str, out int i);
            if (isNum)
            {
                if (i >= 1 && i <= 12)
                {
                    return true;
                }
            }
            return false;
        }
        private bool IsFill(out string s, int col, int row, object[,] range)
        {
            if (range[row, col] != null)
            {
                string str = range[row, col].ToString();
                if (string.IsNullOrWhiteSpace(str))
                {
                    s = null;
                    return false;
                }
                else
                {
                    s = str;
                    return true;
                }
            }
            s = null;
            return false;
        }
    }
}


