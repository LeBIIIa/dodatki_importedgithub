﻿namespace Dodatki
{
    partial class CommonInfo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CommonInfo));
            this.dateString = new System.Windows.Forms.Label();
            this.date = new System.Windows.Forms.DateTimePicker();
            this.yearString = new System.Windows.Forms.Label();
            this.year = new System.Windows.Forms.TextBox();
            this.directorString = new System.Windows.Forms.Label();
            this.director = new System.Windows.Forms.TextBox();
            this.groupSt = new System.Windows.Forms.GroupBox();
            this.thirdSt = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.secondSt = new System.Windows.Forms.TextBox();
            this.secondStString = new System.Windows.Forms.Label();
            this.firstSt = new System.Windows.Forms.TextBox();
            this.firstStString = new System.Windows.Forms.Label();
            this.nameString = new System.Windows.Forms.Label();
            this.cancel = new System.Windows.Forms.Button();
            this.OK = new System.Windows.Forms.Button();
            this.groupSt.SuspendLayout();
            this.SuspendLayout();
            // 
            // dateString
            // 
            this.dateString.AutoSize = true;
            this.dateString.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.dateString.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.dateString.Location = new System.Drawing.Point(142, 7);
            this.dateString.Name = "dateString";
            this.dateString.Size = new System.Drawing.Size(140, 15);
            this.dateString.TabIndex = 0;
            this.dateString.Text = "Дата видачі документу";
            // 
            // date
            // 
            this.date.CustomFormat = "";
            this.date.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.date.Location = new System.Drawing.Point(170, 25);
            this.date.MinDate = new System.DateTime(2000, 1, 1, 0, 0, 0, 0);
            this.date.Name = "date";
            this.date.Size = new System.Drawing.Size(92, 20);
            this.date.TabIndex = 2;
            this.date.ValueChanged += new System.EventHandler(this.Date_ValueChanged);
            // 
            // yearString
            // 
            this.yearString.AutoSize = true;
            this.yearString.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.yearString.ForeColor = System.Drawing.Color.Black;
            this.yearString.Location = new System.Drawing.Point(25, 52);
            this.yearString.Name = "yearString";
            this.yearString.Size = new System.Drawing.Size(272, 15);
            this.yearString.TabIndex = 3;
            this.yearString.Text = "Рік випуску(якщо відрізняється від року видачі)";
            // 
            // year
            // 
            this.year.Location = new System.Drawing.Point(309, 51);
            this.year.Name = "year";
            this.year.Size = new System.Drawing.Size(70, 20);
            this.year.TabIndex = 4;
            // 
            // directorString
            // 
            this.directorString.AutoSize = true;
            this.directorString.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.directorString.ForeColor = System.Drawing.Color.Black;
            this.directorString.Location = new System.Drawing.Point(63, 79);
            this.directorString.Name = "directorString";
            this.directorString.Size = new System.Drawing.Size(64, 15);
            this.directorString.TabIndex = 5;
            this.directorString.Text = "Директор:";
            // 
            // director
            // 
            this.director.Location = new System.Drawing.Point(136, 78);
            this.director.Name = "director";
            this.director.Size = new System.Drawing.Size(174, 20);
            this.director.TabIndex = 6;
            // 
            // groupSt
            // 
            this.groupSt.Controls.Add(this.thirdSt);
            this.groupSt.Controls.Add(this.label1);
            this.groupSt.Controls.Add(this.secondSt);
            this.groupSt.Controls.Add(this.secondStString);
            this.groupSt.Controls.Add(this.firstSt);
            this.groupSt.Controls.Add(this.firstStString);
            this.groupSt.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupSt.Location = new System.Drawing.Point(28, 128);
            this.groupSt.Name = "groupSt";
            this.groupSt.Size = new System.Drawing.Size(351, 144);
            this.groupSt.TabIndex = 7;
            this.groupSt.TabStop = false;
            this.groupSt.Text = "9/11 клас";
            // 
            // thirdSt
            // 
            this.thirdSt.Location = new System.Drawing.Point(6, 118);
            this.thirdSt.Name = "thirdSt";
            this.thirdSt.Size = new System.Drawing.Size(339, 21);
            this.thirdSt.TabIndex = 12;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(3, 100);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(330, 15);
            this.label1.TabIndex = 13;
            this.label1.Text = "Третій рядок назви навчального закладу(не обов\'язково)";
            // 
            // secondSt
            // 
            this.secondSt.Location = new System.Drawing.Point(6, 76);
            this.secondSt.Name = "secondSt";
            this.secondSt.Size = new System.Drawing.Size(339, 21);
            this.secondSt.TabIndex = 10;
            // 
            // secondStString
            // 
            this.secondStString.AutoSize = true;
            this.secondStString.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.secondStString.ForeColor = System.Drawing.Color.Black;
            this.secondStString.Location = new System.Drawing.Point(3, 58);
            this.secondStString.Name = "secondStString";
            this.secondStString.Size = new System.Drawing.Size(240, 15);
            this.secondStString.TabIndex = 11;
            this.secondStString.Text = "Другий рядок назви навчального закладу";
            // 
            // firstSt
            // 
            this.firstSt.Location = new System.Drawing.Point(6, 35);
            this.firstSt.Name = "firstSt";
            this.firstSt.Size = new System.Drawing.Size(339, 21);
            this.firstSt.TabIndex = 9;
            // 
            // firstStString
            // 
            this.firstStString.AutoSize = true;
            this.firstStString.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.firstStString.ForeColor = System.Drawing.Color.Black;
            this.firstStString.Location = new System.Drawing.Point(3, 17);
            this.firstStString.Name = "firstStString";
            this.firstStString.Size = new System.Drawing.Size(248, 15);
            this.firstStString.TabIndex = 9;
            this.firstStString.Text = "Перший рядок назви навчального закладу";
            // 
            // nameString
            // 
            this.nameString.AutoSize = true;
            this.nameString.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.nameString.ForeColor = System.Drawing.Color.Black;
            this.nameString.Location = new System.Drawing.Point(124, 110);
            this.nameString.Name = "nameString";
            this.nameString.Size = new System.Drawing.Size(175, 15);
            this.nameString.TabIndex = 8;
            this.nameString.Text = "Назва навчального закладу:";
            // 
            // cancel
            // 
            this.cancel.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.cancel.Location = new System.Drawing.Point(208, 273);
            this.cancel.Name = "cancel";
            this.cancel.Size = new System.Drawing.Size(95, 23);
            this.cancel.TabIndex = 17;
            this.cancel.Text = "Скасувати";
            this.cancel.UseVisualStyleBackColor = true;
            this.cancel.Click += new System.EventHandler(this.Cancel_Click);
            // 
            // OK
            // 
            this.OK.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.OK.Location = new System.Drawing.Point(127, 273);
            this.OK.Name = "OK";
            this.OK.Size = new System.Drawing.Size(75, 23);
            this.OK.TabIndex = 18;
            this.OK.Text = "OK";
            this.OK.UseVisualStyleBackColor = true;
            this.OK.Click += new System.EventHandler(this.OK_Click);
            // 
            // CommonInfo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(409, 305);
            this.Controls.Add(this.cancel);
            this.Controls.Add(this.OK);
            this.Controls.Add(this.nameString);
            this.Controls.Add(this.groupSt);
            this.Controls.Add(this.director);
            this.Controls.Add(this.directorString);
            this.Controls.Add(this.year);
            this.Controls.Add(this.yearString);
            this.Controls.Add(this.date);
            this.Controls.Add(this.dateString);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "CommonInfo";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Спільні дані";
            this.groupSt.ResumeLayout(false);
            this.groupSt.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label dateString;
        private System.Windows.Forms.DateTimePicker date;
        private System.Windows.Forms.Label yearString;
        private System.Windows.Forms.TextBox year;
        private System.Windows.Forms.Label directorString;
        private System.Windows.Forms.TextBox director;
        private System.Windows.Forms.GroupBox groupSt;
        private System.Windows.Forms.TextBox secondSt;
        private System.Windows.Forms.Label secondStString;
        private System.Windows.Forms.TextBox firstSt;
        private System.Windows.Forms.Label firstStString;
        private System.Windows.Forms.Label nameString;
        private System.Windows.Forms.Button cancel;
        private System.Windows.Forms.Button OK;
        private System.Windows.Forms.TextBox thirdSt;
        private System.Windows.Forms.Label label1;
    }
}