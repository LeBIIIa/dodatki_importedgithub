﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace Dodatki
{
    public partial class SettingsModel : Form
    {
        private OpenFileDialog openFileDialog1;
        private SaveFileDialog saveFileDialog1;
        private Base @base;
        private List<Label> labels = new List<Label>();
        private bool canModify;

        public SettingsModel(Base @base)
        {
            this.@base = @base;
            InitializeComponent();
            Fill();
            if (CommonData.klass == 9)
            {
                tabControl2.SelectedIndex = 0;
                tabControl3.SelectedIndex = 0;
                (tabControl2.TabPages[1] as Control).Enabled = false;
                (tabControl3.TabPages[1] as Control).Enabled = false;
            }
            else if (CommonData.klass == 11)
            {
                tabControl2.SelectedIndex = 1;
                tabControl3.SelectedIndex = 1;
                (tabControl2.TabPages[0] as Control).Enabled = false;
                (tabControl3.TabPages[0] as Control).Enabled = false;
            }
            var all = GetAll(this, "selected");
            foreach (Label control in all)
            {
                labels.Add(control);
            }
            labels = labels.OrderBy(x => x.TabIndex).ToList();
            SelectedTabs();
        }
        private void TextBox1_Enter(object sender, EventArgs e)
        {
            ActiveControl = label1;
        }
        private void Open_Click(object sender, EventArgs e)
        {
            openFileDialog1 = new OpenFileDialog()
            {
                InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDoc‌​uments),
                Filter = "settings files (*.bls)|*.bls",
                Title = "Завантажити налаштування бланку",
                RestoreDirectory = true
            };
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    CommonData.ReadSettings(openFileDialog1.FileName);
                    Fill();
                    SelectedTabs();
                    @base.DrawOnBlank();
                }
                catch ( Exception ex )
                {
					ErrorMessage.LogError(ex);
                    MessageBox.Show(ex.Message, "Помилка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
        private void SaveAs_Click(object sender, EventArgs e)
        {
            saveFileDialog1 = new SaveFileDialog()
            {
                Filter = "settings files (*.bls)|*.bls",
                Title = "Зберегти налаштування бланку",
                RestoreDirectory = true
            };
            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    SetAll();
                    CommonData.WriteSettings(saveFileDialog1.FileName);
                }
                catch (Exception ex)
                {
					ErrorMessage.LogError(ex);
                    MessageBox.Show(ex.Message, "Помилка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
        private void Fill()
        {
            var all = GetAll(this, typeof(NumericUpDown));
            foreach ( NumericUpDown control in all)
            {
                control.Value = Convert.ToDecimal(CommonData.setting[control.Name]);
            }
            canModify = false;
            extraRightLine.Checked = (CommonData.extraRight) ? true : false;
            extraLeftLine.Checked = (CommonData.extraLeft) ? true : false;
            if ( CommonData.dividingLine )
            {
                DividingLine.Checked = true;
                DividingLineUp.Visible = true;
                DividingLabel.Visible = true;
                DividingLineSize.Visible = true;
                DividingLabel2.Visible = true;
                WidthOfLine.Visible = true;
                DividingLabel3.Visible = true;
            }
            canModify = true;
        }
        public IEnumerable<Control> GetAll(Control control, Type type)
        {
            var controls = control.Controls.Cast<Control>();

            return controls.SelectMany(ctrl => GetAll(ctrl, type))
                                      .Concat(controls)
                                      .Where(c => c.GetType() == type);
        }
        public IEnumerable<Control> GetAll(Control control, string name)
        {
            var controls = control.Controls.Cast<Control>();

            return controls.SelectMany(ctrl => GetAll(ctrl, name))
                                      .Concat(controls)
                                      .Where(c => c.Name.Contains(name));
        }
        private void ByDefault_Click(object sender, EventArgs e)
        {
            CommonData.SetDefault();
            CommonData.SetDefaultFont();
            Fill();
        }
        private void OK_Click(object sender, EventArgs e)
        {
            SetAll();
            DialogResult = DialogResult.OK;
            Close();
        }
        private void Cancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }
        private void Apply_Click(object sender, EventArgs e)
        {
            SetAll();
            @base.DrawOnBlank();
        }
        private void CheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (canModify == true)
            {
                CheckBox cb = sender as CheckBox;
                if (cb.Name.Contains("Left"))
                {
                    CommonData.extraLeft ^= true;
                }
                else if (cb.Name.Contains("Right"))
                {
                    CommonData.extraRight ^= true;
                }
                else if (cb.Name.Contains("Dividing"))
                {
                    CommonData.dividingLine ^= true;
                    DividingLineUp.Visible ^= true;
                    DividingLabel.Visible ^= true;
                    DividingLineSize.Visible ^= true;
                    DividingLabel2.Visible ^= true;
                    WidthOfLine.Visible ^= true;
                    DividingLabel3.Visible ^= true;
                }
            }
        }
        private void NumericUpDown_Enter(object sender, EventArgs e)
        {
            NumericUpDown num = (NumericUpDown)sender;
            num.Select(0, num.Text.Length);
        }
        private void NumericUpDown_MouseDown(object sender, MouseEventArgs e)
        {
            NumericUpDown num = (NumericUpDown)sender;
            num.Select(0, num.Text.Length);
        }
        private void Button_Click(object sender, EventArgs e)
        {
            if (sender is Button btn)
            {
                string index = btn.Text;
                FontDialog fontDialog = new FontDialog()
                {
                    Font = CommonData.ItemsFonts[index]
                };
                DialogResult result = fontDialog.ShowDialog();
                if (result == DialogResult.OK)
                {
                    CommonData.ItemsFonts[btn.Text] = fontDialog.Font;
                    SelectedTabs();
                }
            }
        }
        private void SelectedTabs()
        {
            int i = 0;
            foreach (KeyValuePair<string, Font> pair in CommonData.ItemsFonts)
            {
                labels[i].Font = pair.Value;
                labels[i].Text = "Вибраний шрифт";
                ++i;
            }
        }
        private void SetAll()
        {
            var all = GetAll(this, typeof(NumericUpDown));
            foreach (NumericUpDown control in all)
            {
                CommonData.setting[control.Name] = Convert.ToDouble(control.Value);
            }
            CommonData.extraLeft = extraLeftLine.Checked;
            CommonData.extraRight = extraRightLine.Checked;
            CommonData.dividingLine = DividingLine.Checked;
        }
    }
}
