﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Dodatki
{
    public partial class About : Form
    {
        public About()
        {
            InitializeComponent();
            Version.Text = "v" + Properties.Settings.Default.Version;
            CloseBtn.Location = new Point((Size.Width - CloseBtn.Size.Width) / 2, CloseBtn.Location.Y);
            ProgramName.Location = new Point((Size.Width - ProgramName.Size.Width) / 2, ProgramName.Location.Y);
            Graphics gfx = CreateGraphics();
            SizeF stringSize = gfx.MeasureString(ProgramName.Text, ProgramName.Font);
            Version.Location = new Point(ProgramName.Location.X + (int)stringSize.Width + Version.Padding.Left,Version.Location.Y);
        }
        private void LinkLabel_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            LinkLabel link = sender as LinkLabel;
            switch (link.Name)
            {
                case "Forum":
                    link.LinkVisited = true;
                    System.Diagnostics.Process.Start("http://druk-dodatkiv.forumotion.me");
                    break;
                case "Facebook":
                    link.LinkVisited = true;
                    System.Diagnostics.Process.Start("https://www.facebook.com/xakep.dead");
                    break;
                case "Version":
                    link.LinkVisited = true;
                    System.Diagnostics.Process.Start("https://drive.google.com/drive/folders/0B8ZYFa4IuTTwTUZHUkR3WElKNW8?usp=sharing");
                    break;
                default:
                    break;
            }
        }

        private void CloseBtn_Click(object sender, EventArgs e)
        {
            Close();
        }
        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            if (keyData == Keys.Escape)
            {
                Close();
            }
            return base.ProcessCmdKey(ref msg, keyData);
        }
    }
}
