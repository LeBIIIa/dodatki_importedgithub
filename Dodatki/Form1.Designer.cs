﻿namespace Dodatki
{
    partial class Base
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Base));
            this.settings = new System.Windows.Forms.Button();
            this.makeList = new System.Windows.Forms.Button();
            this.importList = new System.Windows.Forms.Button();
            this.listPupils = new System.Windows.Forms.DataGridView();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.commonInfo = new System.Windows.Forms.Button();
            this.showBlank = new System.Windows.Forms.CheckBox();
            this.turnBlank = new System.Windows.Forms.CheckBox();
            this.LoadALL_PDF = new System.Windows.Forms.Button();
            this.currentClass = new System.Windows.Forms.Button();
            this.LoadSel_PDF = new System.Windows.Forms.Button();
            this.blank = new PdfSharp.Forms.PagePreview();
            this.LoadALL_SeparatePDF = new System.Windows.Forms.Button();
            this.about = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.listPupils)).BeginInit();
            this.SuspendLayout();
            // 
            // settings
            // 
            this.settings.Location = new System.Drawing.Point(248, 12);
            this.settings.Name = "settings";
            this.settings.Size = new System.Drawing.Size(91, 50);
            this.settings.TabIndex = 4;
            this.settings.Text = "Налаштувати макет бланку";
            this.settings.UseVisualStyleBackColor = true;
            this.settings.Click += new System.EventHandler(this.Settings_Click);
            // 
            // makeList
            // 
            this.makeList.Location = new System.Drawing.Point(12, 12);
            this.makeList.Name = "makeList";
            this.makeList.Size = new System.Drawing.Size(65, 50);
            this.makeList.TabIndex = 7;
            this.makeList.Text = "Створити оціночну відомість";
            this.makeList.UseVisualStyleBackColor = true;
            this.makeList.Click += new System.EventHandler(this.MakeList_Click);
            // 
            // importList
            // 
            this.importList.Location = new System.Drawing.Point(83, 12);
            this.importList.Name = "importList";
            this.importList.Size = new System.Drawing.Size(79, 50);
            this.importList.TabIndex = 8;
            this.importList.Text = "Імпортувати оціночну відомість";
            this.importList.UseVisualStyleBackColor = true;
            this.importList.Click += new System.EventHandler(this.ImportList_Click);
            // 
            // listPupils
            // 
            this.listPupils.AllowUserToAddRows = false;
            this.listPupils.AllowUserToDeleteRows = false;
            this.listPupils.AllowUserToResizeColumns = false;
            this.listPupils.AllowUserToResizeRows = false;
            this.listPupils.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.Disable;
            this.listPupils.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.listPupils.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.listPupils.ColumnHeadersVisible = false;
            this.listPupils.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column2,
            this.Column1});
            this.listPupils.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.listPupils.EnableHeadersVisualStyles = false;
            this.listPupils.Location = new System.Drawing.Point(12, 68);
            this.listPupils.Name = "listPupils";
            this.listPupils.ReadOnly = true;
            this.listPupils.RowHeadersVisible = false;
            this.listPupils.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.listPupils.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.listPupils.Size = new System.Drawing.Size(201, 558);
            this.listPupils.TabIndex = 9;
            this.listPupils.CellEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.ListPupils_CellEnter);
            this.listPupils.MouseEnter += new System.EventHandler(this.ListPupils_MouseEnter);
            // 
            // Column2
            // 
            this.Column2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.Column2.FillWeight = 20F;
            this.Column2.HeaderText = "Number";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            this.Column2.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.Column2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Column2.Width = 20;
            // 
            // Column1
            // 
            this.Column1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Column1.DefaultCellStyle = dataGridViewCellStyle1;
            this.Column1.HeaderText = "Name";
            this.Column1.MinimumWidth = 160;
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            this.Column1.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.Column1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Column1.Width = 160;
            // 
            // commonInfo
            // 
            this.commonInfo.Location = new System.Drawing.Point(168, 12);
            this.commonInfo.Name = "commonInfo";
            this.commonInfo.Size = new System.Drawing.Size(74, 50);
            this.commonInfo.TabIndex = 10;
            this.commonInfo.Text = "Спільні дані";
            this.commonInfo.UseVisualStyleBackColor = true;
            this.commonInfo.Click += new System.EventHandler(this.CommonInfo_Click);
            // 
            // showBlank
            // 
            this.showBlank.Appearance = System.Windows.Forms.Appearance.Button;
            this.showBlank.Location = new System.Drawing.Point(345, 12);
            this.showBlank.Name = "showBlank";
            this.showBlank.Size = new System.Drawing.Size(100, 50);
            this.showBlank.TabIndex = 11;
            this.showBlank.Text = "Показати бланк";
            this.showBlank.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.showBlank.UseVisualStyleBackColor = true;
            this.showBlank.CheckedChanged += new System.EventHandler(this.ShowBlank_CheckedChanged);
            // 
            // turnBlank
            // 
            this.turnBlank.Appearance = System.Windows.Forms.Appearance.Button;
            this.turnBlank.Location = new System.Drawing.Point(547, 12);
            this.turnBlank.Name = "turnBlank";
            this.turnBlank.Size = new System.Drawing.Size(80, 50);
            this.turnBlank.TabIndex = 12;
            this.turnBlank.Text = "Перегорнути додаток";
            this.turnBlank.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.turnBlank.UseVisualStyleBackColor = true;
            this.turnBlank.CheckedChanged += new System.EventHandler(this.TurnBlank_CheckedChanged);
            // 
            // LoadALL_PDF
            // 
            this.LoadALL_PDF.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.LoadALL_PDF.Location = new System.Drawing.Point(734, 12);
            this.LoadALL_PDF.Name = "LoadALL_PDF";
            this.LoadALL_PDF.Size = new System.Drawing.Size(87, 50);
            this.LoadALL_PDF.TabIndex = 13;
            this.LoadALL_PDF.Text = "Експорт усіх в pdf файл...";
            this.LoadALL_PDF.UseVisualStyleBackColor = true;
            this.LoadALL_PDF.Click += new System.EventHandler(this.LoadALL_PDF_Click);
            // 
            // currentClass
            // 
            this.currentClass.Enabled = false;
            this.currentClass.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.currentClass.Location = new System.Drawing.Point(451, 12);
            this.currentClass.Name = "currentClass";
            this.currentClass.Size = new System.Drawing.Size(90, 50);
            this.currentClass.TabIndex = 0;
            this.currentClass.Text = "Поточний клас";
            this.currentClass.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.currentClass.UseVisualStyleBackColor = true;
            // 
            // LoadSel_PDF
            // 
            this.LoadSel_PDF.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.LoadSel_PDF.Location = new System.Drawing.Point(633, 12);
            this.LoadSel_PDF.Name = "LoadSel_PDF";
            this.LoadSel_PDF.Size = new System.Drawing.Size(95, 50);
            this.LoadSel_PDF.TabIndex = 14;
            this.LoadSel_PDF.Text = "Експорт вибраних в pdf файл...";
            this.LoadSel_PDF.UseVisualStyleBackColor = true;
            this.LoadSel_PDF.Click += new System.EventHandler(this.LoadSel_PDF_Click);
            // 
            // blank
            // 
            this.blank.DesktopColor = System.Drawing.SystemColors.ControlDark;
            this.blank.Location = new System.Drawing.Point(219, 68);
            this.blank.Name = "blank";
            this.blank.PageColor = System.Drawing.Color.GhostWhite;
            this.blank.PageSize = ((PdfSharp.Drawing.XSize)(resources.GetObject("blank.PageSize")));
            this.blank.PageSizeF = new System.Drawing.Size(210, 148);
            this.blank.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.blank.ShowScrollbars = false;
            this.blank.Size = new System.Drawing.Size(793, 559);
            this.blank.TabIndex = 19;
            this.blank.Zoom = PdfSharp.Forms.Zoom.BestFit;
            this.blank.ZoomPercent = 279;
            // 
            // LoadALL_SeparatePDF
            // 
            this.LoadALL_SeparatePDF.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.LoadALL_SeparatePDF.Location = new System.Drawing.Point(827, 12);
            this.LoadALL_SeparatePDF.Name = "LoadALL_SeparatePDF";
            this.LoadALL_SeparatePDF.Size = new System.Drawing.Size(87, 50);
            this.LoadALL_SeparatePDF.TabIndex = 17;
            this.LoadALL_SeparatePDF.Text = "Експорт в окремі pdf файли...";
            this.LoadALL_SeparatePDF.UseVisualStyleBackColor = true;
            this.LoadALL_SeparatePDF.Click += new System.EventHandler(this.LoadALL_SeparatePDF_Click);
            // 
            // about
            // 
            this.about.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.about.Location = new System.Drawing.Point(942, 12);
            this.about.Name = "about";
            this.about.Size = new System.Drawing.Size(67, 50);
            this.about.TabIndex = 18;
            this.about.Text = "Справка";
            this.about.UseVisualStyleBackColor = true;
            this.about.Click += new System.EventHandler(this.About_Click);
            // 
            // Base
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1021, 638);
            this.Controls.Add(this.about);
            this.Controls.Add(this.currentClass);
            this.Controls.Add(this.LoadALL_SeparatePDF);
            this.Controls.Add(this.showBlank);
            this.Controls.Add(this.LoadSel_PDF);
            this.Controls.Add(this.turnBlank);
            this.Controls.Add(this.makeList);
            this.Controls.Add(this.importList);
            this.Controls.Add(this.LoadALL_PDF);
            this.Controls.Add(this.commonInfo);
            this.Controls.Add(this.listPupils);
            this.Controls.Add(this.settings);
            this.Controls.Add(this.blank);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "Base";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Load += new System.EventHandler(this.Base_Load);
            ((System.ComponentModel.ISupportInitialize)(this.listPupils)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button currentClass;
        private System.Windows.Forms.Button settings;
        private System.Windows.Forms.Button makeList;
        private System.Windows.Forms.Button importList;
        private System.Windows.Forms.DataGridView listPupils;
        private System.Windows.Forms.Button commonInfo;
        private System.Windows.Forms.CheckBox showBlank;
        private System.Windows.Forms.CheckBox turnBlank;
        private System.Windows.Forms.Button LoadALL_PDF;
        private System.Windows.Forms.Button LoadSel_PDF;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.Button LoadALL_SeparatePDF;
        private System.Windows.Forms.Button about;
        private PdfSharp.Forms.PagePreview blank;
    }
}

