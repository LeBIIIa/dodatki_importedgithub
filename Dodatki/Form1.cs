﻿using PdfSharp;
using PdfSharp.Drawing;
using PdfSharp.Forms;
using PdfSharp.Pdf;
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using Excel = Microsoft.Office.Interop.Excel;

namespace Dodatki
{
    public partial class Base : Form
    {
        public PagePreview.RenderEvent RenderEvent
        {
            get { return renderEvent; }
            set
            {
                blank.SetRenderEvent(value);
                renderEvent = value;
            }
        } PagePreview.RenderEvent renderEvent;

        List<object> comObj = new List<object>();

        Excel.Application xlApp;
        Excel.Workbooks xlWorkBooks;
        Excel.Workbook xlWorkBook;
        Excel.Sheets xlWorkSheets;
        Excel.Worksheet xlWorkSheet;

        Renderer render = new Renderer();

        public static List<Pupil> pupils = new List<Pupil>();

        int i;
        string filename;

        public Base()
        {
            InitializeComponent();
        }
        private void Base_Load(object sender, EventArgs e)
        {
            RenderEvent = new PagePreview.RenderEvent(render.DrawBlank);
            CommonData.SetDefault();
            CommonData.SetDefaultFont();
            currentClass.Image = Properties.Resources.eleven;
            showBlank.Checked = true;
        }
        private void MakeList_Click(object sender, EventArgs e)
        {
            string message = "Відкрити базовий шаблон?";
            string title = "Вибір шаблона";
            DialogResult result = MessageBox.Show(
                message,
                title,
                MessageBoxButtons.YesNo,
                MessageBoxIcon.Question);
            switch (result)
            {
                case DialogResult.Yes:
                    try
                    {
                        string path = Environment.CurrentDirectory + @"\Шаблон.xltx";
                        xlApp = new Excel.Application()
                        {
                            Visible = true
                        };
                        xlWorkBooks = xlApp.Workbooks;
                        xlWorkBook = xlWorkBooks.Open(path);
                        Add(xlApp);
                        Add(xlWorkBooks);
                        Add(xlWorkBook);
                        xlApp.WorkbookBeforeClose += XlApp_WorkbookBeforeClose;
                    }
                    catch (Exception ex)
                    {
						ErrorMessage.LogError(ex);
                        MessageBox.Show("Не вдалося відкрити шаблон відомості!", "Помилка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    break;
                case DialogResult.No:
                    OpenFileDialog openFileDialog1 = new OpenFileDialog()
                    {
                        InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments),
                        Filter = "Файли MS Excel(*.xltx)|*.xltx",
                        RestoreDirectory = true
                    };
                    if (openFileDialog1.ShowDialog() == DialogResult.OK)
                    {
                        xlApp = new Excel.Application()
                        {
                            Visible = true
                        };
                        xlWorkBooks = xlApp.Workbooks;
                        xlWorkBook = xlWorkBooks.Open(openFileDialog1.FileName);
                        Add(xlApp);
                        Add(xlWorkBooks);
                        Add(xlWorkBook);
                        xlApp.WorkbookBeforeClose += XlApp_WorkbookBeforeClose;
                    }

                    break;
            }
        }
        private void XlApp_WorkbookBeforeClose(Excel.Workbook Wb, ref bool Cancel)
        {
            xlApp.Quit();
            UnRegister();
        }
        private void ImportList_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog1 = new OpenFileDialog()
            {
                InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments),
                Filter = "Файли MS Excel(*.xls; *.xlsx)|*.xls; *.xlsx",
                RestoreDirectory = true
            };
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                xlApp = new Excel.Application();
                xlWorkBooks = xlApp.Workbooks;
                xlWorkBook = xlWorkBooks.Open(openFileDialog1.FileName);
                xlWorkSheets = xlWorkBook.Worksheets;
                xlWorkSheet = (Excel.Worksheet)xlWorkSheets.get_Item(2);
                Add(xlApp);
                Add(xlWorkBooks);
                Add(xlWorkBook);
                Add(xlWorkSheets);
                Add(xlWorkSheet);
                //
                Excel.Range rng = xlWorkSheet.UsedRange;
                object[,] paper2Values = (object[,])rng.Value2;
                bool flag = int.TryParse(paper2Values[1, 2].ToString(), out CommonData.klass);
                if (flag)
                {
                    xlWorkSheet = (Excel.Worksheet)xlWorkSheets.get_Item(1);
                    rng = xlWorkSheet.UsedRange;
                    object[,] paper1Values = (object[,])rng.Value2;
                    ReadExcel excel = new ReadExcel(paper1Values, paper2Values, CommonData.klass);

                    var result = excel.ShowDialog(this);
                    if (result == DialogResult.OK)
                    {
                        SetList();
                        //
                        if (CommonData.klass == 9)
                        {
                            currentClass.Image = Properties.Resources.nine;
                        }
                        else if (CommonData.klass == 11)
                        {
                            currentClass.Image = Properties.Resources.eleven;
                        }
                    }
                    DrawOnBlank();
                    xlApp.Quit();
                    UnRegister();
                }
                else
                {
                    MessageBox.Show("Не вибрано клас!");
                }

            }
        }
        public void SetList()
        {
            listPupils.Rows.Clear();
            for (int i = 0; i < pupils.Count; ++i)
            {
                listPupils.Rows.Add();
                listPupils[0, i].Value = i + 1;
                listPupils[1, i].Value = pupils[i].FName + " " + pupils[i].SName + " " + pupils[i].LName;
            }
        }
        private void CommonInfo_Click(object sender, EventArgs e)
        {
            CommonInfo ci = new CommonInfo();
            if (ci.ShowDialog() == DialogResult.OK)
            {
                DrawOnBlank();
            }
        }
        private void Settings_Click(object sender, EventArgs e)
        {
            SettingsModel sm = new SettingsModel(this);
            DialogResult result = sm.ShowDialog();
            if (result != DialogResult.Cancel || result != DialogResult.Abort)
            {
                DrawOnBlank();
            }
        }
        private void ShowBlank_CheckedChanged(object sender, EventArgs e)
        {
            CommonData.Show_Blank ^= true;
            DrawOnBlank();
        }
        private void TurnBlank_CheckedChanged(object sender, EventArgs e)
        {
            CommonData.turn ^= true;
            DrawOnBlank();
        }
        public void DrawOnBlank()
        {
            renderEvent = new PagePreview.RenderEvent(render.DrawBlank);
            blank.Invalidate();
        }
        private void ListPupils_MouseEnter(object sender, EventArgs e)
        {
            listPupils.Focus();
        }
        private void ListPupils_CellEnter(object sender, DataGridViewCellEventArgs e)
        {
            if (listPupils.SelectedRows.Count > 1)
            {
                CommonData.id = -1;
            }
            else
            {
                CommonData.id = e.RowIndex;
                CommonData.selected = pupils[e.RowIndex];
            }
            DrawOnBlank();
        }
        private void LoadSel_PDF_Click(object sender, EventArgs e)
        {
            if (CheckAndSave(ref filename))
            {
                CreateAndFill((from row in listPupils.SelectedRows.Cast<DataGridViewRow>() select pupils[row.Index]).ToList());
            }
        }
        private void LoadALL_PDF_Click(object sender, EventArgs e)
        {
            if (CheckAndSave(ref filename))
            {
                CreateAndFill(pupils);
            }
        }
        private void LoadALL_SeparatePDF_Click(object sender, EventArgs e)
        {
            if (listPupils.SelectedCells.Count == 0)
            {
                MessageBox.Show("Немає вибраних учнів або таблиця пуста!");
                return;
            }
            FolderBrowserDialog folder = new FolderBrowserDialog()
            {
                ShowNewFolderButton = true,
                Description = "Виберіть папку для збереження вибраних учнів в окремі pdf"
            };
            if (folder.ShowDialog() == DialogResult.OK)
            {
                if (listPupils.SelectedRows.Count > 0)
                {
                    List<Pupil> list = (from row in listPupils.SelectedRows.Cast<DataGridViewRow>() select pupils[row.Index]).ToList();
                    foreach (Pupil pupil in list)
                    {
                        string Name = folder.SelectedPath + "\\" + pupil.IndexNumber.ToString() + "_" +
                            pupil.FName + " " + pupil.SName + " " + pupil.LName + ".pdf";
                        CreateAndFill(pupil, Name);
                    }
                }
                else
                {
                    foreach (Pupil pupil in pupils)
                    {
                        string Name = folder.SelectedPath + "\\" + pupil.IndexNumber.ToString() + "_" +
                            pupil.FName + " " + pupil.SName + " " + pupil.LName + ".pdf";
                        CreateAndFill(pupil, Name);
                    }
                }
                Process.Start("explorer", folder.SelectedPath);
            }
        }
        private bool CheckAndSave(ref string filename)
        {
            if (listPupils.SelectedCells.Count == 0)
            {
                MessageBox.Show("Немає вибраних учнів або таблиця пуста!");
                return false;
            }
            SaveFileDialog saveFileDialog1 = new SaveFileDialog()
            {
                Filter = "pdf files (*.pdf)|*.pdf",
                Title = "Зберегти бланк в pdf",
                RestoreDirectory = true
            };
            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                filename = saveFileDialog1.FileName;
                return true;
            }
            return false;
        }
        private void CreateAndFill(Pupil pupil, string NameOfFile)
        {
            try
            {
                PdfDocument document = new PdfDocument();
                XGraphics gfx;
                PdfPage pg = document.Pages.Add();
                gfx = XGraphics.FromPdfPage(pg, XGraphicsUnit.Millimeter);
                render.DrawFront(gfx, pupil);
                pg = document.Pages.Add();
                gfx = XGraphics.FromPdfPage(pg, XGraphicsUnit.Millimeter);
                render.DrawBack(gfx, pupil);
                document.Save(NameOfFile);
            }
            catch (Exception ex)
            {
				ErrorMessage.LogError(ex);
                MessageBox.Show(ex.Message,"Помилка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void CreateAndFill( List<Pupil> list ){
            try
            {
                PdfDocument document = new PdfDocument();
                XGraphics gfx;
                for (i = 0; i < list.Count * 2; ++i)
                {
                    PdfPage pg = document.Pages.Add();
                    gfx = XGraphics.FromPdfPage(pg, XGraphicsUnit.Millimeter);
                    if (i < list.Count)
                    {
                        render.DrawFront(gfx, list[i]);
                    }
                    else
                    {
                        render.DrawBack(gfx, list[i % list.Count]);
                    }
                }
                document.Save(filename);
                Process.Start(filename);
            }
            catch ( Exception ex )
            {
				ErrorMessage.LogError(ex);
                MessageBox.Show(ex.Message, "Помилка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        public Excel.Application Add(Excel.Application obj)
        {
            comObj.Add(obj);
            return obj;
        }
        public Excel.Workbook Add(Excel.Workbook obj)
        {
            comObj.Add(obj);
            return obj;
        }
        public Excel.Workbooks Add(Excel.Workbooks obj)
        {
            comObj.Add(obj);
            return obj;
        }
        private void About_Click(object sender, EventArgs e)
        {
            About about = new About();
            about.ShowDialog();
        }
        public Excel.Sheets Add(Excel.Sheets obj)
        {
            comObj.Add(obj);
            return obj;
        }
        public Excel.Worksheet Add(Excel.Worksheet obj)
        {
            comObj.Add(obj);
            return obj;
        }
        public void UnRegister()
        {
            for (i = comObj.Count - 1; i >= 0; --i)
            {
                ReleaseComObject(comObj[i]);
            }
            comObj.Clear();
        }
        public static void ReleaseComObject(object obj)
        {
            if (obj != null && Marshal.IsComObject(obj))
                try
                {
                    Marshal.FinalReleaseComObject(obj);
                }
                catch { }
                finally
                {
                    obj = null;
                }
            GC.Collect();
            GC.WaitForPendingFinalizers();
            GC.Collect();
            GC.WaitForPendingFinalizers();
        }

        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            if (keyData == Keys.F1)
            {
                about.PerformClick();
            }
            return base.ProcessCmdKey(ref msg, keyData);
        }
    }
}
