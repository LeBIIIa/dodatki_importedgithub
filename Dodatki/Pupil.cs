﻿using System;
using System.Collections.Generic;

namespace Dodatki
{
    public class Pupil
    {
        //Common
        public static string director = "";
        public static DateTime date = DateTime.Now;
        public static string year = "";
        public static string firstSt = "";
        public static string secondSt = "";
        public static string thirdSt = "";
        //
        //Front
        string fName, sName, lName;
        string sex;
        string seriesSertif;
        string numberSertif;
        List<KeyValuePair<string, string>> subjects;
        List<KeyValuePair<string, string>> dpa;
        string averageScore;
        //Back
        List<string> optionals;
        string honor; //Only 9 class
        string study; //Only 11 class
        string medal; //Only 11 class
        string honorSubject;
        string indexNumber;
        public Pupil()
        {
            subjects = new List<KeyValuePair<string, string>>();
            dpa = new List<KeyValuePair<string, string>>();
            optionals = new List<string>();
        }
        public string FName
        {
            get { return fName; }
            set { fName = value; }
        }
        public string SName
        {
            get { return sName; }
            set { sName = value; }
        }
        public string LName
        {
            get { return lName; }
            set { lName = value; }
        }
        public string Sex
        {
            get { return sex; }
            set { sex = value; }
        }
        public string SSertif
        {
            get { return seriesSertif; }
            set { seriesSertif = value; }
        }
        public string NumSertif
        {
            get { return numberSertif; }
            set { numberSertif = value; }
        }
        public string AvScore
        {
            get { return averageScore; }
            set { averageScore = value; }
        }
        public string Honor
        {
            get { return honor; }
            set { honor = value; }
        }
        public string Study
        {
            get { return study; }
            set { study = value; }
        }
        public string Medal
        {
            get { return medal; }
            set { medal = value; }
        }
        public string HonorSubject
        {
            get { return honorSubject; }
            set { honorSubject = value; }
        }
        public string IndexNumber
        {
            get { return indexNumber; }
            set { indexNumber = value; }
        }
        public List<KeyValuePair<string, string>> Subjects
        {
            get {
                return subjects;
            }
        }
        public List<KeyValuePair<string, string>> Dpa
        {
            get
            {
                return dpa;
            }
        }
        public List<string> Optionals
        {
            get
            {
                return optionals;
            }
        }
        public void AddToSubjects(KeyValuePair<string, string> pair)
        {
            subjects.Add(pair);
        }
        public void AddToDpa(KeyValuePair<string, string> pair)
        {
            dpa.Add(pair);
        }
        public void AddToOptionals(string subject)
        {
            optionals.Add(subject);
        }
        public static string WithSex(string sex, int i)
        {
            if ( i > 3 )
            {
                throw new Exception();
            }
            if ( i == 1 )
            {
                if ( sex.Equals("ч") )
                {
                    return "ов";
                }else if ( sex.Equals("ж") )
                {
                    return "ла";
                }
            }else if ( i == 2 )
            {
                if (sex.Equals("ч"))
                {
                    return "ий";
                }
                else if (sex.Equals("ж"))
                {
                    return "а";
                }
            }else if ( i == 3 )
            {
                if (sex.Equals("ч"))
                {
                    return "в";
                }
                else if (sex.Equals("ж"))
                {
                    return "ла";
                }
            }
            return null;
        }
    }
}
