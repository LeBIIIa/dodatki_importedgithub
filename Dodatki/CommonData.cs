﻿using PdfSharp.Drawing;
using PdfSharp.Forms;
using PdfSharp.Pdf;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;

namespace Dodatki
{
    class Renderer
    {
        private XSize stringSize = new XSize();
        private XSize stringSize2 = new XSize();
        private double h, co, t = 0;
        private int n, i, count;
        private string str;
        private XBrush brush = XBrushes.Black, brushBackground = XBrushes.Gray;
        private XPen lines = new XPen(XColors.Gray, 0.2f);
        private XFont fontLines = new XFont("Arial Narrow", CommonData.PtIntoMm(12), XFontStyle.Bold);
        string[] months = {"", "січня", "лютого", "березня", "квітня",
            "травня", "червня", "липня", "серпня",
            "вересня", "жовтня", "листопада", "грудня" };
        public void DrawBlank(XGraphics gfx)
        {
            if ( CommonData.Show_Blank == true )
            {
                DrawBackground(gfx);
            }
            if (CommonData.id != -1)
            {
                if (CommonData.turn == true)
                {
                    DrawFront(gfx);
                } else
                {
                    DrawBack(gfx);
                }
            }
        }
        public void DrawBackground(XGraphics gfx)
        {
            if (CommonData.Show_Blank == true)
            {
                XFont drawFont = new XFont("Arial Narrow", CommonData.PtIntoMm(10), XFontStyle.Bold);

                if (CommonData.turn == true)
                {
                    XPoint s, e;
                    s = new XPoint()
                    {
                        X = CommonData.setting["frontNamesLBoth"],
                        Y = CommonData.setting["frontNamesFUp"]
                    };
                    e = new XPoint()
                    {
                        X = CommonData.setting["frontNamesLBoth"] + CommonData.setting["sizeOfLine"],
                        Y = CommonData.setting["frontNamesFUp"]
                    };
                    //Names Lines
                    gfx.DrawLine(lines, s, e);
                    s.Y = CommonData.setting["frontNamesSUp"];
                    e.Y = CommonData.setting["frontNamesSUp"];
                    gfx.DrawLine(lines, s, e);
                    //School Lines
                    s.Y = CommonData.setting["frontSchoolFUp"];
                    e.Y = CommonData.setting["frontSchoolFUp"];
                    gfx.DrawLine(lines, s, e);
                    s.Y = CommonData.setting["frontSchoolSUp"];
                    e.Y = CommonData.setting["frontSchoolSUp"];
                    gfx.DrawLine(lines, s, e);
                    //Series && Number of Sertif
                    gfx.DrawString(CommonData.str4Len, fontLines, brushBackground, CommonData.setting["frontSeriesLeft"], CommonData.setting["frontSerNum"]);
                    gfx.DrawString("№", drawFont, brushBackground, CommonData.setting["frontSeriesLeft"] + gfx.MeasureString(CommonData.str4Len, fontLines).Width, CommonData.setting["frontSerNum"]);
                    gfx.DrawString(CommonData.strShoterStandart, fontLines, brushBackground, CommonData.setting["frontNumberLeft"], CommonData.setting["frontSerNum"]);
                    //
                    gfx.DrawString("з такими балами:", drawFont, brushBackground, CommonData.setting["frontNamesLBoth"], 62);
                    //
                    n = 12;
                    co = (CommonData.setting["frontSL_LLine"]   - CommonData.setting["frontSL_FLine"]) / (n - 1);
                    h = CommonData.setting["frontSL_FLine"];
                    for (i = 0; i < n; ++i)
                    {
                        gfx.DrawString(CommonData.strStandart, fontLines, brushBackground, CommonData.setting["frontSL_LSub"], h);
                        gfx.DrawString(CommonData.strShoterStandart, fontLines, brushBackground, CommonData.setting["frontSL_LRate"], h);
                        if (i == 11)
                        {
                            t = h;
                        }
                        h += co;
                    }
                    //
                    gfx.DrawString("назва предмету", drawFont, brushBackground, CommonData.setting["frontSL_LSub"] + Math.Abs((gfx.MeasureString("назва предмета", drawFont).Width - gfx.MeasureString(CommonData.strStandart, fontLines).Width) / 2),67);
                    gfx.DrawString("бал", drawFont, brushBackground, (CommonData.setting["frontSL_LRate"]   + Math.Abs((gfx.MeasureString("бал", drawFont).Width - gfx.MeasureString(CommonData.strShoterStandart, fontLines).Width) / 2)), 67);
                    //Right Subject Rows
                    n = 12;
                    co = (CommonData.setting["frontSR_LLine"]  - CommonData.setting["frontSR_FLine"] ) / (n - 1);
                    h = CommonData.setting["frontSR_FLine"];
                    for (i = 0; i < n; ++i)
                    {
                        gfx.DrawString(CommonData.strStandart, fontLines, brushBackground, CommonData.setting["frontSR_LSub"] , h);
                        gfx.DrawString(CommonData.strShoterStandart, fontLines, brushBackground, CommonData.setting["frontSR_LRate"] , h);
                        h += co;
                    }

                    //
                    if (CommonData.dividingLine == true)
                    {
                        XPen pen = new XPen(XColors.Black, CommonData.setting["WidthOfLine"]);
                        double x1 = CommonData.setting["frontSR_LSub"],
                            x2 = CommonData.setting["frontSR_LSub"]  + CommonData.setting["DividingLineSize"];
                        double y1 = CommonData.setting["DividingLineUp"];
                        gfx.DrawLine(pen, x1, y1, x2, y1);
                    }
                    //
                    gfx.DrawString("Пройш", drawFont, brushBackground, CommonData.setting["frontSR_LSub"], CommonData.setting["frontEduUp"]);
                    gfx.DrawString(CommonData.str2Len, fontLines, brushBackground, CommonData.setting["frontEduLeft"], CommonData.setting["frontEduUp"]);
                    str = "державну підсумкову атестацію";
                    gfx.DrawString(str, drawFont, brushBackground, CommonData.setting["frontEduLeft"]  + gfx.MeasureString(CommonData.str2Len, fontLines).Width, CommonData.setting["frontEduUp"]);
                    gfx.DrawString("з таких предметів:", drawFont, brushBackground, CommonData.setting["frontSR_LSub"], CommonData.setting["frontEduUp"]  + gfx.MeasureString(str, fontLines).Height);

                    n = 6;
                    co = (CommonData.setting["frontSL_LLine"] - CommonData.setting["frontSR_FLDPA"])  / (n - 1);
                    h = t;
                    for (i = 0; i < n; ++i)
                    {
                        gfx.DrawString(CommonData.strStandart, fontLines, brushBackground, CommonData.setting["frontSR_LSub"] , h);
                        gfx.DrawString(CommonData.strShoterStandart, fontLines, brushBackground, CommonData.setting["frontSR_LRate"] , h);
                        h -= co;
                    }
                    gfx.DrawString("назва предмету", drawFont, brushBackground, CommonData.setting["frontSR_LSub"]  + Math.Abs((gfx.MeasureString("назва предмета", drawFont).Width - gfx.MeasureString(CommonData.strStandart, fontLines).Width) / 2), CommonData.setting["frontSR_FLDPA"] - 8);
                    gfx.DrawString("бал", drawFont, brushBackground, (CommonData.setting["frontSR_LRate"]  + Math.Abs((gfx.MeasureString("бал", drawFont).Width - gfx.MeasureString(CommonData.strShoterStandart, fontLines).Width) / 2)), CommonData.setting["frontSR_FLDPA"] - 8);

                    if (!string.IsNullOrEmpty(Pupil.thirdSt))
                    {
                        s.Y = CommonData.setting["frontSchoolEUp"];
                        e.Y = CommonData.setting["frontSchoolEUp"];
                        gfx.DrawLine(lines, s, e);
                    }

                    str = CommonData.strLong;
                    if (CommonData.klass == 11)
                    {

                        stringSize2 = gfx.MeasureString(str, fontLines);

                        stringSize = gfx.MeasureString("ДОДАТОК ДО АТЕСТАТА", drawFont);
                        h = stringSize.Height;
                        gfx.DrawString("ДОДАТОК ДО АТЕСТАТА", drawFont, brushBackground, CommonData.setting["frontNamesLBoth"]  + Math.Abs((stringSize2.Width - stringSize.Width) / 2), 8);

                        stringSize = gfx.MeasureString("про повну загальну середню освіту", drawFont);
                        gfx.DrawString("про повну загальну середню освіту", drawFont, brushBackground, CommonData.setting["frontNamesLBoth"]  + Math.Abs((stringSize2.Width - stringSize.Width) / 2), 8 + h);
                                                                                                                                                                                                                                      //
                        gfx.DrawString("здобу", drawFont, brushBackground, CommonData.setting["frontNamesLBoth"], CommonData.setting["front11FinishUp"]);
                        gfx.DrawString(CommonData.str2Len, fontLines, brushBackground, CommonData.setting["front11FinishLeft"], CommonData.setting["front11FinishUp"]);
                        //
                        gfx.DrawString("повну загальну середню освіту", drawFont, brushBackground, CommonData.setting["front11FinishLeft"]  + gfx.MeasureString(CommonData.str2Len, fontLines).Width, CommonData.setting["front11FinishUp"] );
                        gfx.DrawString(CommonData.str2Len, fontLines, brushBackground, CommonData.setting["front11YearLeft"] ,CommonData.setting["front11YearUp"] );
                        //
                        stringSize = gfx.MeasureString("в 20", drawFont);
                        gfx.DrawString("в 20", drawFont, brushBackground, CommonData.setting["front11YearLeft"]  - stringSize.Width, CommonData.setting["front11YearUp"] );
                        gfx.DrawString("році у", drawFont, brushBackground, CommonData.setting["front11YearLeft"]  + gfx.MeasureString(CommonData.str2Len, fontLines).Width, CommonData.setting["front11YearUp"] );

                    }
                    else
                    {
                        stringSize2 = gfx.MeasureString(str, fontLines);

                        stringSize = gfx.MeasureString("ДОДАТОК ДО СВІДОЦТВА", drawFont);
                        h = stringSize.Height;
                        gfx.DrawString("ДОДАТОК ДО СВІДОЦТВА", drawFont, brushBackground, CommonData.setting["frontNamesLBoth"]  + Math.Abs((stringSize2.Width - stringSize.Width) / 2), 8);

                        stringSize = gfx.MeasureString("про базову загальну середню освіту", drawFont);
                        gfx.DrawString("про базову загальну середню освіту", drawFont, brushBackground, CommonData.setting["frontNamesLBoth"]  + Math.Abs((stringSize2.Width - stringSize.Width) / 2), 8 + h);
                        //
                        gfx.DrawString("закінчи", drawFont, brushBackground, CommonData.setting["frontNamesLBoth"] , CommonData.setting["front9FinishUp"] );
                        gfx.DrawString(CommonData.str2Len, fontLines, brushBackground, CommonData.setting["front9FinishLeft"], CommonData.setting["front9FinishUp"]);
                        //
                        gfx.DrawString("у 20", drawFont, brushBackground, CommonData.setting["front9YearLeft"]  - gfx.MeasureString("у 20", drawFont).Width, CommonData.setting["front9YearUp"]);
                        gfx.DrawString(CommonData.str2Len, fontLines, brushBackground, CommonData.setting["front9YearLeft"] , CommonData.setting["front9YearUp"]);
                        gfx.DrawString("році", drawFont, brushBackground, CommonData.setting["front9YearLeft"]  + gfx.MeasureString(CommonData.str2Len, fontLines).Width, CommonData.setting["front9YearUp"]);

                    }

                }
                else
                {
                    stringSize = gfx.MeasureString("Успішно засвої", drawFont);
                    gfx.DrawString("Успішно засвої", drawFont, brushBackground, CommonData.setting["backFacultLeftAll"], CommonData.setting["backLearnUp"]);
                    gfx.DrawString("курсів:", drawFont, brushBackground, CommonData.setting["backFacultLeftAll"], CommonData.setting["backLearnUp"]  + stringSize.Height);
                    gfx.DrawString(CommonData.str4Len, fontLines, brushBackground, CommonData.setting["backLearnLeft"], CommonData.setting["backLearnUp"] );
                    gfx.DrawString("програму факультативних", drawFont, brushBackground, CommonData.setting["backLearnLeft"]  + gfx.MeasureString(CommonData.str4Len, fontLines).Width, CommonData.setting["backLearnUp"]);
                    n = 5;
                    co = (CommonData.setting["backFacultSUp"]  - CommonData.setting["backFacultFUp"] ) / (n - 1);
                    h = CommonData.setting["backFacultFUp"];

                    for (i = 0; i < n; ++i)
                    {
                        gfx.DrawString(CommonData.strFacult, fontLines, brushBackground, CommonData.setting["backFacultLeftAll"], h);
                        h += co;

                    }

                    gfx.DrawString(CommonData.str4Len, fontLines, brushBackground, CommonData.setting["backDateNum"], CommonData.setting["backDateAllUp"] );
                    gfx.DrawString(CommonData.strShoterStandart, fontLines, brushBackground, CommonData.setting["backDateMonth"] , CommonData.setting["backDateAllUp"]);
                    gfx.DrawString("20", drawFont, brushBackground, CommonData.setting["backDateYear"] - gfx.MeasureString("20", drawFont).Width, CommonData.setting["backDateAllUp"]);
                    gfx.DrawString(CommonData.str2Len, fontLines, brushBackground, CommonData.setting["backDateYear"] , CommonData.setting["backDateAllUp"]);
                    gfx.DrawString("р.", drawFont, brushBackground, CommonData.setting["backDateYear"] + gfx.MeasureString(CommonData.str2Len, fontLines).Width + 1, CommonData.setting["backDateAllUp"] );
                    if (CommonData.klass == 11)
                    {
                        gfx.DrawString("За", drawFont, brushBackground, CommonData.setting["backFacultLeftAll"] , CommonData.setting["back11AchiveUp"]);
                        gfx.DrawString(CommonData.strBack, fontLines, brushBackground, CommonData.setting["back11AchiveLeft"], CommonData.setting["back11AchiveUp"]);
                        gfx.DrawString("досягнення у", drawFont, brushBackground, CommonData.setting["back11AchiveLeft"]  + gfx.MeasureString(CommonData.strBack, fontLines).Width, CommonData.setting["back11AchiveUp"] );

                        gfx.DrawString("навчанні нагороджен", drawFont, brushBackground, CommonData.setting["backFacultLeftAll"], CommonData.setting["back11AwardedUp"] );
                        gfx.DrawString(CommonData.str2Len, fontLines, brushBackground, CommonData.setting["back11AwardedLeft"], CommonData.setting["back11AwardedUp"] );
                        gfx.DrawString(CommonData.strBack, fontLines, brushBackground, CommonData.setting["backFacultLeftAll"], CommonData.setting["back11MedalUp"] );
                        gfx.DrawString("медаллю", drawFont, brushBackground, CommonData.setting["backFacultLeftAll"]  + gfx.MeasureString(CommonData.strBack, fontLines).Width, CommonData.setting["back11MedalUp"] );
                        gfx.DrawString("За особливі досягнення у", drawFont, brushBackground, CommonData.setting["backFacultLeftAll"] , CommonData.setting["back11MedalUp"] + 5);
                        gfx.DrawString("вивченні", drawFont, brushBackground, CommonData.setting["backFacultLeftAll"], CommonData.setting["back11LearnUp"] );
                        gfx.DrawString(CommonData.strBack, fontLines, brushBackground, CommonData.setting["back11LearnLeft"], CommonData.setting["back11LearnUp"] );

                        gfx.DrawString("нагороджен", drawFont, brushBackground, CommonData.setting["backFacultLeftAll"] , CommonData.setting["back11DiplomUp"] );
                        gfx.DrawString(CommonData.str2Len, fontLines, brushBackground, CommonData.setting["back11DiplomLeft"] , CommonData.setting["back11DiplomUp"] );
                        gfx.DrawString("Похвальною грамотою", drawFont, brushBackground, CommonData.setting["backFacultLeftAll"] , CommonData.setting["back11DiplomUp"] + 5);

                        gfx.DrawString("Директор", drawFont, brushBackground, CommonData.setting["backFacultLeftAll"], CommonData.setting["back11DirectorUp"]);
                        gfx.DrawString(CommonData.strShoterStandart, fontLines, brushBackground, CommonData.setting["back11DirectorLeft"], CommonData.setting["back11DirectorUp"]);
                    }
                    else
                    {

                        gfx.DrawString("За високі досягнення у навчанні одержа", drawFont, brushBackground, CommonData.setting["backFacultLeftAll"], CommonData.setting["back9GetUp"] );
                        gfx.DrawString(CommonData.str2Len, fontLines, brushBackground, CommonData.setting["back9GetLeft"] , CommonData.setting["back9GetUp"]);
                        gfx.DrawString(CommonData.strFacult, fontLines, brushBackground, CommonData.setting["backFacultLeftAll"], CommonData.setting["back9GetUp_LLow"]);

                        gfx.DrawString("За особливі досягнення у вивченні", drawFont, brushBackground, CommonData.setting["backFacultLeftAll"], CommonData.setting["back9AwardBothUp"] - 5);
                        gfx.DrawString(CommonData.strBack, fontLines, brushBackground, CommonData.setting["backFacultLeftAll"], CommonData.setting["back9AwardBothUp"]);
                        gfx.DrawString("нагорожден", drawFont, brushBackground, CommonData.setting["backFacultLeftAll"]  + gfx.MeasureString(CommonData.strBack, fontLines).Width, CommonData.setting["back9AwardBothUp"] );
                        gfx.DrawString(CommonData.str2Len, fontLines, brushBackground, CommonData.setting["back9AwardShortLeft"], CommonData.setting["back9AwardBothUp"]);
                        gfx.DrawString("Похвальною грамотою", drawFont, brushBackground, CommonData.setting["backFacultLeftAll"], CommonData.setting["back9AwardBothUp"] + 5);

                        gfx.DrawString("Директор", drawFont, brushBackground, CommonData.setting["backFacultLeftAll"], CommonData.setting["back9DirectorUp"] );
                        gfx.DrawString(CommonData.strShoterStandart, fontLines, brushBackground, CommonData.setting["back9DirectorLeft"], CommonData.setting["back9DirectorUp"] );

                    }

                }
            }
        }
        public void DrawFront(XGraphics gfx)
        {
            try
            {
                //First Name Row
                XFont drawFontNames = CommonData.GetFont(1);
                stringSize = gfx.MeasureString(CommonData.selected.FName, drawFontNames);
                gfx.DrawString(CommonData.selected.FName, drawFontNames, brush, CommonData.setting["frontNamesLBoth"] + Math.Abs((CommonData.setting["sizeOfLine"] - stringSize.Width) / 2), CommonData.setting["frontNamesFUp"]);
                //Second Name Row
                str = CommonData.selected.SName + " " + CommonData.selected.LName;
                stringSize = gfx.MeasureString(str, drawFontNames);
                gfx.DrawString(str, drawFontNames, brush, CommonData.setting["frontNamesLBoth"] + Math.Abs((CommonData.setting["sizeOfLine"] - stringSize.Width) / 2), CommonData.setting["frontNamesSUp"]);
                //School Rows
                XFont drawFontSchool = CommonData.GetFont(2);
                stringSize = gfx.MeasureString(Pupil.firstSt, drawFontSchool);
                gfx.DrawString(Pupil.firstSt, drawFontSchool, brush, CommonData.setting["frontNamesLBoth"] + Math.Abs((CommonData.setting["sizeOfLine"] - stringSize.Width) / 2), CommonData.setting["frontSchoolFUp"]);
                stringSize = gfx.MeasureString(Pupil.secondSt, drawFontSchool);
                gfx.DrawString(Pupil.secondSt, drawFontSchool, brush, CommonData.setting["frontNamesLBoth"] + Math.Abs((CommonData.setting["sizeOfLine"] - stringSize.Width) / 2), CommonData.setting["frontSchoolSUp"]);
                //Extra School Row
                stringSize = gfx.MeasureString(Pupil.thirdSt, drawFontSchool);
                gfx.DrawString(Pupil.thirdSt, drawFontSchool, brush, CommonData.setting["frontNamesLBoth"] + Math.Abs((CommonData.setting["sizeOfLine"] - stringSize.Width) / 2), CommonData.setting["frontSchoolEUp"]);
                //Number && Serial Sertificate
                XFont drawFontDoc = CommonData.GetFont(0);
                gfx.DrawString(CommonData.selected.SSertif, drawFontDoc, brush, CommonData.setting["frontSeriesLeft"], CommonData.setting["frontSerNum"]);
                gfx.DrawString(CommonData.selected.NumSertif, drawFontDoc, brush, CommonData.setting["frontNumberLeft"], CommonData.setting["frontSerNum"]);
                //Left Subject Rows
                XFont drawFontSubject = CommonData.GetFont(3);
                n = 12;
                co = (CommonData.setting["frontSL_LLine"] - CommonData.setting["frontSL_FLine"]) / (n - 1);
                h = CommonData.setting["frontSL_FLine"];
                n += (CommonData.extraLeft == true) ? 1 : 0;
                count = 0;
                for (i = 0; i < n; ++i)
                {
                    if (count >= CommonData.selected.Subjects.Count)
                    {
                        break;
                    }
                    KeyValuePair<string, string> pair = CommonData.selected.Subjects.ElementAt(count);
                    str = CommonData.NumToStr(pair.Value);
                    gfx.DrawString(pair.Key, drawFontSubject, brush, CommonData.setting["frontSL_LSub"], h);
                    gfx.DrawString(str, drawFontSubject, brush, CommonData.setting["frontSL_LRate"], h);
                    if (i == 11)
                    {
                        t = h;
                    }
                    count++;
                    h += co;
                }
                if (CommonData.extraLeft == true)
                {
                    XPen pen = new XPen(XColors.Black, 0.3f);
                    double x1 = CommonData.setting["frontSL_LSub"], x2 = CommonData.setting["frontSL_LSub"] + CommonData.setting["sizeOfLineSub"];
                    double y1 = h - co + 0.33f;
                    gfx.DrawLine(pen, x1, y1, x2, y1);
                    x1 = CommonData.setting["frontSL_LRate"];
                    x2 = CommonData.setting["frontSL_LRate"] + CommonData.setting["sizeOfLineRate"];
                    gfx.DrawLine(pen, x1, y1, x2, y1);
                }
                //Right Subject Rows
                n = 12;
                co = (CommonData.setting["frontSR_LLine"] - CommonData.setting["frontSR_FLine"]) / (n - 1);
                h = CommonData.setting["frontSR_FLine"];
                if (CommonData.extraRight == true)
                {
                    n++;
                    h -= co;
                    XPen pen = new XPen(XColors.Black, 0.3f);
                    double x1 = CommonData.setting["frontSR_LSub"], x2 = CommonData.setting["frontSR_LSub"] + CommonData.setting["sizeOfLineSub"];
                    double y1 = h + 0.33f;
                    gfx.DrawLine(pen, x1, y1, x2, y1);
                    x1 = CommonData.setting["frontSR_LRate"];
                    x2 = CommonData.setting["frontSR_LRate"] + CommonData.setting["sizeOfLineRate"];
                    gfx.DrawLine(pen, x1, y1, x2, y1);
                }
                for (i = 0; i < n; ++i)
                {
                    if (count >= CommonData.selected.Subjects.Count)
                    {
                        break;
                    }
                    KeyValuePair<string, string> pair = CommonData.selected.Subjects.ElementAt(count);
                    str = CommonData.NumToStr(pair.Value);
                    gfx.DrawString(pair.Key, drawFontSubject, brush, CommonData.setting["frontSR_LSub"], h);
                    gfx.DrawString(str, drawFontSubject, brush, CommonData.setting["frontSR_LRate"], h);
                    count++;
                    h += co;

                }
                //
                if (CommonData.dividingLine == true)
                {
                    XPen pen = new XPen(XColors.Black, CommonData.PtIntoMm(CommonData.setting["WidthOfLine"]));
                    double x1 = CommonData.setting["frontSR_LSub"],
                        x2 = CommonData.setting["frontSR_LSub"] + CommonData.setting["DividingLineSize"];
                    double y1 = CommonData.setting["DividingLineUp"];
                    gfx.DrawLine(pen, x1, y1, x2, y1);
                }
                if ( CommonData.selected.Dpa.Count == 1 && CommonData.selected.Dpa.Contains(new KeyValuePair<string, string>("Звільнений", "")) )
                {}
                else
                {
                    //Pupil Set Sex
                    gfx.DrawString(Pupil.WithSex(CommonData.selected.Sex, 1), drawFontSubject, brush, CommonData.setting["frontEduLeft"], CommonData.setting["frontEduUp"]);
                }
                //DPA Rows
                n = 6;
                co = (CommonData.setting["frontSL_LLine"] - CommonData.setting["frontSR_FLDPA"]) / (n - 1);
                h = CommonData.setting["frontSR_FLDPA"];
                count = 0;
                for (i = 0; i < n; ++i)
                {
                    if (count < CommonData.selected.Dpa.Count)
                    {
                        KeyValuePair<string, string> pair = CommonData.selected.Dpa.ElementAt(count);
                        str = CommonData.NumToStr(pair.Value);
                        gfx.DrawString(pair.Key, drawFontSubject, brush, CommonData.setting["frontSR_LSub"], h);
                        gfx.DrawString(str, drawFontSubject, brush, CommonData.setting["frontSR_LRate"], h);
                    }
                    h += co;
                    count++;
                }
                h -= co;
                //Average Score
                if (CommonData.selected.AvScore != null)
                {
                    gfx.DrawString(CommonData.selected.AvScore, drawFontSubject, brush, CommonData.setting["frontSR_LRate"], h);
                    if ( CommonData.klass == 11 )
                    {
                        gfx.DrawString("Середній бал атестата", drawFontSubject, brush, CommonData.setting["frontSR_LSub"], h);
                    }
                    else
                    {
                        gfx.DrawString("Середній бал свідоцтва", drawFontSubject, brush, CommonData.setting["frontSR_LSub"], h);
                    }
                }
                if (CommonData.klass == 11)
                {
                    gfx.DrawString(Pupil.WithSex(CommonData.selected.Sex, 3), drawFontSubject, brush, CommonData.setting["front11FinishLeft"], CommonData.setting["front11FinishUp"]);
                    //
                    str = Pupil.year;
                    if (!string.IsNullOrEmpty(str))
                    {
                        str = str.Substring(str.Length - 2);
                    }
                    gfx.DrawString(str, drawFontSubject, brush, CommonData.setting["front11YearLeft"], CommonData.setting["front11YearUp"]);
                }
                else
                {
                    gfx.DrawString(Pupil.WithSex(CommonData.selected.Sex, 3), drawFontSubject, brush, CommonData.setting["front9FinishLeft"], CommonData.setting["front9FinishUp"]);
                    //
                    str = Pupil.year;
                    if (!string.IsNullOrEmpty(str))
                    {
                        str = str.Substring(str.Length - 2);
                    }
                    gfx.DrawString(str, drawFontSubject, brush, CommonData.setting["front9YearLeft"], CommonData.setting["front9YearUp"]);
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void DrawBack(XGraphics gfx)
        {
            try
            {
                XFont drawFont = new XFont("Arial", 4.233f);
                
                XFont drawFontBack = CommonData.GetFont(4);
                XFont drawOptionals = CommonData.GetFont(5);
                //Pupil Set Sex
                if (CommonData.selected.Optionals.Count > 0)
                {
                    gfx.DrawString(Pupil.WithSex(CommonData.selected.Sex, 3), drawFontBack, brush, CommonData.setting["backLearnLeft"], CommonData.setting["backLearnUp"]);
                }
                //Pupil Facult
                n = 5;
                co = (CommonData.setting["backFacultSUp"] - CommonData.setting["backFacultFUp"]) / (n - 1);
                h = CommonData.setting["backFacultFUp"];
                for (i = 0; i < n; ++i)
                {
                    if (i >= CommonData.selected.Optionals.Count)
                    {
                        break;
                    }
                    gfx.DrawString(CommonData.selected.Optionals[i], drawOptionals, brush, CommonData.setting["backFacultLeftAll"], h);
                    h += co;

                }
                //Date
                str = Pupil.date.ToString("dd", CultureInfo.GetCultureInfo("uk-UA"));
                stringSize = gfx.MeasureString(CommonData.str4Len, fontLines);
                stringSize2 = gfx.MeasureString(str, drawFont);
                gfx.DrawString(str, drawFontBack, brush, CommonData.setting["backDateNum"] + Math.Abs(stringSize.Width - stringSize2.Width) / 2, CommonData.setting["backDateAllUp"]);

                str = months[Pupil.date.Month];
                stringSize = gfx.MeasureString(CommonData.strShoterStandart, fontLines);
                stringSize2 = gfx.MeasureString(str, drawFont);
                gfx.DrawString(str, drawFontBack, brush, CommonData.setting["backDateMonth"] + Math.Abs(stringSize.Width - stringSize2.Width) / 2, CommonData.setting["backDateAllUp"]);

                str = Pupil.date.ToString("yy", CultureInfo.GetCultureInfo("uk-UA"));
                stringSize = gfx.MeasureString(CommonData.str3Len, fontLines);
                stringSize2 = gfx.MeasureString(str, drawFont);
                gfx.DrawString(str, drawFontBack, brush, CommonData.setting["backDateYear"] + Math.Abs(stringSize.Width - stringSize2.Width) / 2, CommonData.setting["backDateAllUp"]);
                //
                if (CommonData.klass == 11)
                {
                    stringSize = ((CommonData.selected.Study == null) ? gfx.MeasureString("", drawFontBack) : gfx.MeasureString(CommonData.selected.Study, drawFontBack));
                    stringSize2 = gfx.MeasureString(CommonData.strBack, fontLines);
                    gfx.DrawString(CommonData.selected.Study ?? "", drawFontBack, brush, CommonData.setting["back11AchiveLeft"] + Math.Abs(stringSize.Width - stringSize2.Width) / 2, CommonData.setting["back11AchiveUp"]);

                    if (!string.IsNullOrWhiteSpace(CommonData.selected.Medal))
                    {
                        stringSize = ((CommonData.selected.Medal == null) ? gfx.MeasureString("", drawFontBack) : gfx.MeasureString(CommonData.selected.Medal, drawFontBack));
                        stringSize2 = gfx.MeasureString(CommonData.strBack, fontLines);
                        gfx.DrawString(Pupil.WithSex(CommonData.selected.Sex, 2), drawFontBack, brush, CommonData.setting["back11AwardedLeft"], CommonData.setting["back11AwardedUp"]);
                        gfx.DrawString(CommonData.selected.Medal, drawFontBack, brush, CommonData.setting["backFacultLeftAll"] + Math.Abs(stringSize.Width - stringSize2.Width) / 2, CommonData.setting["back11MedalUp"]);
                    }
                    if (!string.IsNullOrWhiteSpace(CommonData.selected.HonorSubject))
                    {
                        gfx.DrawString(Pupil.WithSex(CommonData.selected.Sex, 2), drawFontBack, brush, CommonData.setting["back11DiplomLeft"], CommonData.setting["back11DiplomUp"]);
                        gfx.DrawString(CommonData.selected.HonorSubject, drawFontBack, brush, CommonData.setting["back11LearnLeft"], CommonData.setting["back11LearnUp"]);
                    }
                    gfx.DrawString(Pupil.director, drawFontBack, brush, CommonData.setting["back11DirectorLeft"], CommonData.setting["back11DirectorUp"]);
                }
                else
                {
                    if (!string.IsNullOrWhiteSpace(CommonData.selected.Honor))
                    {
                        gfx.DrawString(Pupil.WithSex(CommonData.selected.Sex, 3), drawFontBack, brush, CommonData.setting["back9GetLeft"], CommonData.setting["back9GetUp"]);
                        gfx.DrawString(CommonData.selected.Honor, drawFontBack, brush, CommonData.setting["backFacultLeftAll"], CommonData.setting["back9GetUp_LLow"]);
                    }

                    if (!string.IsNullOrWhiteSpace(CommonData.selected.HonorSubject))
                    {
                        gfx.DrawString(Pupil.WithSex(CommonData.selected.Sex, 2), drawFontBack, brush, CommonData.setting["back9AwardShortLeft"], CommonData.setting["back9AwardBothUp"]);
                        gfx.DrawString(CommonData.selected.HonorSubject, drawFontBack, brush, CommonData.setting["backFacultLeftAll"], CommonData.setting["back9AwardBothUp"]);
                    }
                    gfx.DrawString(Pupil.director, drawFontBack, brush, CommonData.setting["back11DirectorLeft"], CommonData.setting["back9DirectorUp"]);

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void DrawFront(XGraphics gfx, Pupil pupil)
        {
            try
            {
                XFont drawFont = new XFont("Arial", 4.233f);
                //First Name Row
                XFont drawFontNames = CommonData.GetFont(1);
                stringSize = gfx.MeasureString(pupil.FName, drawFontNames);
                gfx.DrawString(pupil.FName, drawFontNames, brush, CommonData.setting["frontNamesLBoth"] + Math.Abs((CommonData.setting["sizeOfLine"] - stringSize.Width) / 2), CommonData.setting["frontNamesFUp"]);
                //Second Name Row
                str = pupil.SName + " " + pupil.LName;
                stringSize = gfx.MeasureString(str, drawFontNames);
                gfx.DrawString(str, drawFontNames, brush, CommonData.setting["frontNamesLBoth"] + Math.Abs((CommonData.setting["sizeOfLine"] - stringSize.Width) / 2), CommonData.setting["frontNamesSUp"]);
                //School Rows
                XFont drawFontSchool = CommonData.GetFont(2);
                stringSize = gfx.MeasureString(Pupil.firstSt, drawFontSchool);
                gfx.DrawString(Pupil.firstSt, drawFontSchool, brush, CommonData.setting["frontNamesLBoth"] + Math.Abs((CommonData.setting["sizeOfLine"] - stringSize.Width) / 2), CommonData.setting["frontSchoolFUp"]);
                stringSize = gfx.MeasureString(Pupil.secondSt, drawFontSchool);
                gfx.DrawString(Pupil.secondSt, drawFontSchool, brush, CommonData.setting["frontNamesLBoth"] + Math.Abs((CommonData.setting["sizeOfLine"] - stringSize.Width) / 2), CommonData.setting["frontSchoolSUp"]);
                //Extra School Row
                stringSize = gfx.MeasureString(Pupil.thirdSt, drawFontSchool);
                gfx.DrawString(Pupil.thirdSt, drawFontSchool, brush, CommonData.setting["frontNamesLBoth"] + Math.Abs((CommonData.setting["sizeOfLine"] - stringSize.Width) / 2), CommonData.setting["frontSchoolEUp"]);
                //Number && Serial Sertificate
                XFont drawFontDoc = CommonData.GetFont(0);
                gfx.DrawString(pupil.SSertif, drawFontDoc, brush, CommonData.setting["frontSeriesLeft"], CommonData.setting["frontSerNum"]);
                gfx.DrawString(pupil.NumSertif, drawFontDoc, brush, CommonData.setting["frontNumberLeft"], CommonData.setting["frontSerNum"]);
                //Left Subject Rows
                XFont drawFontSubject = CommonData.GetFont(3);
                n = 12;
                co = (CommonData.setting["frontSL_LLine"] - CommonData.setting["frontSL_FLine"]) / (n - 1);
                h = CommonData.setting["frontSL_FLine"];
                n += (CommonData.extraLeft == true) ? 1 : 0;
                count = 0;
                for (i = 0; i < n; ++i)
                {
                    if (count >= pupil.Subjects.Count)
                    {
                        break;
                    }
                    KeyValuePair<string, string> pair = pupil.Subjects.ElementAt(count);
                    str = CommonData.NumToStr(pair.Value);
                    gfx.DrawString(pair.Key, drawFontSubject, brush, CommonData.setting["frontSL_LSub"], h);
                    gfx.DrawString(str, drawFontSubject, brush, CommonData.setting["frontSL_LRate"], h);
                    if (i == 11)
                    {
                        t = h;
                    }
                    count++;
                    h += co;
                }
                if (CommonData.extraLeft == true)
                {
                    XPen pen = new XPen(XColors.Black, 0.3f);
                    double x1 = CommonData.setting["frontSL_LSub"], x2 = CommonData.setting["frontSL_LSub"] + CommonData.setting["sizeOfLineSub"];
                    double y1 = h - co + 0.33f;
                    gfx.DrawLine(pen, x1, y1, x2, y1);
                    x1 = CommonData.setting["frontSL_LRate"];
                    x2 = CommonData.setting["frontSL_LRate"] + CommonData.setting["sizeOfLineRate"];
                    gfx.DrawLine(pen, x1, y1, x2, y1);
                }
                //Right Subject Rows
                n = 12;
                co = (CommonData.setting["frontSR_LLine"] - CommonData.setting["frontSR_FLine"]) / (n - 1);
                h = CommonData.setting["frontSR_FLine"];
                if (CommonData.extraRight == true)
                {
                    n++;
                    h -= co;
                    XPen pen = new XPen(XColors.Black, 0.3f);
                    double x1 = CommonData.setting["frontSR_LSub"], x2 = CommonData.setting["frontSR_LSub"] + CommonData.setting["sizeOfLineSub"];
                    double y1 = h + 0.33f;
                    gfx.DrawLine(pen, x1, y1, x2, y1);
                    x1 = CommonData.setting["frontSR_LRate"];
                    x2 = CommonData.setting["frontSR_LRate"] + CommonData.setting["sizeOfLineRate"];
                    gfx.DrawLine(pen, x1, y1, x2, y1);
                }
                for (i = 0; i < n; ++i)
                {
                    if (count >= pupil.Subjects.Count)
                    {
                        break;
                    }
                    KeyValuePair<string, string> pair = pupil.Subjects.ElementAt(count);
                    str = CommonData.NumToStr(pair.Value);
                    gfx.DrawString(pair.Key, drawFontSubject, brush, CommonData.setting["frontSR_LSub"], h);
                    gfx.DrawString(str, drawFontSubject, brush, CommonData.setting["frontSR_LRate"], h);
                    count++;
                    h += co;

                }
                //
                if (CommonData.dividingLine == true)
                {
                    XPen pen = new XPen(XColors.Black, CommonData.PtIntoMm(CommonData.setting["WidthOfLine"]));
                    double x1 = CommonData.setting["frontSR_LSub"],
                        x2 = CommonData.setting["frontSR_LSub"] + CommonData.setting["DividingLineSize"];
                    double y1 = CommonData.setting["DividingLineUp"];
                    gfx.DrawLine(pen, x1, y1, x2, y1);
                }

                if (CommonData.selected.Dpa.Count == 1 && CommonData.selected.Dpa.Contains(new KeyValuePair<string, string>("Звільнений", "")))
                { }
                else
                {
                    //Pupil Set Sex
                    gfx.DrawString(Pupil.WithSex(CommonData.selected.Sex, 1), drawFontSubject, brush, CommonData.setting["frontEduLeft"], CommonData.setting["frontEduUp"]);
                }

                //DPA Rows
                n = 6;
                co = (CommonData.setting["frontSL_LLine"] - CommonData.setting["frontSR_FLDPA"]) / (n - 1);
                h = CommonData.setting["frontSR_FLDPA"];
                count = 0;
                for (i = 0; i < n; ++i)
                {
                    if (count < pupil.Dpa.Count)
                    {
                        KeyValuePair<string, string> pair = pupil.Dpa.ElementAt(count);
                        str = CommonData.NumToStr(pair.Value);
                        gfx.DrawString(pair.Key, drawFontSubject, brush, CommonData.setting["frontSR_LSub"], h);
                        gfx.DrawString(str, drawFontSubject, brush, CommonData.setting["frontSR_LRate"], h);
                    }
                    h += co;
                    count++;
                }
                h -= co;
                if (pupil.AvScore != null)
                {
                    //Average Score
                    gfx.DrawString(pupil.AvScore, drawFontSubject, brush, CommonData.setting["frontSR_LRate"], h);
                    if ( CommonData.klass == 11 )
                    {
                        gfx.DrawString("Середній бал атестата", drawFontSubject, brush, CommonData.setting["frontSR_LSub"], h);
                    }
                    else
                    {
                        gfx.DrawString("Середній бал свідоцтва", drawFontSubject, brush, CommonData.setting["frontSR_LSub"], h);
                    }
                }
                if (CommonData.klass == 11)
                {
                    gfx.DrawString(Pupil.WithSex(pupil.Sex, 3), drawFontSubject, brush, CommonData.setting["front11FinishLeft"], CommonData.setting["front11FinishUp"]);
                    //
                    str = Pupil.year;
                    if (!string.IsNullOrEmpty(str))
                    {
                        str = str.Substring(str.Length - 2);
                    }
                    gfx.DrawString(str, drawFontSubject, brush, CommonData.setting["front11YearLeft"], CommonData.setting["front11YearUp"]);
                }
                else
                {
                    gfx.DrawString(Pupil.WithSex(pupil.Sex, 3), drawFontSubject, brush, CommonData.setting["front9FinishLeft"], CommonData.setting["front9FinishUp"]);
                    //
                    str = Pupil.year;
                    if (!string.IsNullOrEmpty(str))
                    {
                        str = str.Substring(str.Length - 2);
                    }
                    gfx.DrawString(str, drawFontSubject, brush, CommonData.setting["front9YearLeft"], CommonData.setting["front9YearUp"]);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void DrawBack(XGraphics gfx, Pupil pupil)
        {
            try
            {
                XFont drawFont = new XFont("Arial", 4.233f);

                XFont drawFontBack = CommonData.GetFont(4);
                //Pupil Set Sex
                if ( pupil.Optionals.Count > 0 ) {
                    gfx.DrawString(Pupil.WithSex(pupil.Sex, 3), drawFontBack, brush, CommonData.setting["backLearnLeft"], CommonData.setting["backLearnUp"]);
                }
                //Pupil Facult
                n = 5;
                co = (CommonData.setting["backFacultSUp"] - CommonData.setting["backFacultFUp"]) / (n - 1);
                h = CommonData.setting["backFacultFUp"];
                for (i = 0; i < n; ++i)
                {
                    if (i >= pupil.Optionals.Count)
                    {
                        break;
                    }
                    gfx.DrawString(pupil.Optionals[i], drawFontBack, brush, CommonData.setting["backFacultLeftAll"], h);
                    h += co;
                }
                //Date
                str = Pupil.date.ToString("dd", CultureInfo.GetCultureInfo("uk-UA"));
                stringSize = gfx.MeasureString(CommonData.str4Len, fontLines);
                stringSize2 = gfx.MeasureString(str, drawFont);
                gfx.DrawString(str, drawFontBack, brush, CommonData.setting["backDateNum"] + Math.Abs(stringSize.Width - stringSize2.Width) / 2, CommonData.setting["backDateAllUp"]);

                str = months[Pupil.date.Month];
                stringSize = gfx.MeasureString(CommonData.strShoterStandart, fontLines);
                stringSize2 = gfx.MeasureString(str, drawFont);
                gfx.DrawString(str, drawFontBack, brush, CommonData.setting["backDateMonth"] + Math.Abs(stringSize.Width - stringSize2.Width) / 2, CommonData.setting["backDateAllUp"]);

                str = Pupil.date.ToString("yy", CultureInfo.GetCultureInfo("uk-UA"));
                stringSize = gfx.MeasureString(CommonData.str3Len, fontLines);
                stringSize2 = gfx.MeasureString(str, drawFont);
                gfx.DrawString(str, drawFontBack, brush, CommonData.setting["backDateYear"] + Math.Abs(stringSize.Width - stringSize2.Width) / 2, CommonData.setting["backDateAllUp"]);
                //
                if (CommonData.klass == 11)
                {
                    stringSize = ((pupil.Study == null) ? gfx.MeasureString("", drawFontBack) : gfx.MeasureString(pupil.Study, drawFontBack));
                    stringSize2 = gfx.MeasureString(CommonData.strBack, fontLines);
                    gfx.DrawString(pupil.Study ?? "", drawFontBack, brush, CommonData.setting["back11AchiveLeft"] + Math.Abs(stringSize.Width - stringSize2.Width) / 2, CommonData.setting["back11AchiveUp"]);

                    if (!string.IsNullOrWhiteSpace(pupil.Medal))
                    {
                        stringSize = ((pupil.Medal == null) ? gfx.MeasureString("", drawFontBack) : gfx.MeasureString(pupil.Medal, drawFontBack));
                        stringSize2 = gfx.MeasureString(CommonData.strBack, fontLines);
                        gfx.DrawString(Pupil.WithSex(pupil.Sex, 2), drawFontBack, brush, CommonData.setting["back11AwardedLeft"], CommonData.setting["back11AwardedUp"]);
                        gfx.DrawString(pupil.Medal, drawFontBack, brush, CommonData.setting["backFacultLeftAll"] + Math.Abs(stringSize.Width - stringSize2.Width) / 2, CommonData.setting["back11MedalUp"]);
                    }
                    if (!string.IsNullOrWhiteSpace(pupil.HonorSubject))
                    {
                        gfx.DrawString(Pupil.WithSex(pupil.Sex, 2), drawFontBack, brush, CommonData.setting["back11DiplomLeft"], CommonData.setting["back11DiplomUp"]);
                        gfx.DrawString(pupil.HonorSubject, drawFontBack, brush, CommonData.setting["back11LearnLeft"], CommonData.setting["back11LearnUp"]);
                    }
                    gfx.DrawString(Pupil.director, drawFontBack, brush, CommonData.setting["back11DirectorLeft"], CommonData.setting["back11DirectorUp"]);
                }
                else
                {
                    if (!string.IsNullOrWhiteSpace(pupil.Honor))
                    {
                        gfx.DrawString(Pupil.WithSex(pupil.Sex, 3), drawFontBack, brush, CommonData.setting["back9GetLeft"], CommonData.setting["back9GetUp"]);
                        gfx.DrawString(pupil.Honor, drawFontBack, brush, CommonData.setting["backFacultLeftAll"], CommonData.setting["back9GetUp_LLow"]);
                    }

                    if (!string.IsNullOrWhiteSpace(pupil.HonorSubject))
                    {
                        gfx.DrawString(Pupil.WithSex(pupil.Sex, 2), drawFontBack, brush, CommonData.setting["back9AwardShortLeft"], CommonData.setting["back9AwardBothUp"]);
                        gfx.DrawString(pupil.HonorSubject, drawFontBack, brush, CommonData.setting["backFacultLeftAll"], CommonData.setting["back9AwardBothUp"]);
                    }
                    gfx.DrawString(Pupil.director, drawFontBack, brush, CommonData.setting["back11DirectorLeft"], CommonData.setting["back9DirectorUp"]);

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }

public static class CommonData
    {
        public static float KO = 2.834645669f;
        public static Dictionary<string, double> setting = new Dictionary<string, double>();
        public static Dictionary<string, Font> ItemsFonts = new Dictionary<string, Font>();
        public static bool extraLeft;
        public static bool extraRight;
        public static bool dividingLine;
        public static bool turn = true;
        public static bool Show_Blank = false;
        public static int klass = 11;
        public static int id = -1;
        public static Pupil selected;
        //
        private static XPdfFontOptions options = new XPdfFontOptions(PdfFontEncoding.Unicode, PdfFontEmbedding.Always);
        //
        public static string strLong = "________________________________________________";
        public static string str4Len = "_____";
        public static string strStandart = "________________________________";
        public static string strShoterStandart = "_______________";

        public static string str2Len = "___";
        public static string str3Len = "____";
        public static string strFacult = "_______________________________________";
        public static string strBack = "__________________________";
        public static string strDirector = "__________________";
        //

        //
        public static double PtIntoMm(double pt)
        {
            return pt * 0.3514;
        }
        public static string NumToStr(string str)
        {
            try
            {
                switch (str)
                {
                    case "1":
                        return "один";
                    case "2":
                        return "два";
                    case "3":
                        return "три";
                    case "4":
                        return "чотири";
                    case "5":
                        return "п'ять";
                    case "6":
                        return "шість";
                    case "7":
                        return "сім";
                    case "8":
                        return "вісім";
                    case "9":
                        return "дев'ять";
                    case "10":
                        return "десять";
                    case "11":
                        return "одинадцять";
                    case "12":
                        return "дванадцять";
                    default:
                        return str;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static void ReadSettings(string path)
        {
            string s = string.Empty;
            string s1 = string.Empty;
            BinaryFormatter bf = null;
            FileStream fs = null;
            try
            {
                if (File.Exists(path))
                {
                    fs = new FileStream(path, FileMode.Open, FileAccess.Read);
                    bf = new BinaryFormatter();
                    List<object> ls = bf.Deserialize(fs) as List<object>;
                    int index = 0;
                    SetDefault();
                    SetDefaultFont();
                    if ( ls[index] is Dictionary<string, double>)
                    {
                        Dictionary<string, double> temp = ls[index] as Dictionary<string, double>;
                        foreach ( KeyValuePair<string, double> pair in temp )
                        {
                            if ( setting.ContainsKey(pair.Key) == true )
                            {
                                setting[pair.Key] = pair.Value;
                            }
                        }
                        index++;
                    }
                    if ( index < ls.Count )
                    {
                        if (ls[index] is bool)
                        {
                            extraLeft = (bool)ls[index];
                        }
                        index++;
                    }
                    if ( index < ls.Count)
                    {
                        if (ls[index] is bool)
                        {
                            extraRight = (bool)ls[index];
                        }
                        index++;
                    }
                    if (index < ls.Count)
                    {
                        if (ls[index] is bool)
                        {
                            dividingLine = (bool)ls[index];
                        }
                        index++;
                    }
                    if ( index < ls.Count )
                    {
                        Dictionary<string, Font> temp = ls[index] as Dictionary<string, Font>;
                        foreach (KeyValuePair<string, Font> pair in temp)
                        {
                            if (ItemsFonts.ContainsKey(pair.Key) == true)
                            {
                                ItemsFonts[pair.Key] = pair.Value;
                            }
                        }
                        index++;
                    }
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (fs != null)
                    fs.Close();
                if (bf != null)
                    bf = null;
            }
        }
        public static void WriteSettings(string path)
        {
            try
            {
                if (setting.Count == 0)
                {
                    throw new Exception("Проблеми з налаштуваннями!");
                }
                BinaryFormatter bf = null;
                using (FileStream fs = File.Create(path))
                {
                    try
                    {
                        bf = new BinaryFormatter();
                        List<object> ls = new List<object>
                        {
                            setting,
                            extraLeft,
                            extraRight,
                            dividingLine,
                            ItemsFonts
                        };
                        bf.Serialize(fs, ls);
                    }
                    finally
                    {
                        if (fs != null) { fs.Close(); }
                        if (bf != null) { bf = null; }
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorMessage.LogError(ex);
                throw new Exception(ex.Message);
            }
        }
        public static void SetDefault()
        {
            extraLeft = false;
            extraRight = false;
            dividingLine = false;
            setting.Clear();
            /////////////////////////////////
            //Front
            /////////////////////////////////
            setting.Add("frontSerNum", 16);
            setting.Add("frontSeriesLeft", 30);
            setting.Add("frontNumberLeft", 45);
            setting.Add("front9FinishLeft", 17);
            setting.Add("front9FinishUp", 37);
            setting.Add("front9YearLeft", 31);
            setting.Add("front9YearUp", 37);
            setting.Add("front11FinishLeft", 15);
            setting.Add("front11FinishUp", 44);
            setting.Add("front11YearLeft", 80);
            setting.Add("front11YearUp", 44);
            setting.Add("frontSL_FLine", 74);
            setting.Add("frontSL_LSub", 5);
            setting.Add("frontSL_LRate", 73);
            setting.Add("frontSL_LLine", 138);
            setting.Add("frontNamesLBoth", 5);
            setting.Add("frontNamesFUp", 25);
            setting.Add("frontNamesSUp", 31);
            setting.Add("frontSchoolFUp", 42);
            setting.Add("frontSchoolSUp", 48);
            setting.Add("frontSchoolEL", 38);
            setting.Add("frontSchoolEUp", 55);
            setting.Add("frontEduLeft", 125);
            setting.Add("frontEduUp", 83);
            setting.Add("frontSR_FLine", 9);
            setting.Add("frontSR_LSub", 110);
            setting.Add("frontSR_LRate", 180);
            setting.Add("frontSR_LLine", 75);
            setting.Add("frontSR_FLDPA", 109);
            //
            setting.Add("sizeOfLine", 97);
            setting.Add("sizeOfLineSub", 65);
            setting.Add("sizeOfLineRate", 25);
            //
            setting.Add("DividingLineUp", 1);
            setting.Add("DividingLineSize", 1);
            setting.Add("WidthOfLine", .25f);
            /////////////////////////////////
            //Back
            /////////////////////////////////
            setting.Add("backLearnLeft", 38);
            setting.Add("backLearnUp", 11);
            setting.Add("backFacultLeftAll", 14);
            setting.Add("backFacultFUp", 21);
            setting.Add("backFacultSUp", 50);
            setting.Add("backDateAllUp", 116);
            setting.Add("backDateNum", 14);
            setting.Add("backDateMonth", 26);
            setting.Add("backDateYear", 61);
            //
            setting.Add("back9GetLeft", 78);
            setting.Add("back9GetUp", 54.5);
            setting.Add("back9GetUp_LLow", 59.5);
            setting.Add("back9AwardBothUp", 77);
            setting.Add("back9AwardShortLeft", 78);
            setting.Add("back9DirectorLeft", 38);
            setting.Add("back9DirectorUp", 96);
            setting.Add("back9RegNLeft", 56);
            setting.Add("back9RegNUp", 131);
            //
            setting.Add("back11AchiveLeft", 19);
            setting.Add("back11AchiveUp", 58);
            setting.Add("back11AwardedLeft", 47);
            setting.Add("back11AwardedUp", 64);
            setting.Add("back11MedalUp", 70);
            setting.Add("back11LearnLeft", 28);
            setting.Add("back11LearnUp", 83);
            setting.Add("back11DiplomLeft", 32);
            setting.Add("back11DiplomUp", 89);
            setting.Add("back11DirectorLeft", 37);
            setting.Add("back11DirectorUp", 103);
            setting.Add("back11RegNLeft", 54);
            setting.Add("back11RegNUp", 131);
        }
        public static void SetDefaultFont()
        {
            ItemsFonts.Clear();
            ItemsFonts.Add("Серія та номер документа...", new Font("Arial", 11, FontStyle.Bold));
            ItemsFonts.Add("Прізвище, ім'я та по батькові...", new Font("Arial", 13, FontStyle.Bold | FontStyle.Italic));
            ItemsFonts.Add("Назва навчального закладу...", new Font("Arial", 12, FontStyle.Bold));
            ItemsFonts.Add("Предмети та оцінки...", new Font("Arial", 12, FontStyle.Bold | FontStyle.Italic));
            ItemsFonts.Add("Задня сторінка...", new Font("Arial", 12, FontStyle.Bold | FontStyle.Italic));
            ItemsFonts.Add("Факультативні предмети...", new Font("Arial Narrow", 12, FontStyle.Bold | FontStyle.Italic));
        }
        public static XFont GetFont(int index)
        {
            int i = 0;
            foreach (KeyValuePair<string, Font> value in ItemsFonts)
            {
                if (i == index)
                {
                    return new XFont(value.Value.Name, PtIntoMm(value.Value.Size), (XFontStyle)value.Value.Style, options);
                }
                ++i;
            }
            throw new ArgumentOutOfRangeException();
        }
    }
}
