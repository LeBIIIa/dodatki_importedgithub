﻿namespace Dodatki
{
    partial class SettingsModel
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SettingsModel));
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.WidthOfLine = new System.Windows.Forms.NumericUpDown();
            this.DividingLabel3 = new System.Windows.Forms.Label();
            this.DividingLineSize = new System.Windows.Forms.NumericUpDown();
            this.DividingLine = new System.Windows.Forms.CheckBox();
            this.DividingLabel2 = new System.Windows.Forms.Label();
            this.DividingLineUp = new System.Windows.Forms.NumericUpDown();
            this.DividingLabel = new System.Windows.Forms.Label();
            this.extraRightLine = new System.Windows.Forms.CheckBox();
            this.frontSR_FLDPA = new System.Windows.Forms.NumericUpDown();
            this.label20 = new System.Windows.Forms.Label();
            this.frontSR_LLine = new System.Windows.Forms.NumericUpDown();
            this.label17 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.frontSR_LRate = new System.Windows.Forms.NumericUpDown();
            this.label26 = new System.Windows.Forms.Label();
            this.frontSR_LSub = new System.Windows.Forms.NumericUpDown();
            this.label27 = new System.Windows.Forms.Label();
            this.frontSR_FLine = new System.Windows.Forms.NumericUpDown();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.label22 = new System.Windows.Forms.Label();
            this.frontEduUp = new System.Windows.Forms.NumericUpDown();
            this.label23 = new System.Windows.Forms.Label();
            this.frontEduLeft = new System.Windows.Forms.NumericUpDown();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.label19 = new System.Windows.Forms.Label();
            this.frontSchoolEUp = new System.Windows.Forms.NumericUpDown();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.label16 = new System.Windows.Forms.Label();
            this.frontSchoolSUp = new System.Windows.Forms.NumericUpDown();
            this.label18 = new System.Windows.Forms.Label();
            this.frontSchoolFUp = new System.Windows.Forms.NumericUpDown();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.sizeOfLine = new System.Windows.Forms.NumericUpDown();
            this.label21 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.frontNamesFUp = new System.Windows.Forms.NumericUpDown();
            this.label14 = new System.Windows.Forms.Label();
            this.frontNamesSUp = new System.Windows.Forms.NumericUpDown();
            this.label15 = new System.Windows.Forms.Label();
            this.frontNamesLBoth = new System.Windows.Forms.NumericUpDown();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.sizeOfLineRate = new System.Windows.Forms.NumericUpDown();
            this.sizeOfLineSub = new System.Windows.Forms.NumericUpDown();
            this.label63 = new System.Windows.Forms.Label();
            this.label58 = new System.Windows.Forms.Label();
            this.extraLeftLine = new System.Windows.Forms.CheckBox();
            this.frontSL_LLine = new System.Windows.Forms.NumericUpDown();
            this.label31 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.frontSL_LRate = new System.Windows.Forms.NumericUpDown();
            this.label11 = new System.Windows.Forms.Label();
            this.frontSL_LSub = new System.Windows.Forms.NumericUpDown();
            this.label12 = new System.Windows.Forms.Label();
            this.frontSL_FLine = new System.Windows.Forms.NumericUpDown();
            this.tabControl2 = new System.Windows.Forms.TabControl();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label8 = new System.Windows.Forms.Label();
            this.front9YearUp = new System.Windows.Forms.NumericUpDown();
            this.label9 = new System.Windows.Forms.Label();
            this.front9YearLeft = new System.Windows.Forms.NumericUpDown();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label6 = new System.Windows.Forms.Label();
            this.front9FinishUp = new System.Windows.Forms.NumericUpDown();
            this.label7 = new System.Windows.Forms.Label();
            this.front9FinishLeft = new System.Windows.Forms.NumericUpDown();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this.label24 = new System.Windows.Forms.Label();
            this.front11YearUp = new System.Windows.Forms.NumericUpDown();
            this.label28 = new System.Windows.Forms.Label();
            this.front11YearLeft = new System.Windows.Forms.NumericUpDown();
            this.groupBox11 = new System.Windows.Forms.GroupBox();
            this.label29 = new System.Windows.Forms.Label();
            this.front11FinishUp = new System.Windows.Forms.NumericUpDown();
            this.label30 = new System.Windows.Forms.Label();
            this.front11FinishLeft = new System.Windows.Forms.NumericUpDown();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label5 = new System.Windows.Forms.Label();
            this.frontNumberLeft = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            this.frontSeriesLeft = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.frontSerNum = new System.Windows.Forms.NumericUpDown();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.groupBox15 = new System.Windows.Forms.GroupBox();
            this.label41 = new System.Windows.Forms.Label();
            this.backLearnUp = new System.Windows.Forms.NumericUpDown();
            this.label42 = new System.Windows.Forms.Label();
            this.backLearnLeft = new System.Windows.Forms.NumericUpDown();
            this.tabControl3 = new System.Windows.Forms.TabControl();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.groupBox18 = new System.Windows.Forms.GroupBox();
            this.label36 = new System.Windows.Forms.Label();
            this.back9RegNUp = new System.Windows.Forms.NumericUpDown();
            this.label39 = new System.Windows.Forms.Label();
            this.back9RegNLeft = new System.Windows.Forms.NumericUpDown();
            this.groupBox14 = new System.Windows.Forms.GroupBox();
            this.label34 = new System.Windows.Forms.Label();
            this.back9DirectorUp = new System.Windows.Forms.NumericUpDown();
            this.label35 = new System.Windows.Forms.Label();
            this.back9DirectorLeft = new System.Windows.Forms.NumericUpDown();
            this.groupBox12 = new System.Windows.Forms.GroupBox();
            this.label32 = new System.Windows.Forms.Label();
            this.back9AwardShortLeft = new System.Windows.Forms.NumericUpDown();
            this.label33 = new System.Windows.Forms.Label();
            this.back9AwardBothUp = new System.Windows.Forms.NumericUpDown();
            this.groupBox19 = new System.Windows.Forms.GroupBox();
            this.label47 = new System.Windows.Forms.Label();
            this.back9GetUp_LLow = new System.Windows.Forms.NumericUpDown();
            this.label40 = new System.Windows.Forms.Label();
            this.label52 = new System.Windows.Forms.Label();
            this.back9GetUp = new System.Windows.Forms.NumericUpDown();
            this.label53 = new System.Windows.Forms.Label();
            this.back9GetLeft = new System.Windows.Forms.NumericUpDown();
            this.tabPage6 = new System.Windows.Forms.TabPage();
            this.groupBox24 = new System.Windows.Forms.GroupBox();
            this.label59 = new System.Windows.Forms.Label();
            this.back11DirectorUp = new System.Windows.Forms.NumericUpDown();
            this.label60 = new System.Windows.Forms.Label();
            this.back11DirectorLeft = new System.Windows.Forms.NumericUpDown();
            this.groupBox27 = new System.Windows.Forms.GroupBox();
            this.label65 = new System.Windows.Forms.Label();
            this.back11RegNUp = new System.Windows.Forms.NumericUpDown();
            this.label66 = new System.Windows.Forms.Label();
            this.back11RegNLeft = new System.Windows.Forms.NumericUpDown();
            this.groupBox22 = new System.Windows.Forms.GroupBox();
            this.label48 = new System.Windows.Forms.Label();
            this.back11LearnUp = new System.Windows.Forms.NumericUpDown();
            this.label50 = new System.Windows.Forms.Label();
            this.back11LearnLeft = new System.Windows.Forms.NumericUpDown();
            this.groupBox25 = new System.Windows.Forms.GroupBox();
            this.label61 = new System.Windows.Forms.Label();
            this.back11DiplomUp = new System.Windows.Forms.NumericUpDown();
            this.label62 = new System.Windows.Forms.Label();
            this.back11DiplomLeft = new System.Windows.Forms.NumericUpDown();
            this.groupBox20 = new System.Windows.Forms.GroupBox();
            this.label54 = new System.Windows.Forms.Label();
            this.back11AwardedUp = new System.Windows.Forms.NumericUpDown();
            this.label55 = new System.Windows.Forms.Label();
            this.back11AwardedLeft = new System.Windows.Forms.NumericUpDown();
            this.groupBox23 = new System.Windows.Forms.GroupBox();
            this.label51 = new System.Windows.Forms.Label();
            this.back11MedalUp = new System.Windows.Forms.NumericUpDown();
            this.groupBox21 = new System.Windows.Forms.GroupBox();
            this.label56 = new System.Windows.Forms.Label();
            this.back11AchiveUp = new System.Windows.Forms.NumericUpDown();
            this.label57 = new System.Windows.Forms.Label();
            this.back11AchiveLeft = new System.Windows.Forms.NumericUpDown();
            this.groupBox16 = new System.Windows.Forms.GroupBox();
            this.label43 = new System.Windows.Forms.Label();
            this.backFacultFUp = new System.Windows.Forms.NumericUpDown();
            this.label44 = new System.Windows.Forms.Label();
            this.backFacultSUp = new System.Windows.Forms.NumericUpDown();
            this.label45 = new System.Windows.Forms.Label();
            this.backFacultLeftAll = new System.Windows.Forms.NumericUpDown();
            this.groupBox17 = new System.Windows.Forms.GroupBox();
            this.label49 = new System.Windows.Forms.Label();
            this.groupBox13 = new System.Windows.Forms.GroupBox();
            this.label46 = new System.Windows.Forms.Label();
            this.backDateMonth = new System.Windows.Forms.NumericUpDown();
            this.label37 = new System.Windows.Forms.Label();
            this.backDateYear = new System.Windows.Forms.NumericUpDown();
            this.label38 = new System.Windows.Forms.Label();
            this.backDateNum = new System.Windows.Forms.NumericUpDown();
            this.backDateAllUp = new System.Windows.Forms.NumericUpDown();
            this.tabPage7 = new System.Windows.Forms.TabPage();
            this.groupBox32 = new System.Windows.Forms.GroupBox();
            this.selected6 = new System.Windows.Forms.Label();
            this.btn6 = new System.Windows.Forms.Button();
            this.groupBox30 = new System.Windows.Forms.GroupBox();
            this.selected5 = new System.Windows.Forms.Label();
            this.btn5 = new System.Windows.Forms.Button();
            this.groupBox31 = new System.Windows.Forms.GroupBox();
            this.selected4 = new System.Windows.Forms.Label();
            this.btn4 = new System.Windows.Forms.Button();
            this.groupBox29 = new System.Windows.Forms.GroupBox();
            this.selected3 = new System.Windows.Forms.Label();
            this.btn3 = new System.Windows.Forms.Button();
            this.groupBox28 = new System.Windows.Forms.GroupBox();
            this.selected2 = new System.Windows.Forms.Label();
            this.btn2 = new System.Windows.Forms.Button();
            this.groupBox26 = new System.Windows.Forms.GroupBox();
            this.selected1 = new System.Windows.Forms.Label();
            this.btn1 = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.saveAs = new System.Windows.Forms.Button();
            this.open = new System.Windows.Forms.Button();
            this.apply = new System.Windows.Forms.Button();
            this.cancel = new System.Windows.Forms.Button();
            this.OK = new System.Windows.Forms.Button();
            this.byDefault = new System.Windows.Forms.Button();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.groupBox9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.WidthOfLine)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DividingLineSize)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DividingLineUp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.frontSR_FLDPA)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.frontSR_LLine)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.frontSR_LRate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.frontSR_LSub)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.frontSR_FLine)).BeginInit();
            this.groupBox8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.frontEduUp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.frontEduLeft)).BeginInit();
            this.groupBox7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.frontSchoolEUp)).BeginInit();
            this.groupBox6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.frontSchoolSUp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.frontSchoolFUp)).BeginInit();
            this.groupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.sizeOfLine)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.frontNamesFUp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.frontNamesSUp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.frontNamesLBoth)).BeginInit();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.sizeOfLineRate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sizeOfLineSub)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.frontSL_LLine)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.frontSL_LRate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.frontSL_LSub)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.frontSL_FLine)).BeginInit();
            this.tabControl2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.front9YearUp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.front9YearLeft)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.front9FinishUp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.front9FinishLeft)).BeginInit();
            this.tabPage4.SuspendLayout();
            this.groupBox10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.front11YearUp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.front11YearLeft)).BeginInit();
            this.groupBox11.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.front11FinishUp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.front11FinishLeft)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.frontNumberLeft)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.frontSeriesLeft)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.frontSerNum)).BeginInit();
            this.tabPage2.SuspendLayout();
            this.groupBox15.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.backLearnUp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.backLearnLeft)).BeginInit();
            this.tabControl3.SuspendLayout();
            this.tabPage5.SuspendLayout();
            this.groupBox18.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.back9RegNUp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.back9RegNLeft)).BeginInit();
            this.groupBox14.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.back9DirectorUp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.back9DirectorLeft)).BeginInit();
            this.groupBox12.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.back9AwardShortLeft)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.back9AwardBothUp)).BeginInit();
            this.groupBox19.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.back9GetUp_LLow)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.back9GetUp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.back9GetLeft)).BeginInit();
            this.tabPage6.SuspendLayout();
            this.groupBox24.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.back11DirectorUp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.back11DirectorLeft)).BeginInit();
            this.groupBox27.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.back11RegNUp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.back11RegNLeft)).BeginInit();
            this.groupBox22.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.back11LearnUp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.back11LearnLeft)).BeginInit();
            this.groupBox25.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.back11DiplomUp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.back11DiplomLeft)).BeginInit();
            this.groupBox20.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.back11AwardedUp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.back11AwardedLeft)).BeginInit();
            this.groupBox23.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.back11MedalUp)).BeginInit();
            this.groupBox21.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.back11AchiveUp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.back11AchiveLeft)).BeginInit();
            this.groupBox16.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.backFacultFUp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.backFacultSUp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.backFacultLeftAll)).BeginInit();
            this.groupBox17.SuspendLayout();
            this.groupBox13.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.backDateMonth)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.backDateYear)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.backDateNum)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.backDateAllUp)).BeginInit();
            this.tabPage7.SuspendLayout();
            this.groupBox32.SuspendLayout();
            this.groupBox30.SuspendLayout();
            this.groupBox31.SuspendLayout();
            this.groupBox29.SuspendLayout();
            this.groupBox28.SuspendLayout();
            this.groupBox26.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage7);
            this.tabControl1.Location = new System.Drawing.Point(12, 62);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(640, 420);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.groupBox9);
            this.tabPage1.Controls.Add(this.groupBox8);
            this.tabPage1.Controls.Add(this.groupBox7);
            this.tabPage1.Controls.Add(this.groupBox6);
            this.tabPage1.Controls.Add(this.groupBox5);
            this.tabPage1.Controls.Add(this.groupBox4);
            this.tabPage1.Controls.Add(this.tabControl2);
            this.tabPage1.Controls.Add(this.groupBox1);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(632, 394);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Розворот з оцінками";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this.WidthOfLine);
            this.groupBox9.Controls.Add(this.DividingLabel3);
            this.groupBox9.Controls.Add(this.DividingLineSize);
            this.groupBox9.Controls.Add(this.DividingLine);
            this.groupBox9.Controls.Add(this.DividingLabel2);
            this.groupBox9.Controls.Add(this.DividingLineUp);
            this.groupBox9.Controls.Add(this.DividingLabel);
            this.groupBox9.Controls.Add(this.extraRightLine);
            this.groupBox9.Controls.Add(this.frontSR_FLDPA);
            this.groupBox9.Controls.Add(this.label20);
            this.groupBox9.Controls.Add(this.frontSR_LLine);
            this.groupBox9.Controls.Add(this.label17);
            this.groupBox9.Controls.Add(this.label25);
            this.groupBox9.Controls.Add(this.frontSR_LRate);
            this.groupBox9.Controls.Add(this.label26);
            this.groupBox9.Controls.Add(this.frontSR_LSub);
            this.groupBox9.Controls.Add(this.label27);
            this.groupBox9.Controls.Add(this.frontSR_FLine);
            this.groupBox9.Location = new System.Drawing.Point(312, 219);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(311, 169);
            this.groupBox9.TabIndex = 10;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "Лінія предметів правої сторінки";
            // 
            // WidthOfLine
            // 
            this.WidthOfLine.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.WidthOfLine.DecimalPlaces = 2;
            this.WidthOfLine.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.WidthOfLine.Location = new System.Drawing.Point(191, 100);
            this.WidthOfLine.Margin = new System.Windows.Forms.Padding(0);
            this.WidthOfLine.Maximum = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.WidthOfLine.Name = "WidthOfLine";
            this.WidthOfLine.Size = new System.Drawing.Size(48, 20);
            this.WidthOfLine.TabIndex = 16;
            this.WidthOfLine.Visible = false;
            this.WidthOfLine.Enter += new System.EventHandler(this.NumericUpDown_Enter);
            this.WidthOfLine.MouseDown += new System.Windows.Forms.MouseEventHandler(this.NumericUpDown_MouseDown);
            // 
            // DividingLabel3
            // 
            this.DividingLabel3.AutoSize = true;
            this.DividingLabel3.Location = new System.Drawing.Point(137, 102);
            this.DividingLabel3.Margin = new System.Windows.Forms.Padding(0);
            this.DividingLabel3.Name = "DividingLabel3";
            this.DividingLabel3.Size = new System.Drawing.Size(53, 13);
            this.DividingLabel3.TabIndex = 15;
            this.DividingLabel3.Text = "Товщина";
            this.DividingLabel3.Visible = false;
            // 
            // DividingLineSize
            // 
            this.DividingLineSize.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.DividingLineSize.DecimalPlaces = 1;
            this.DividingLineSize.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.DividingLineSize.Location = new System.Drawing.Point(191, 144);
            this.DividingLineSize.Margin = new System.Windows.Forms.Padding(0);
            this.DividingLineSize.Maximum = new decimal(new int[] {
            210,
            0,
            0,
            0});
            this.DividingLineSize.Name = "DividingLineSize";
            this.DividingLineSize.Size = new System.Drawing.Size(48, 20);
            this.DividingLineSize.TabIndex = 14;
            this.DividingLineSize.Visible = false;
            this.DividingLineSize.Enter += new System.EventHandler(this.NumericUpDown_Enter);
            this.DividingLineSize.MouseDown += new System.Windows.Forms.MouseEventHandler(this.NumericUpDown_MouseDown);
            // 
            // DividingLine
            // 
            this.DividingLine.Location = new System.Drawing.Point(6, 122);
            this.DividingLine.Name = "DividingLine";
            this.DividingLine.Size = new System.Drawing.Size(129, 35);
            this.DividingLine.TabIndex = 12;
            this.DividingLine.Text = "Роздільна лінія варіативної частини";
            this.DividingLine.UseVisualStyleBackColor = true;
            this.DividingLine.CheckedChanged += new System.EventHandler(this.CheckBox_CheckedChanged);
            // 
            // DividingLabel2
            // 
            this.DividingLabel2.AutoSize = true;
            this.DividingLabel2.Location = new System.Drawing.Point(137, 146);
            this.DividingLabel2.Margin = new System.Windows.Forms.Padding(0);
            this.DividingLabel2.Name = "DividingLabel2";
            this.DividingLabel2.Size = new System.Drawing.Size(54, 13);
            this.DividingLabel2.TabIndex = 13;
            this.DividingLabel2.Text = "Довжина";
            this.DividingLabel2.Visible = false;
            // 
            // DividingLineUp
            // 
            this.DividingLineUp.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.DividingLineUp.DecimalPlaces = 1;
            this.DividingLineUp.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.DividingLineUp.Location = new System.Drawing.Point(180, 122);
            this.DividingLineUp.Margin = new System.Windows.Forms.Padding(0);
            this.DividingLineUp.Maximum = new decimal(new int[] {
            210,
            0,
            0,
            0});
            this.DividingLineUp.Name = "DividingLineUp";
            this.DividingLineUp.Size = new System.Drawing.Size(48, 20);
            this.DividingLineUp.TabIndex = 11;
            this.DividingLineUp.Visible = false;
            this.DividingLineUp.Enter += new System.EventHandler(this.NumericUpDown_Enter);
            this.DividingLineUp.MouseDown += new System.Windows.Forms.MouseEventHandler(this.NumericUpDown_MouseDown);
            // 
            // DividingLabel
            // 
            this.DividingLabel.AutoSize = true;
            this.DividingLabel.Location = new System.Drawing.Point(138, 124);
            this.DividingLabel.Margin = new System.Windows.Forms.Padding(0);
            this.DividingLabel.Name = "DividingLabel";
            this.DividingLabel.Size = new System.Drawing.Size(42, 13);
            this.DividingLabel.TabIndex = 10;
            this.DividingLabel.Text = "Зверху";
            this.DividingLabel.Visible = false;
            // 
            // extraRightLine
            // 
            this.extraRightLine.AutoSize = true;
            this.extraRightLine.Location = new System.Drawing.Point(6, 96);
            this.extraRightLine.Name = "extraRightLine";
            this.extraRightLine.Size = new System.Drawing.Size(107, 17);
            this.extraRightLine.TabIndex = 9;
            this.extraRightLine.Text = "Додаткова лінія";
            this.extraRightLine.UseVisualStyleBackColor = true;
            this.extraRightLine.CheckedChanged += new System.EventHandler(this.CheckBox_CheckedChanged);
            // 
            // frontSR_FLDPA
            // 
            this.frontSR_FLDPA.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.frontSR_FLDPA.DecimalPlaces = 1;
            this.frontSR_FLDPA.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.frontSR_FLDPA.Location = new System.Drawing.Point(217, 78);
            this.frontSR_FLDPA.Margin = new System.Windows.Forms.Padding(0);
            this.frontSR_FLDPA.Maximum = new decimal(new int[] {
            210,
            0,
            0,
            0});
            this.frontSR_FLDPA.Name = "frontSR_FLDPA";
            this.frontSR_FLDPA.Size = new System.Drawing.Size(48, 20);
            this.frontSR_FLDPA.TabIndex = 9;
            this.frontSR_FLDPA.Enter += new System.EventHandler(this.NumericUpDown_Enter);
            this.frontSR_FLDPA.MouseDown += new System.Windows.Forms.MouseEventHandler(this.NumericUpDown_MouseDown);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(34, 80);
            this.label20.Margin = new System.Windows.Forms.Padding(0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(183, 13);
            this.label20.TabIndex = 8;
            this.label20.Text = "Зверху перша лінія предметів ДПА";
            // 
            // frontSR_LLine
            // 
            this.frontSR_LLine.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.frontSR_LLine.DecimalPlaces = 1;
            this.frontSR_LLine.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.frontSR_LLine.Location = new System.Drawing.Point(178, 57);
            this.frontSR_LLine.Margin = new System.Windows.Forms.Padding(0);
            this.frontSR_LLine.Maximum = new decimal(new int[] {
            210,
            0,
            0,
            0});
            this.frontSR_LLine.Name = "frontSR_LLine";
            this.frontSR_LLine.Size = new System.Drawing.Size(48, 20);
            this.frontSR_LLine.TabIndex = 7;
            this.frontSR_LLine.Enter += new System.EventHandler(this.NumericUpDown_Enter);
            this.frontSR_LLine.MouseDown += new System.Windows.Forms.MouseEventHandler(this.NumericUpDown_MouseDown);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(63, 59);
            this.label17.Margin = new System.Windows.Forms.Padding(0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(111, 13);
            this.label17.TabIndex = 6;
            this.label17.Text = "Зверху остання лінія";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(158, 40);
            this.label25.Margin = new System.Windows.Forms.Padding(0);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(69, 13);
            this.label25.TabIndex = 5;
            this.label25.Text = "Зліва оцінки";
            // 
            // frontSR_LRate
            // 
            this.frontSR_LRate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.frontSR_LRate.DecimalPlaces = 1;
            this.frontSR_LRate.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.frontSR_LRate.Location = new System.Drawing.Point(227, 38);
            this.frontSR_LRate.Margin = new System.Windows.Forms.Padding(0);
            this.frontSR_LRate.Maximum = new decimal(new int[] {
            210,
            0,
            0,
            0});
            this.frontSR_LRate.Name = "frontSR_LRate";
            this.frontSR_LRate.Size = new System.Drawing.Size(48, 20);
            this.frontSR_LRate.TabIndex = 4;
            this.frontSR_LRate.Enter += new System.EventHandler(this.NumericUpDown_Enter);
            this.frontSR_LRate.MouseDown += new System.Windows.Forms.MouseEventHandler(this.NumericUpDown_MouseDown);
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(13, 40);
            this.label26.Margin = new System.Windows.Forms.Padding(0);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(86, 13);
            this.label26.TabIndex = 3;
            this.label26.Text = "Зліва предмети";
            // 
            // frontSR_LSub
            // 
            this.frontSR_LSub.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.frontSR_LSub.DecimalPlaces = 1;
            this.frontSR_LSub.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.frontSR_LSub.Location = new System.Drawing.Point(101, 38);
            this.frontSR_LSub.Margin = new System.Windows.Forms.Padding(0);
            this.frontSR_LSub.Maximum = new decimal(new int[] {
            210,
            0,
            0,
            0});
            this.frontSR_LSub.Name = "frontSR_LSub";
            this.frontSR_LSub.Size = new System.Drawing.Size(48, 20);
            this.frontSR_LSub.TabIndex = 2;
            this.frontSR_LSub.Enter += new System.EventHandler(this.NumericUpDown_Enter);
            this.frontSR_LSub.MouseDown += new System.Windows.Forms.MouseEventHandler(this.NumericUpDown_MouseDown);
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(77, 18);
            this.label27.Margin = new System.Windows.Forms.Padding(0);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(102, 13);
            this.label27.TabIndex = 1;
            this.label27.Text = "Зверху перша лінія";
            // 
            // frontSR_FLine
            // 
            this.frontSR_FLine.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.frontSR_FLine.DecimalPlaces = 1;
            this.frontSR_FLine.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.frontSR_FLine.Location = new System.Drawing.Point(179, 16);
            this.frontSR_FLine.Margin = new System.Windows.Forms.Padding(0);
            this.frontSR_FLine.Maximum = new decimal(new int[] {
            210,
            0,
            0,
            0});
            this.frontSR_FLine.Name = "frontSR_FLine";
            this.frontSR_FLine.Size = new System.Drawing.Size(48, 20);
            this.frontSR_FLine.TabIndex = 0;
            this.frontSR_FLine.Enter += new System.EventHandler(this.NumericUpDown_Enter);
            this.frontSR_FLine.MouseDown += new System.Windows.Forms.MouseEventHandler(this.NumericUpDown_MouseDown);
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.label22);
            this.groupBox8.Controls.Add(this.frontEduUp);
            this.groupBox8.Controls.Add(this.label23);
            this.groupBox8.Controls.Add(this.frontEduLeft);
            this.groupBox8.Location = new System.Drawing.Point(339, 170);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(290, 42);
            this.groupBox8.TabIndex = 9;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "Лінія \"пройш_____ державну...\"";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(190, 16);
            this.label22.Margin = new System.Windows.Forms.Padding(0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(42, 13);
            this.label22.TabIndex = 5;
            this.label22.Text = "Зверху";
            // 
            // frontEduUp
            // 
            this.frontEduUp.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.frontEduUp.DecimalPlaces = 1;
            this.frontEduUp.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.frontEduUp.Location = new System.Drawing.Point(236, 14);
            this.frontEduUp.Margin = new System.Windows.Forms.Padding(0);
            this.frontEduUp.Maximum = new decimal(new int[] {
            210,
            0,
            0,
            0});
            this.frontEduUp.Name = "frontEduUp";
            this.frontEduUp.Size = new System.Drawing.Size(48, 20);
            this.frontEduUp.TabIndex = 4;
            this.frontEduUp.Enter += new System.EventHandler(this.NumericUpDown_Enter);
            this.frontEduUp.MouseDown += new System.Windows.Forms.MouseEventHandler(this.NumericUpDown_MouseDown);
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(59, 18);
            this.label23.Margin = new System.Windows.Forms.Padding(0);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(34, 13);
            this.label23.TabIndex = 3;
            this.label23.Text = "Зліва";
            // 
            // frontEduLeft
            // 
            this.frontEduLeft.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.frontEduLeft.DecimalPlaces = 1;
            this.frontEduLeft.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.frontEduLeft.Location = new System.Drawing.Point(93, 16);
            this.frontEduLeft.Margin = new System.Windows.Forms.Padding(0);
            this.frontEduLeft.Maximum = new decimal(new int[] {
            210,
            0,
            0,
            0});
            this.frontEduLeft.Name = "frontEduLeft";
            this.frontEduLeft.Size = new System.Drawing.Size(48, 20);
            this.frontEduLeft.TabIndex = 2;
            this.frontEduLeft.Enter += new System.EventHandler(this.NumericUpDown_Enter);
            this.frontEduLeft.MouseDown += new System.Windows.Forms.MouseEventHandler(this.NumericUpDown_MouseDown);
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.label19);
            this.groupBox7.Controls.Add(this.frontSchoolEUp);
            this.groupBox7.Location = new System.Drawing.Point(339, 120);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(287, 42);
            this.groupBox7.TabIndex = 8;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Додаткова лінія назви знизу(не стандарт)";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(190, 18);
            this.label19.Margin = new System.Windows.Forms.Padding(0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(42, 13);
            this.label19.TabIndex = 5;
            this.label19.Text = "Зверху";
            // 
            // frontSchoolEUp
            // 
            this.frontSchoolEUp.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.frontSchoolEUp.DecimalPlaces = 1;
            this.frontSchoolEUp.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.frontSchoolEUp.Location = new System.Drawing.Point(236, 16);
            this.frontSchoolEUp.Margin = new System.Windows.Forms.Padding(0);
            this.frontSchoolEUp.Maximum = new decimal(new int[] {
            210,
            0,
            0,
            0});
            this.frontSchoolEUp.Name = "frontSchoolEUp";
            this.frontSchoolEUp.Size = new System.Drawing.Size(48, 20);
            this.frontSchoolEUp.TabIndex = 4;
            this.frontSchoolEUp.Enter += new System.EventHandler(this.NumericUpDown_Enter);
            this.frontSchoolEUp.MouseDown += new System.Windows.Forms.MouseEventHandler(this.NumericUpDown_MouseDown);
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.label16);
            this.groupBox6.Controls.Add(this.frontSchoolSUp);
            this.groupBox6.Controls.Add(this.label18);
            this.groupBox6.Controls.Add(this.frontSchoolFUp);
            this.groupBox6.Location = new System.Drawing.Point(339, 72);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(287, 42);
            this.groupBox6.TabIndex = 7;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Лінія назви навчального закладу";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(160, 18);
            this.label16.Margin = new System.Windows.Forms.Padding(0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(76, 13);
            this.label16.TabIndex = 5;
            this.label16.Text = "Зверху(друга)";
            // 
            // frontSchoolSUp
            // 
            this.frontSchoolSUp.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.frontSchoolSUp.DecimalPlaces = 1;
            this.frontSchoolSUp.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.frontSchoolSUp.Location = new System.Drawing.Point(236, 16);
            this.frontSchoolSUp.Margin = new System.Windows.Forms.Padding(0);
            this.frontSchoolSUp.Maximum = new decimal(new int[] {
            210,
            0,
            0,
            0});
            this.frontSchoolSUp.Name = "frontSchoolSUp";
            this.frontSchoolSUp.Size = new System.Drawing.Size(48, 20);
            this.frontSchoolSUp.TabIndex = 4;
            this.frontSchoolSUp.Enter += new System.EventHandler(this.NumericUpDown_Enter);
            this.frontSchoolSUp.MouseDown += new System.Windows.Forms.MouseEventHandler(this.NumericUpDown_MouseDown);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(19, 18);
            this.label18.Margin = new System.Windows.Forms.Padding(0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(80, 13);
            this.label18.TabIndex = 1;
            this.label18.Text = "Зверху(перша)";
            // 
            // frontSchoolFUp
            // 
            this.frontSchoolFUp.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.frontSchoolFUp.DecimalPlaces = 1;
            this.frontSchoolFUp.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.frontSchoolFUp.Location = new System.Drawing.Point(99, 16);
            this.frontSchoolFUp.Margin = new System.Windows.Forms.Padding(0);
            this.frontSchoolFUp.Maximum = new decimal(new int[] {
            210,
            0,
            0,
            0});
            this.frontSchoolFUp.Name = "frontSchoolFUp";
            this.frontSchoolFUp.Size = new System.Drawing.Size(48, 20);
            this.frontSchoolFUp.TabIndex = 0;
            this.frontSchoolFUp.Enter += new System.EventHandler(this.NumericUpDown_Enter);
            this.frontSchoolFUp.MouseDown += new System.Windows.Forms.MouseEventHandler(this.NumericUpDown_MouseDown);
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.sizeOfLine);
            this.groupBox5.Controls.Add(this.label21);
            this.groupBox5.Controls.Add(this.label13);
            this.groupBox5.Controls.Add(this.frontNamesFUp);
            this.groupBox5.Controls.Add(this.label14);
            this.groupBox5.Controls.Add(this.frontNamesSUp);
            this.groupBox5.Controls.Add(this.label15);
            this.groupBox5.Controls.Add(this.frontNamesLBoth);
            this.groupBox5.Location = new System.Drawing.Point(339, 6);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(287, 60);
            this.groupBox5.TabIndex = 6;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Лінія ПІБ";
            // 
            // sizeOfLine
            // 
            this.sizeOfLine.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.sizeOfLine.DecimalPlaces = 1;
            this.sizeOfLine.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.sizeOfLine.Location = new System.Drawing.Point(99, 37);
            this.sizeOfLine.Margin = new System.Windows.Forms.Padding(0);
            this.sizeOfLine.Maximum = new decimal(new int[] {
            210,
            0,
            0,
            0});
            this.sizeOfLine.Name = "sizeOfLine";
            this.sizeOfLine.Size = new System.Drawing.Size(48, 20);
            this.sizeOfLine.TabIndex = 7;
            this.sizeOfLine.Enter += new System.EventHandler(this.NumericUpDown_Enter);
            this.sizeOfLine.MouseDown += new System.Windows.Forms.MouseEventHandler(this.NumericUpDown_MouseDown);
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(23, 39);
            this.label21.Margin = new System.Windows.Forms.Padding(0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(76, 13);
            this.label21.TabIndex = 6;
            this.label21.Text = "Довжина лінії";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(156, 16);
            this.label13.Margin = new System.Windows.Forms.Padding(0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(80, 13);
            this.label13.TabIndex = 5;
            this.label13.Text = "Зверху(перша)";
            // 
            // frontNamesFUp
            // 
            this.frontNamesFUp.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.frontNamesFUp.DecimalPlaces = 1;
            this.frontNamesFUp.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.frontNamesFUp.Location = new System.Drawing.Point(236, 11);
            this.frontNamesFUp.Margin = new System.Windows.Forms.Padding(0);
            this.frontNamesFUp.Maximum = new decimal(new int[] {
            210,
            0,
            0,
            0});
            this.frontNamesFUp.Name = "frontNamesFUp";
            this.frontNamesFUp.Size = new System.Drawing.Size(48, 20);
            this.frontNamesFUp.TabIndex = 4;
            this.frontNamesFUp.Enter += new System.EventHandler(this.NumericUpDown_Enter);
            this.frontNamesFUp.MouseDown += new System.Windows.Forms.MouseEventHandler(this.NumericUpDown_MouseDown);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(160, 39);
            this.label14.Margin = new System.Windows.Forms.Padding(0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(76, 13);
            this.label14.TabIndex = 3;
            this.label14.Text = "Зверху(друга)";
            // 
            // frontNamesSUp
            // 
            this.frontNamesSUp.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.frontNamesSUp.DecimalPlaces = 1;
            this.frontNamesSUp.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.frontNamesSUp.Location = new System.Drawing.Point(236, 37);
            this.frontNamesSUp.Margin = new System.Windows.Forms.Padding(0);
            this.frontNamesSUp.Maximum = new decimal(new int[] {
            210,
            0,
            0,
            0});
            this.frontNamesSUp.Name = "frontNamesSUp";
            this.frontNamesSUp.Size = new System.Drawing.Size(48, 20);
            this.frontNamesSUp.TabIndex = 2;
            this.frontNamesSUp.Enter += new System.EventHandler(this.NumericUpDown_Enter);
            this.frontNamesSUp.MouseDown += new System.Windows.Forms.MouseEventHandler(this.NumericUpDown_MouseDown);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(27, 16);
            this.label15.Margin = new System.Windows.Forms.Padding(0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(72, 13);
            this.label15.TabIndex = 1;
            this.label15.Text = "Зліва(обидві)";
            // 
            // frontNamesLBoth
            // 
            this.frontNamesLBoth.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.frontNamesLBoth.DecimalPlaces = 1;
            this.frontNamesLBoth.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.frontNamesLBoth.Location = new System.Drawing.Point(99, 11);
            this.frontNamesLBoth.Margin = new System.Windows.Forms.Padding(0);
            this.frontNamesLBoth.Maximum = new decimal(new int[] {
            210,
            0,
            0,
            0});
            this.frontNamesLBoth.Name = "frontNamesLBoth";
            this.frontNamesLBoth.Size = new System.Drawing.Size(48, 20);
            this.frontNamesLBoth.TabIndex = 0;
            this.frontNamesLBoth.Enter += new System.EventHandler(this.NumericUpDown_Enter);
            this.frontNamesLBoth.MouseDown += new System.Windows.Forms.MouseEventHandler(this.NumericUpDown_MouseDown);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.sizeOfLineRate);
            this.groupBox4.Controls.Add(this.sizeOfLineSub);
            this.groupBox4.Controls.Add(this.label63);
            this.groupBox4.Controls.Add(this.label58);
            this.groupBox4.Controls.Add(this.extraLeftLine);
            this.groupBox4.Controls.Add(this.frontSL_LLine);
            this.groupBox4.Controls.Add(this.label31);
            this.groupBox4.Controls.Add(this.label10);
            this.groupBox4.Controls.Add(this.frontSL_LRate);
            this.groupBox4.Controls.Add(this.label11);
            this.groupBox4.Controls.Add(this.frontSL_LSub);
            this.groupBox4.Controls.Add(this.label12);
            this.groupBox4.Controls.Add(this.frontSL_FLine);
            this.groupBox4.Location = new System.Drawing.Point(10, 233);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(296, 154);
            this.groupBox4.TabIndex = 6;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Лінія предметів лівої сторінки";
            // 
            // sizeOfLineRate
            // 
            this.sizeOfLineRate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.sizeOfLineRate.DecimalPlaces = 1;
            this.sizeOfLineRate.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.sizeOfLineRate.Location = new System.Drawing.Point(112, 125);
            this.sizeOfLineRate.Margin = new System.Windows.Forms.Padding(0);
            this.sizeOfLineRate.Maximum = new decimal(new int[] {
            210,
            0,
            0,
            0});
            this.sizeOfLineRate.Name = "sizeOfLineRate";
            this.sizeOfLineRate.Size = new System.Drawing.Size(48, 20);
            this.sizeOfLineRate.TabIndex = 12;
            this.sizeOfLineRate.Enter += new System.EventHandler(this.NumericUpDown_Enter);
            this.sizeOfLineRate.MouseDown += new System.Windows.Forms.MouseEventHandler(this.NumericUpDown_MouseDown);
            // 
            // sizeOfLineSub
            // 
            this.sizeOfLineSub.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.sizeOfLineSub.DecimalPlaces = 1;
            this.sizeOfLineSub.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.sizeOfLineSub.Location = new System.Drawing.Point(133, 103);
            this.sizeOfLineSub.Margin = new System.Windows.Forms.Padding(0);
            this.sizeOfLineSub.Maximum = new decimal(new int[] {
            210,
            0,
            0,
            0});
            this.sizeOfLineSub.Name = "sizeOfLineSub";
            this.sizeOfLineSub.Size = new System.Drawing.Size(48, 20);
            this.sizeOfLineSub.TabIndex = 11;
            this.sizeOfLineSub.Enter += new System.EventHandler(this.NumericUpDown_Enter);
            this.sizeOfLineSub.MouseDown += new System.Windows.Forms.MouseEventHandler(this.NumericUpDown_MouseDown);
            // 
            // label63
            // 
            this.label63.AutoSize = true;
            this.label63.Location = new System.Drawing.Point(3, 127);
            this.label63.Margin = new System.Windows.Forms.Padding(0);
            this.label63.Name = "label63";
            this.label63.Size = new System.Drawing.Size(111, 13);
            this.label63.TabIndex = 10;
            this.label63.Text = "Довжина лінії оцінок";
            // 
            // label58
            // 
            this.label58.AutoSize = true;
            this.label58.Location = new System.Drawing.Point(3, 105);
            this.label58.Margin = new System.Windows.Forms.Padding(0);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(130, 13);
            this.label58.TabIndex = 9;
            this.label58.Text = "Довжина лінії предметів";
            // 
            // extraLeftLine
            // 
            this.extraLeftLine.AutoSize = true;
            this.extraLeftLine.Location = new System.Drawing.Point(7, 84);
            this.extraLeftLine.Name = "extraLeftLine";
            this.extraLeftLine.Size = new System.Drawing.Size(107, 17);
            this.extraLeftLine.TabIndex = 8;
            this.extraLeftLine.Text = "Додаткова лінія";
            this.extraLeftLine.UseVisualStyleBackColor = true;
            this.extraLeftLine.CheckedChanged += new System.EventHandler(this.CheckBox_CheckedChanged);
            // 
            // frontSL_LLine
            // 
            this.frontSL_LLine.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.frontSL_LLine.DecimalPlaces = 1;
            this.frontSL_LLine.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.frontSL_LLine.Location = new System.Drawing.Point(177, 66);
            this.frontSL_LLine.Margin = new System.Windows.Forms.Padding(0);
            this.frontSL_LLine.Maximum = new decimal(new int[] {
            210,
            0,
            0,
            0});
            this.frontSL_LLine.Name = "frontSL_LLine";
            this.frontSL_LLine.Size = new System.Drawing.Size(48, 20);
            this.frontSL_LLine.TabIndex = 7;
            this.frontSL_LLine.Enter += new System.EventHandler(this.NumericUpDown_Enter);
            this.frontSL_LLine.MouseDown += new System.Windows.Forms.MouseEventHandler(this.NumericUpDown_MouseDown);
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(66, 68);
            this.label31.Margin = new System.Windows.Forms.Padding(0);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(111, 13);
            this.label31.TabIndex = 6;
            this.label31.Text = "Зверху остання лінія";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(167, 40);
            this.label10.Margin = new System.Windows.Forms.Padding(0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(69, 13);
            this.label10.TabIndex = 5;
            this.label10.Text = "Зліва оцінки";
            // 
            // frontSL_LRate
            // 
            this.frontSL_LRate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.frontSL_LRate.DecimalPlaces = 1;
            this.frontSL_LRate.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.frontSL_LRate.Location = new System.Drawing.Point(236, 38);
            this.frontSL_LRate.Margin = new System.Windows.Forms.Padding(0);
            this.frontSL_LRate.Maximum = new decimal(new int[] {
            210,
            0,
            0,
            0});
            this.frontSL_LRate.Name = "frontSL_LRate";
            this.frontSL_LRate.Size = new System.Drawing.Size(48, 20);
            this.frontSL_LRate.TabIndex = 4;
            this.frontSL_LRate.Enter += new System.EventHandler(this.NumericUpDown_Enter);
            this.frontSL_LRate.MouseDown += new System.Windows.Forms.MouseEventHandler(this.NumericUpDown_MouseDown);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(4, 40);
            this.label11.Margin = new System.Windows.Forms.Padding(0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(86, 13);
            this.label11.TabIndex = 3;
            this.label11.Text = "Зліва предмети";
            // 
            // frontSL_LSub
            // 
            this.frontSL_LSub.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.frontSL_LSub.DecimalPlaces = 1;
            this.frontSL_LSub.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.frontSL_LSub.Location = new System.Drawing.Point(90, 38);
            this.frontSL_LSub.Margin = new System.Windows.Forms.Padding(0);
            this.frontSL_LSub.Maximum = new decimal(new int[] {
            210,
            0,
            0,
            0});
            this.frontSL_LSub.Name = "frontSL_LSub";
            this.frontSL_LSub.Size = new System.Drawing.Size(48, 20);
            this.frontSL_LSub.TabIndex = 2;
            this.frontSL_LSub.Enter += new System.EventHandler(this.NumericUpDown_Enter);
            this.frontSL_LSub.MouseDown += new System.Windows.Forms.MouseEventHandler(this.NumericUpDown_MouseDown);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(58, 18);
            this.label12.Margin = new System.Windows.Forms.Padding(0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(102, 13);
            this.label12.TabIndex = 1;
            this.label12.Text = "Зверху перша лінія";
            // 
            // frontSL_FLine
            // 
            this.frontSL_FLine.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.frontSL_FLine.DecimalPlaces = 1;
            this.frontSL_FLine.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.frontSL_FLine.Location = new System.Drawing.Point(160, 16);
            this.frontSL_FLine.Margin = new System.Windows.Forms.Padding(0);
            this.frontSL_FLine.Maximum = new decimal(new int[] {
            210,
            0,
            0,
            0});
            this.frontSL_FLine.Name = "frontSL_FLine";
            this.frontSL_FLine.Size = new System.Drawing.Size(48, 20);
            this.frontSL_FLine.TabIndex = 0;
            this.frontSL_FLine.Enter += new System.EventHandler(this.NumericUpDown_Enter);
            this.frontSL_FLine.MouseDown += new System.Windows.Forms.MouseEventHandler(this.NumericUpDown_MouseDown);
            // 
            // tabControl2
            // 
            this.tabControl2.Controls.Add(this.tabPage3);
            this.tabControl2.Controls.Add(this.tabPage4);
            this.tabControl2.Location = new System.Drawing.Point(6, 72);
            this.tabControl2.Name = "tabControl2";
            this.tabControl2.SelectedIndex = 0;
            this.tabControl2.Size = new System.Drawing.Size(300, 155);
            this.tabControl2.TabIndex = 1;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.groupBox3);
            this.tabPage3.Controls.Add(this.groupBox2);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(292, 129);
            this.tabPage3.TabIndex = 0;
            this.tabPage3.Text = "9 клас";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label8);
            this.groupBox3.Controls.Add(this.front9YearUp);
            this.groupBox3.Controls.Add(this.label9);
            this.groupBox3.Controls.Add(this.front9YearLeft);
            this.groupBox3.Location = new System.Drawing.Point(6, 76);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(280, 40);
            this.groupBox3.TabIndex = 7;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Лінія \"у 20__ році\"";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(174, 16);
            this.label8.Margin = new System.Windows.Forms.Padding(0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(42, 13);
            this.label8.TabIndex = 5;
            this.label8.Text = "Зверху";
            // 
            // front9YearUp
            // 
            this.front9YearUp.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.front9YearUp.DecimalPlaces = 1;
            this.front9YearUp.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.front9YearUp.Location = new System.Drawing.Point(216, 14);
            this.front9YearUp.Margin = new System.Windows.Forms.Padding(0);
            this.front9YearUp.Maximum = new decimal(new int[] {
            210,
            0,
            0,
            0});
            this.front9YearUp.Name = "front9YearUp";
            this.front9YearUp.Size = new System.Drawing.Size(48, 20);
            this.front9YearUp.TabIndex = 4;
            this.front9YearUp.Enter += new System.EventHandler(this.NumericUpDown_Enter);
            this.front9YearUp.MouseDown += new System.Windows.Forms.MouseEventHandler(this.NumericUpDown_MouseDown);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(46, 16);
            this.label9.Margin = new System.Windows.Forms.Padding(0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(34, 13);
            this.label9.TabIndex = 3;
            this.label9.Text = "Зліва";
            // 
            // front9YearLeft
            // 
            this.front9YearLeft.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.front9YearLeft.DecimalPlaces = 1;
            this.front9YearLeft.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.front9YearLeft.Location = new System.Drawing.Point(80, 14);
            this.front9YearLeft.Margin = new System.Windows.Forms.Padding(0);
            this.front9YearLeft.Maximum = new decimal(new int[] {
            210,
            0,
            0,
            0});
            this.front9YearLeft.Name = "front9YearLeft";
            this.front9YearLeft.Size = new System.Drawing.Size(48, 20);
            this.front9YearLeft.TabIndex = 2;
            this.front9YearLeft.Enter += new System.EventHandler(this.NumericUpDown_Enter);
            this.front9YearLeft.MouseDown += new System.Windows.Forms.MouseEventHandler(this.NumericUpDown_MouseDown);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.front9FinishUp);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.front9FinishLeft);
            this.groupBox2.Location = new System.Drawing.Point(6, 17);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(280, 40);
            this.groupBox2.TabIndex = 6;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Лінія \"закінчив_\"";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(174, 16);
            this.label6.Margin = new System.Windows.Forms.Padding(0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(42, 13);
            this.label6.TabIndex = 5;
            this.label6.Text = "Зверху";
            // 
            // front9FinishUp
            // 
            this.front9FinishUp.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.front9FinishUp.DecimalPlaces = 1;
            this.front9FinishUp.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.front9FinishUp.Location = new System.Drawing.Point(216, 14);
            this.front9FinishUp.Margin = new System.Windows.Forms.Padding(0);
            this.front9FinishUp.Maximum = new decimal(new int[] {
            210,
            0,
            0,
            0});
            this.front9FinishUp.Name = "front9FinishUp";
            this.front9FinishUp.Size = new System.Drawing.Size(48, 20);
            this.front9FinishUp.TabIndex = 4;
            this.front9FinishUp.Enter += new System.EventHandler(this.NumericUpDown_Enter);
            this.front9FinishUp.MouseDown += new System.Windows.Forms.MouseEventHandler(this.NumericUpDown_MouseDown);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(46, 16);
            this.label7.Margin = new System.Windows.Forms.Padding(0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(34, 13);
            this.label7.TabIndex = 3;
            this.label7.Text = "Зліва";
            // 
            // front9FinishLeft
            // 
            this.front9FinishLeft.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.front9FinishLeft.DecimalPlaces = 1;
            this.front9FinishLeft.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.front9FinishLeft.Location = new System.Drawing.Point(80, 14);
            this.front9FinishLeft.Margin = new System.Windows.Forms.Padding(0);
            this.front9FinishLeft.Maximum = new decimal(new int[] {
            210,
            0,
            0,
            0});
            this.front9FinishLeft.Name = "front9FinishLeft";
            this.front9FinishLeft.Size = new System.Drawing.Size(48, 20);
            this.front9FinishLeft.TabIndex = 2;
            this.front9FinishLeft.Enter += new System.EventHandler(this.NumericUpDown_Enter);
            this.front9FinishLeft.MouseDown += new System.Windows.Forms.MouseEventHandler(this.NumericUpDown_MouseDown);
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.groupBox10);
            this.tabPage4.Controls.Add(this.groupBox11);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(292, 129);
            this.tabPage4.TabIndex = 1;
            this.tabPage4.Text = "11 клас";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // groupBox10
            // 
            this.groupBox10.Controls.Add(this.label24);
            this.groupBox10.Controls.Add(this.front11YearUp);
            this.groupBox10.Controls.Add(this.label28);
            this.groupBox10.Controls.Add(this.front11YearLeft);
            this.groupBox10.Location = new System.Drawing.Point(6, 76);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Size = new System.Drawing.Size(280, 40);
            this.groupBox10.TabIndex = 9;
            this.groupBox10.TabStop = false;
            this.groupBox10.Text = "Лінія \"у 20__ році\"";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(174, 16);
            this.label24.Margin = new System.Windows.Forms.Padding(0);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(42, 13);
            this.label24.TabIndex = 5;
            this.label24.Text = "Зверху";
            // 
            // front11YearUp
            // 
            this.front11YearUp.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.front11YearUp.DecimalPlaces = 1;
            this.front11YearUp.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.front11YearUp.Location = new System.Drawing.Point(216, 14);
            this.front11YearUp.Margin = new System.Windows.Forms.Padding(0);
            this.front11YearUp.Maximum = new decimal(new int[] {
            210,
            0,
            0,
            0});
            this.front11YearUp.Name = "front11YearUp";
            this.front11YearUp.Size = new System.Drawing.Size(48, 20);
            this.front11YearUp.TabIndex = 4;
            this.front11YearUp.Enter += new System.EventHandler(this.NumericUpDown_Enter);
            this.front11YearUp.MouseDown += new System.Windows.Forms.MouseEventHandler(this.NumericUpDown_MouseDown);
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(46, 16);
            this.label28.Margin = new System.Windows.Forms.Padding(0);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(34, 13);
            this.label28.TabIndex = 3;
            this.label28.Text = "Зліва";
            // 
            // front11YearLeft
            // 
            this.front11YearLeft.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.front11YearLeft.DecimalPlaces = 1;
            this.front11YearLeft.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.front11YearLeft.Location = new System.Drawing.Point(80, 14);
            this.front11YearLeft.Margin = new System.Windows.Forms.Padding(0);
            this.front11YearLeft.Maximum = new decimal(new int[] {
            210,
            0,
            0,
            0});
            this.front11YearLeft.Name = "front11YearLeft";
            this.front11YearLeft.Size = new System.Drawing.Size(48, 20);
            this.front11YearLeft.TabIndex = 2;
            this.front11YearLeft.Enter += new System.EventHandler(this.NumericUpDown_Enter);
            this.front11YearLeft.MouseDown += new System.Windows.Forms.MouseEventHandler(this.NumericUpDown_MouseDown);
            // 
            // groupBox11
            // 
            this.groupBox11.Controls.Add(this.label29);
            this.groupBox11.Controls.Add(this.front11FinishUp);
            this.groupBox11.Controls.Add(this.label30);
            this.groupBox11.Controls.Add(this.front11FinishLeft);
            this.groupBox11.Location = new System.Drawing.Point(6, 17);
            this.groupBox11.Name = "groupBox11";
            this.groupBox11.Size = new System.Drawing.Size(280, 40);
            this.groupBox11.TabIndex = 8;
            this.groupBox11.TabStop = false;
            this.groupBox11.Text = "Лінія \"здобу__\"";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(174, 16);
            this.label29.Margin = new System.Windows.Forms.Padding(0);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(42, 13);
            this.label29.TabIndex = 5;
            this.label29.Text = "Зверху";
            // 
            // front11FinishUp
            // 
            this.front11FinishUp.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.front11FinishUp.DecimalPlaces = 1;
            this.front11FinishUp.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.front11FinishUp.Location = new System.Drawing.Point(216, 14);
            this.front11FinishUp.Margin = new System.Windows.Forms.Padding(0);
            this.front11FinishUp.Maximum = new decimal(new int[] {
            210,
            0,
            0,
            0});
            this.front11FinishUp.Name = "front11FinishUp";
            this.front11FinishUp.Size = new System.Drawing.Size(48, 20);
            this.front11FinishUp.TabIndex = 4;
            this.front11FinishUp.Enter += new System.EventHandler(this.NumericUpDown_Enter);
            this.front11FinishUp.MouseDown += new System.Windows.Forms.MouseEventHandler(this.NumericUpDown_MouseDown);
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(46, 16);
            this.label30.Margin = new System.Windows.Forms.Padding(0);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(34, 13);
            this.label30.TabIndex = 3;
            this.label30.Text = "Зліва";
            // 
            // front11FinishLeft
            // 
            this.front11FinishLeft.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.front11FinishLeft.DecimalPlaces = 1;
            this.front11FinishLeft.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.front11FinishLeft.Location = new System.Drawing.Point(80, 14);
            this.front11FinishLeft.Margin = new System.Windows.Forms.Padding(0);
            this.front11FinishLeft.Maximum = new decimal(new int[] {
            210,
            0,
            0,
            0});
            this.front11FinishLeft.Name = "front11FinishLeft";
            this.front11FinishLeft.Size = new System.Drawing.Size(48, 20);
            this.front11FinishLeft.TabIndex = 2;
            this.front11FinishLeft.Enter += new System.EventHandler(this.NumericUpDown_Enter);
            this.front11FinishLeft.MouseDown += new System.Windows.Forms.MouseEventHandler(this.NumericUpDown_MouseDown);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.frontNumberLeft);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.frontSeriesLeft);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.frontSerNum);
            this.groupBox1.Location = new System.Drawing.Point(6, 6);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(300, 60);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Лінія серії та номера атестату";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(156, 39);
            this.label5.Margin = new System.Windows.Forms.Padding(0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(70, 13);
            this.label5.TabIndex = 5;
            this.label5.Text = "Номер зліва";
            // 
            // frontNumberLeft
            // 
            this.frontNumberLeft.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.frontNumberLeft.DecimalPlaces = 1;
            this.frontNumberLeft.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.frontNumberLeft.Location = new System.Drawing.Point(226, 37);
            this.frontNumberLeft.Margin = new System.Windows.Forms.Padding(0);
            this.frontNumberLeft.Maximum = new decimal(new int[] {
            210,
            0,
            0,
            0});
            this.frontNumberLeft.Name = "frontNumberLeft";
            this.frontNumberLeft.Size = new System.Drawing.Size(48, 20);
            this.frontNumberLeft.TabIndex = 4;
            this.frontNumberLeft.Enter += new System.EventHandler(this.NumericUpDown_Enter);
            this.frontNumberLeft.MouseDown += new System.Windows.Forms.MouseEventHandler(this.NumericUpDown_MouseDown);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(27, 39);
            this.label4.Margin = new System.Windows.Forms.Padding(0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(63, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Серія зліва";
            // 
            // frontSeriesLeft
            // 
            this.frontSeriesLeft.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.frontSeriesLeft.DecimalPlaces = 1;
            this.frontSeriesLeft.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.frontSeriesLeft.Location = new System.Drawing.Point(90, 37);
            this.frontSeriesLeft.Margin = new System.Windows.Forms.Padding(0);
            this.frontSeriesLeft.Maximum = new decimal(new int[] {
            210,
            0,
            0,
            0});
            this.frontSeriesLeft.Name = "frontSeriesLeft";
            this.frontSeriesLeft.Size = new System.Drawing.Size(48, 20);
            this.frontSeriesLeft.TabIndex = 2;
            this.frontSeriesLeft.Enter += new System.EventHandler(this.NumericUpDown_Enter);
            this.frontSeriesLeft.MouseDown += new System.Windows.Forms.MouseEventHandler(this.NumericUpDown_MouseDown);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(70, 18);
            this.label1.Margin = new System.Windows.Forms.Padding(0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(80, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Зверху(обидві)";
            // 
            // frontSerNum
            // 
            this.frontSerNum.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.frontSerNum.DecimalPlaces = 1;
            this.frontSerNum.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.frontSerNum.Location = new System.Drawing.Point(153, 16);
            this.frontSerNum.Margin = new System.Windows.Forms.Padding(0);
            this.frontSerNum.Maximum = new decimal(new int[] {
            210,
            0,
            0,
            0});
            this.frontSerNum.Name = "frontSerNum";
            this.frontSerNum.Size = new System.Drawing.Size(48, 20);
            this.frontSerNum.TabIndex = 0;
            this.frontSerNum.Enter += new System.EventHandler(this.NumericUpDown_Enter);
            this.frontSerNum.MouseDown += new System.Windows.Forms.MouseEventHandler(this.NumericUpDown_MouseDown);
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.groupBox15);
            this.tabPage2.Controls.Add(this.tabControl3);
            this.tabPage2.Controls.Add(this.groupBox16);
            this.tabPage2.Controls.Add(this.groupBox17);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(632, 394);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Задня частина";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // groupBox15
            // 
            this.groupBox15.Controls.Add(this.label41);
            this.groupBox15.Controls.Add(this.backLearnUp);
            this.groupBox15.Controls.Add(this.label42);
            this.groupBox15.Controls.Add(this.backLearnLeft);
            this.groupBox15.Location = new System.Drawing.Point(6, 6);
            this.groupBox15.Name = "groupBox15";
            this.groupBox15.Size = new System.Drawing.Size(287, 42);
            this.groupBox15.TabIndex = 15;
            this.groupBox15.TabStop = false;
            this.groupBox15.Text = "Лінія \"...успішно засвої___\"";
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Location = new System.Drawing.Point(194, 18);
            this.label41.Margin = new System.Windows.Forms.Padding(0);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(42, 13);
            this.label41.TabIndex = 5;
            this.label41.Text = "Зверху";
            // 
            // backLearnUp
            // 
            this.backLearnUp.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.backLearnUp.DecimalPlaces = 1;
            this.backLearnUp.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.backLearnUp.Location = new System.Drawing.Point(236, 16);
            this.backLearnUp.Margin = new System.Windows.Forms.Padding(0);
            this.backLearnUp.Maximum = new decimal(new int[] {
            210,
            0,
            0,
            0});
            this.backLearnUp.Name = "backLearnUp";
            this.backLearnUp.Size = new System.Drawing.Size(48, 20);
            this.backLearnUp.TabIndex = 4;
            this.backLearnUp.Enter += new System.EventHandler(this.NumericUpDown_Enter);
            this.backLearnUp.MouseDown += new System.Windows.Forms.MouseEventHandler(this.NumericUpDown_MouseDown);
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Location = new System.Drawing.Point(65, 18);
            this.label42.Margin = new System.Windows.Forms.Padding(0);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(34, 13);
            this.label42.TabIndex = 1;
            this.label42.Text = "Зліва";
            // 
            // backLearnLeft
            // 
            this.backLearnLeft.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.backLearnLeft.DecimalPlaces = 1;
            this.backLearnLeft.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.backLearnLeft.Location = new System.Drawing.Point(99, 16);
            this.backLearnLeft.Margin = new System.Windows.Forms.Padding(0);
            this.backLearnLeft.Maximum = new decimal(new int[] {
            210,
            0,
            0,
            0});
            this.backLearnLeft.Name = "backLearnLeft";
            this.backLearnLeft.Size = new System.Drawing.Size(48, 20);
            this.backLearnLeft.TabIndex = 0;
            this.backLearnLeft.Enter += new System.EventHandler(this.NumericUpDown_Enter);
            this.backLearnLeft.MouseDown += new System.Windows.Forms.MouseEventHandler(this.NumericUpDown_MouseDown);
            // 
            // tabControl3
            // 
            this.tabControl3.Controls.Add(this.tabPage5);
            this.tabControl3.Controls.Add(this.tabPage6);
            this.tabControl3.Location = new System.Drawing.Point(296, 6);
            this.tabControl3.Name = "tabControl3";
            this.tabControl3.SelectedIndex = 0;
            this.tabControl3.Size = new System.Drawing.Size(330, 390);
            this.tabControl3.TabIndex = 12;
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this.groupBox18);
            this.tabPage5.Controls.Add(this.groupBox14);
            this.tabPage5.Controls.Add(this.groupBox12);
            this.tabPage5.Controls.Add(this.groupBox19);
            this.tabPage5.Location = new System.Drawing.Point(4, 22);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage5.Size = new System.Drawing.Size(322, 364);
            this.tabPage5.TabIndex = 0;
            this.tabPage5.Text = "9 клас";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // groupBox18
            // 
            this.groupBox18.Controls.Add(this.label36);
            this.groupBox18.Controls.Add(this.back9RegNUp);
            this.groupBox18.Controls.Add(this.label39);
            this.groupBox18.Controls.Add(this.back9RegNLeft);
            this.groupBox18.Location = new System.Drawing.Point(15, 303);
            this.groupBox18.Name = "groupBox18";
            this.groupBox18.Size = new System.Drawing.Size(290, 49);
            this.groupBox18.TabIndex = 9;
            this.groupBox18.TabStop = false;
            this.groupBox18.Text = "Лінія \"Реєстраційний номер\"";
            this.groupBox18.Visible = false;
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(174, 16);
            this.label36.Margin = new System.Windows.Forms.Padding(0);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(42, 13);
            this.label36.TabIndex = 5;
            this.label36.Text = "Зверху";
            // 
            // back9RegNUp
            // 
            this.back9RegNUp.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.back9RegNUp.DecimalPlaces = 1;
            this.back9RegNUp.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.back9RegNUp.Location = new System.Drawing.Point(216, 14);
            this.back9RegNUp.Margin = new System.Windows.Forms.Padding(0);
            this.back9RegNUp.Maximum = new decimal(new int[] {
            210,
            0,
            0,
            0});
            this.back9RegNUp.Name = "back9RegNUp";
            this.back9RegNUp.Size = new System.Drawing.Size(48, 20);
            this.back9RegNUp.TabIndex = 4;
            this.back9RegNUp.Enter += new System.EventHandler(this.NumericUpDown_Enter);
            this.back9RegNUp.MouseDown += new System.Windows.Forms.MouseEventHandler(this.NumericUpDown_MouseDown);
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(46, 16);
            this.label39.Margin = new System.Windows.Forms.Padding(0);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(34, 13);
            this.label39.TabIndex = 3;
            this.label39.Text = "Зліва";
            // 
            // back9RegNLeft
            // 
            this.back9RegNLeft.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.back9RegNLeft.DecimalPlaces = 1;
            this.back9RegNLeft.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.back9RegNLeft.Location = new System.Drawing.Point(80, 14);
            this.back9RegNLeft.Margin = new System.Windows.Forms.Padding(0);
            this.back9RegNLeft.Maximum = new decimal(new int[] {
            210,
            0,
            0,
            0});
            this.back9RegNLeft.Name = "back9RegNLeft";
            this.back9RegNLeft.Size = new System.Drawing.Size(48, 20);
            this.back9RegNLeft.TabIndex = 2;
            this.back9RegNLeft.Enter += new System.EventHandler(this.NumericUpDown_Enter);
            this.back9RegNLeft.MouseDown += new System.Windows.Forms.MouseEventHandler(this.NumericUpDown_MouseDown);
            // 
            // groupBox14
            // 
            this.groupBox14.Controls.Add(this.label34);
            this.groupBox14.Controls.Add(this.back9DirectorUp);
            this.groupBox14.Controls.Add(this.label35);
            this.groupBox14.Controls.Add(this.back9DirectorLeft);
            this.groupBox14.Location = new System.Drawing.Point(15, 253);
            this.groupBox14.Name = "groupBox14";
            this.groupBox14.Size = new System.Drawing.Size(290, 44);
            this.groupBox14.TabIndex = 8;
            this.groupBox14.TabStop = false;
            this.groupBox14.Text = "Лінія \"Директор\"";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(174, 16);
            this.label34.Margin = new System.Windows.Forms.Padding(0);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(42, 13);
            this.label34.TabIndex = 5;
            this.label34.Text = "Зверху";
            // 
            // back9DirectorUp
            // 
            this.back9DirectorUp.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.back9DirectorUp.DecimalPlaces = 1;
            this.back9DirectorUp.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.back9DirectorUp.Location = new System.Drawing.Point(216, 14);
            this.back9DirectorUp.Margin = new System.Windows.Forms.Padding(0);
            this.back9DirectorUp.Maximum = new decimal(new int[] {
            210,
            0,
            0,
            0});
            this.back9DirectorUp.Name = "back9DirectorUp";
            this.back9DirectorUp.Size = new System.Drawing.Size(48, 20);
            this.back9DirectorUp.TabIndex = 4;
            this.back9DirectorUp.Enter += new System.EventHandler(this.NumericUpDown_Enter);
            this.back9DirectorUp.MouseDown += new System.Windows.Forms.MouseEventHandler(this.NumericUpDown_MouseDown);
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(46, 16);
            this.label35.Margin = new System.Windows.Forms.Padding(0);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(34, 13);
            this.label35.TabIndex = 3;
            this.label35.Text = "Зліва";
            // 
            // back9DirectorLeft
            // 
            this.back9DirectorLeft.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.back9DirectorLeft.DecimalPlaces = 1;
            this.back9DirectorLeft.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.back9DirectorLeft.Location = new System.Drawing.Point(80, 14);
            this.back9DirectorLeft.Margin = new System.Windows.Forms.Padding(0);
            this.back9DirectorLeft.Maximum = new decimal(new int[] {
            210,
            0,
            0,
            0});
            this.back9DirectorLeft.Name = "back9DirectorLeft";
            this.back9DirectorLeft.Size = new System.Drawing.Size(48, 20);
            this.back9DirectorLeft.TabIndex = 2;
            this.back9DirectorLeft.Enter += new System.EventHandler(this.NumericUpDown_Enter);
            this.back9DirectorLeft.MouseDown += new System.Windows.Forms.MouseEventHandler(this.NumericUpDown_MouseDown);
            // 
            // groupBox12
            // 
            this.groupBox12.Controls.Add(this.label32);
            this.groupBox12.Controls.Add(this.back9AwardShortLeft);
            this.groupBox12.Controls.Add(this.label33);
            this.groupBox12.Controls.Add(this.back9AwardBothUp);
            this.groupBox12.Location = new System.Drawing.Point(15, 129);
            this.groupBox12.Name = "groupBox12";
            this.groupBox12.Size = new System.Drawing.Size(290, 85);
            this.groupBox12.TabIndex = 7;
            this.groupBox12.TabStop = false;
            this.groupBox12.Text = "Лінія \"_______ нагороджен_\"";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(72, 55);
            this.label32.Margin = new System.Windows.Forms.Padding(0);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(140, 13);
            this.label32.TabIndex = 5;
            this.label32.Text = "Зліва Коротка(права) лінія";
            // 
            // back9AwardShortLeft
            // 
            this.back9AwardShortLeft.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.back9AwardShortLeft.DecimalPlaces = 1;
            this.back9AwardShortLeft.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.back9AwardShortLeft.Location = new System.Drawing.Point(217, 53);
            this.back9AwardShortLeft.Margin = new System.Windows.Forms.Padding(0);
            this.back9AwardShortLeft.Maximum = new decimal(new int[] {
            210,
            0,
            0,
            0});
            this.back9AwardShortLeft.Name = "back9AwardShortLeft";
            this.back9AwardShortLeft.Size = new System.Drawing.Size(48, 20);
            this.back9AwardShortLeft.TabIndex = 4;
            this.back9AwardShortLeft.Enter += new System.EventHandler(this.NumericUpDown_Enter);
            this.back9AwardShortLeft.MouseDown += new System.Windows.Forms.MouseEventHandler(this.NumericUpDown_MouseDown);
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(70, 18);
            this.label33.Margin = new System.Windows.Forms.Padding(0);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(80, 13);
            this.label33.TabIndex = 3;
            this.label33.Text = "Зверху(обидві)";
            // 
            // back9AwardBothUp
            // 
            this.back9AwardBothUp.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.back9AwardBothUp.DecimalPlaces = 1;
            this.back9AwardBothUp.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.back9AwardBothUp.Location = new System.Drawing.Point(155, 16);
            this.back9AwardBothUp.Margin = new System.Windows.Forms.Padding(0);
            this.back9AwardBothUp.Maximum = new decimal(new int[] {
            210,
            0,
            0,
            0});
            this.back9AwardBothUp.Name = "back9AwardBothUp";
            this.back9AwardBothUp.Size = new System.Drawing.Size(48, 20);
            this.back9AwardBothUp.TabIndex = 2;
            this.back9AwardBothUp.Enter += new System.EventHandler(this.NumericUpDown_Enter);
            this.back9AwardBothUp.MouseDown += new System.Windows.Forms.MouseEventHandler(this.NumericUpDown_MouseDown);
            // 
            // groupBox19
            // 
            this.groupBox19.Controls.Add(this.label47);
            this.groupBox19.Controls.Add(this.back9GetUp_LLow);
            this.groupBox19.Controls.Add(this.label40);
            this.groupBox19.Controls.Add(this.label52);
            this.groupBox19.Controls.Add(this.back9GetUp);
            this.groupBox19.Controls.Add(this.label53);
            this.groupBox19.Controls.Add(this.back9GetLeft);
            this.groupBox19.Location = new System.Drawing.Point(15, 6);
            this.groupBox19.Name = "groupBox19";
            this.groupBox19.Size = new System.Drawing.Size(290, 85);
            this.groupBox19.TabIndex = 6;
            this.groupBox19.TabStop = false;
            this.groupBox19.Text = "Лінія \"...одержа_\"";
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Location = new System.Drawing.Point(115, 37);
            this.label47.Margin = new System.Windows.Forms.Padding(0);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(99, 13);
            this.label47.TabIndex = 8;
            this.label47.Text = "Лінія трохи нижче:";
            // 
            // back9GetUp_LLow
            // 
            this.back9GetUp_LLow.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.back9GetUp_LLow.DecimalPlaces = 1;
            this.back9GetUp_LLow.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.back9GetUp_LLow.Location = new System.Drawing.Point(216, 54);
            this.back9GetUp_LLow.Margin = new System.Windows.Forms.Padding(0);
            this.back9GetUp_LLow.Maximum = new decimal(new int[] {
            210,
            0,
            0,
            0});
            this.back9GetUp_LLow.Name = "back9GetUp_LLow";
            this.back9GetUp_LLow.Size = new System.Drawing.Size(48, 20);
            this.back9GetUp_LLow.TabIndex = 7;
            this.back9GetUp_LLow.Enter += new System.EventHandler(this.NumericUpDown_Enter);
            this.back9GetUp_LLow.MouseDown += new System.Windows.Forms.MouseEventHandler(this.NumericUpDown_MouseDown);
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(174, 56);
            this.label40.Margin = new System.Windows.Forms.Padding(0);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(42, 13);
            this.label40.TabIndex = 6;
            this.label40.Text = "Зверху";
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Location = new System.Drawing.Point(174, 16);
            this.label52.Margin = new System.Windows.Forms.Padding(0);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(42, 13);
            this.label52.TabIndex = 5;
            this.label52.Text = "Зверху";
            // 
            // back9GetUp
            // 
            this.back9GetUp.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.back9GetUp.DecimalPlaces = 1;
            this.back9GetUp.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.back9GetUp.Location = new System.Drawing.Point(216, 14);
            this.back9GetUp.Margin = new System.Windows.Forms.Padding(0);
            this.back9GetUp.Maximum = new decimal(new int[] {
            210,
            0,
            0,
            0});
            this.back9GetUp.Name = "back9GetUp";
            this.back9GetUp.Size = new System.Drawing.Size(48, 20);
            this.back9GetUp.TabIndex = 4;
            this.back9GetUp.Enter += new System.EventHandler(this.NumericUpDown_Enter);
            this.back9GetUp.MouseDown += new System.Windows.Forms.MouseEventHandler(this.NumericUpDown_MouseDown);
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Location = new System.Drawing.Point(25, 16);
            this.label53.Margin = new System.Windows.Forms.Padding(0);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(34, 13);
            this.label53.TabIndex = 3;
            this.label53.Text = "Зліва";
            // 
            // back9GetLeft
            // 
            this.back9GetLeft.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.back9GetLeft.DecimalPlaces = 1;
            this.back9GetLeft.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.back9GetLeft.Location = new System.Drawing.Point(59, 14);
            this.back9GetLeft.Margin = new System.Windows.Forms.Padding(0);
            this.back9GetLeft.Maximum = new decimal(new int[] {
            210,
            0,
            0,
            0});
            this.back9GetLeft.Name = "back9GetLeft";
            this.back9GetLeft.Size = new System.Drawing.Size(48, 20);
            this.back9GetLeft.TabIndex = 2;
            this.back9GetLeft.Enter += new System.EventHandler(this.NumericUpDown_Enter);
            this.back9GetLeft.MouseDown += new System.Windows.Forms.MouseEventHandler(this.NumericUpDown_MouseDown);
            // 
            // tabPage6
            // 
            this.tabPage6.Controls.Add(this.groupBox24);
            this.tabPage6.Controls.Add(this.groupBox27);
            this.tabPage6.Controls.Add(this.groupBox22);
            this.tabPage6.Controls.Add(this.groupBox25);
            this.tabPage6.Controls.Add(this.groupBox20);
            this.tabPage6.Controls.Add(this.groupBox23);
            this.tabPage6.Controls.Add(this.groupBox21);
            this.tabPage6.Location = new System.Drawing.Point(4, 22);
            this.tabPage6.Name = "tabPage6";
            this.tabPage6.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage6.Size = new System.Drawing.Size(322, 364);
            this.tabPage6.TabIndex = 1;
            this.tabPage6.Text = "11 клас";
            this.tabPage6.UseVisualStyleBackColor = true;
            // 
            // groupBox24
            // 
            this.groupBox24.Controls.Add(this.label59);
            this.groupBox24.Controls.Add(this.back11DirectorUp);
            this.groupBox24.Controls.Add(this.label60);
            this.groupBox24.Controls.Add(this.back11DirectorLeft);
            this.groupBox24.Location = new System.Drawing.Point(15, 253);
            this.groupBox24.Name = "groupBox24";
            this.groupBox24.Size = new System.Drawing.Size(290, 44);
            this.groupBox24.TabIndex = 13;
            this.groupBox24.TabStop = false;
            this.groupBox24.Text = "Лінія \"Директор\"";
            // 
            // label59
            // 
            this.label59.AutoSize = true;
            this.label59.Location = new System.Drawing.Point(174, 16);
            this.label59.Margin = new System.Windows.Forms.Padding(0);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(42, 13);
            this.label59.TabIndex = 5;
            this.label59.Text = "Зверху";
            // 
            // back11DirectorUp
            // 
            this.back11DirectorUp.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.back11DirectorUp.DecimalPlaces = 1;
            this.back11DirectorUp.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.back11DirectorUp.Location = new System.Drawing.Point(216, 14);
            this.back11DirectorUp.Margin = new System.Windows.Forms.Padding(0);
            this.back11DirectorUp.Maximum = new decimal(new int[] {
            210,
            0,
            0,
            0});
            this.back11DirectorUp.Name = "back11DirectorUp";
            this.back11DirectorUp.Size = new System.Drawing.Size(48, 20);
            this.back11DirectorUp.TabIndex = 4;
            this.back11DirectorUp.Enter += new System.EventHandler(this.NumericUpDown_Enter);
            this.back11DirectorUp.MouseDown += new System.Windows.Forms.MouseEventHandler(this.NumericUpDown_MouseDown);
            // 
            // label60
            // 
            this.label60.AutoSize = true;
            this.label60.Location = new System.Drawing.Point(46, 16);
            this.label60.Margin = new System.Windows.Forms.Padding(0);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(34, 13);
            this.label60.TabIndex = 3;
            this.label60.Text = "Зліва";
            // 
            // back11DirectorLeft
            // 
            this.back11DirectorLeft.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.back11DirectorLeft.DecimalPlaces = 1;
            this.back11DirectorLeft.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.back11DirectorLeft.Location = new System.Drawing.Point(80, 14);
            this.back11DirectorLeft.Margin = new System.Windows.Forms.Padding(0);
            this.back11DirectorLeft.Maximum = new decimal(new int[] {
            210,
            0,
            0,
            0});
            this.back11DirectorLeft.Name = "back11DirectorLeft";
            this.back11DirectorLeft.Size = new System.Drawing.Size(48, 20);
            this.back11DirectorLeft.TabIndex = 2;
            this.back11DirectorLeft.Enter += new System.EventHandler(this.NumericUpDown_Enter);
            this.back11DirectorLeft.MouseDown += new System.Windows.Forms.MouseEventHandler(this.NumericUpDown_MouseDown);
            // 
            // groupBox27
            // 
            this.groupBox27.Controls.Add(this.label65);
            this.groupBox27.Controls.Add(this.back11RegNUp);
            this.groupBox27.Controls.Add(this.label66);
            this.groupBox27.Controls.Add(this.back11RegNLeft);
            this.groupBox27.Location = new System.Drawing.Point(15, 303);
            this.groupBox27.Name = "groupBox27";
            this.groupBox27.Size = new System.Drawing.Size(290, 49);
            this.groupBox27.TabIndex = 14;
            this.groupBox27.TabStop = false;
            this.groupBox27.Text = "Лінія \"Реєстраційний номер\"";
            // 
            // label65
            // 
            this.label65.AutoSize = true;
            this.label65.Location = new System.Drawing.Point(174, 16);
            this.label65.Margin = new System.Windows.Forms.Padding(0);
            this.label65.Name = "label65";
            this.label65.Size = new System.Drawing.Size(42, 13);
            this.label65.TabIndex = 5;
            this.label65.Text = "Зверху";
            // 
            // back11RegNUp
            // 
            this.back11RegNUp.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.back11RegNUp.DecimalPlaces = 1;
            this.back11RegNUp.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.back11RegNUp.Location = new System.Drawing.Point(216, 14);
            this.back11RegNUp.Margin = new System.Windows.Forms.Padding(0);
            this.back11RegNUp.Maximum = new decimal(new int[] {
            210,
            0,
            0,
            0});
            this.back11RegNUp.Name = "back11RegNUp";
            this.back11RegNUp.Size = new System.Drawing.Size(48, 20);
            this.back11RegNUp.TabIndex = 4;
            this.back11RegNUp.Enter += new System.EventHandler(this.NumericUpDown_Enter);
            this.back11RegNUp.MouseDown += new System.Windows.Forms.MouseEventHandler(this.NumericUpDown_MouseDown);
            // 
            // label66
            // 
            this.label66.AutoSize = true;
            this.label66.Location = new System.Drawing.Point(46, 16);
            this.label66.Margin = new System.Windows.Forms.Padding(0);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(34, 13);
            this.label66.TabIndex = 3;
            this.label66.Text = "Зліва";
            // 
            // back11RegNLeft
            // 
            this.back11RegNLeft.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.back11RegNLeft.DecimalPlaces = 1;
            this.back11RegNLeft.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.back11RegNLeft.Location = new System.Drawing.Point(80, 14);
            this.back11RegNLeft.Margin = new System.Windows.Forms.Padding(0);
            this.back11RegNLeft.Maximum = new decimal(new int[] {
            210,
            0,
            0,
            0});
            this.back11RegNLeft.Name = "back11RegNLeft";
            this.back11RegNLeft.Size = new System.Drawing.Size(48, 20);
            this.back11RegNLeft.TabIndex = 2;
            this.back11RegNLeft.Enter += new System.EventHandler(this.NumericUpDown_Enter);
            this.back11RegNLeft.MouseDown += new System.Windows.Forms.MouseEventHandler(this.NumericUpDown_MouseDown);
            // 
            // groupBox22
            // 
            this.groupBox22.Controls.Add(this.label48);
            this.groupBox22.Controls.Add(this.back11LearnUp);
            this.groupBox22.Controls.Add(this.label50);
            this.groupBox22.Controls.Add(this.back11LearnLeft);
            this.groupBox22.Location = new System.Drawing.Point(15, 157);
            this.groupBox22.Name = "groupBox22";
            this.groupBox22.Size = new System.Drawing.Size(290, 42);
            this.groupBox22.TabIndex = 11;
            this.groupBox22.TabStop = false;
            this.groupBox22.Text = "Лінія \"вивченні _______\"";
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Location = new System.Drawing.Point(174, 16);
            this.label48.Margin = new System.Windows.Forms.Padding(0);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(42, 13);
            this.label48.TabIndex = 5;
            this.label48.Text = "Зверху";
            // 
            // back11LearnUp
            // 
            this.back11LearnUp.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.back11LearnUp.DecimalPlaces = 1;
            this.back11LearnUp.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.back11LearnUp.Location = new System.Drawing.Point(216, 14);
            this.back11LearnUp.Margin = new System.Windows.Forms.Padding(0);
            this.back11LearnUp.Maximum = new decimal(new int[] {
            210,
            0,
            0,
            0});
            this.back11LearnUp.Name = "back11LearnUp";
            this.back11LearnUp.Size = new System.Drawing.Size(48, 20);
            this.back11LearnUp.TabIndex = 4;
            this.back11LearnUp.Enter += new System.EventHandler(this.NumericUpDown_Enter);
            this.back11LearnUp.MouseDown += new System.Windows.Forms.MouseEventHandler(this.NumericUpDown_MouseDown);
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Location = new System.Drawing.Point(46, 16);
            this.label50.Margin = new System.Windows.Forms.Padding(0);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(34, 13);
            this.label50.TabIndex = 3;
            this.label50.Text = "Зліва";
            // 
            // back11LearnLeft
            // 
            this.back11LearnLeft.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.back11LearnLeft.DecimalPlaces = 1;
            this.back11LearnLeft.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.back11LearnLeft.Location = new System.Drawing.Point(80, 14);
            this.back11LearnLeft.Margin = new System.Windows.Forms.Padding(0);
            this.back11LearnLeft.Maximum = new decimal(new int[] {
            210,
            0,
            0,
            0});
            this.back11LearnLeft.Name = "back11LearnLeft";
            this.back11LearnLeft.Size = new System.Drawing.Size(48, 20);
            this.back11LearnLeft.TabIndex = 2;
            this.back11LearnLeft.Enter += new System.EventHandler(this.NumericUpDown_Enter);
            this.back11LearnLeft.MouseDown += new System.Windows.Forms.MouseEventHandler(this.NumericUpDown_MouseDown);
            // 
            // groupBox25
            // 
            this.groupBox25.Controls.Add(this.label61);
            this.groupBox25.Controls.Add(this.back11DiplomUp);
            this.groupBox25.Controls.Add(this.label62);
            this.groupBox25.Controls.Add(this.back11DiplomLeft);
            this.groupBox25.Location = new System.Drawing.Point(15, 205);
            this.groupBox25.Name = "groupBox25";
            this.groupBox25.Size = new System.Drawing.Size(290, 42);
            this.groupBox25.TabIndex = 12;
            this.groupBox25.TabStop = false;
            this.groupBox25.Text = "Лінія \"...нагороджен___(Похв. грамотою)\"";
            // 
            // label61
            // 
            this.label61.AutoSize = true;
            this.label61.Location = new System.Drawing.Point(174, 16);
            this.label61.Margin = new System.Windows.Forms.Padding(0);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(42, 13);
            this.label61.TabIndex = 5;
            this.label61.Text = "Зверху";
            // 
            // back11DiplomUp
            // 
            this.back11DiplomUp.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.back11DiplomUp.DecimalPlaces = 1;
            this.back11DiplomUp.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.back11DiplomUp.Location = new System.Drawing.Point(216, 14);
            this.back11DiplomUp.Margin = new System.Windows.Forms.Padding(0);
            this.back11DiplomUp.Maximum = new decimal(new int[] {
            210,
            0,
            0,
            0});
            this.back11DiplomUp.Name = "back11DiplomUp";
            this.back11DiplomUp.Size = new System.Drawing.Size(48, 20);
            this.back11DiplomUp.TabIndex = 4;
            this.back11DiplomUp.Enter += new System.EventHandler(this.NumericUpDown_Enter);
            this.back11DiplomUp.MouseDown += new System.Windows.Forms.MouseEventHandler(this.NumericUpDown_MouseDown);
            // 
            // label62
            // 
            this.label62.AutoSize = true;
            this.label62.Location = new System.Drawing.Point(46, 16);
            this.label62.Margin = new System.Windows.Forms.Padding(0);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(34, 13);
            this.label62.TabIndex = 3;
            this.label62.Text = "Зліва";
            // 
            // back11DiplomLeft
            // 
            this.back11DiplomLeft.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.back11DiplomLeft.DecimalPlaces = 1;
            this.back11DiplomLeft.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.back11DiplomLeft.Location = new System.Drawing.Point(80, 14);
            this.back11DiplomLeft.Margin = new System.Windows.Forms.Padding(0);
            this.back11DiplomLeft.Maximum = new decimal(new int[] {
            210,
            0,
            0,
            0});
            this.back11DiplomLeft.Name = "back11DiplomLeft";
            this.back11DiplomLeft.Size = new System.Drawing.Size(48, 20);
            this.back11DiplomLeft.TabIndex = 2;
            this.back11DiplomLeft.Enter += new System.EventHandler(this.NumericUpDown_Enter);
            this.back11DiplomLeft.MouseDown += new System.Windows.Forms.MouseEventHandler(this.NumericUpDown_MouseDown);
            // 
            // groupBox20
            // 
            this.groupBox20.Controls.Add(this.label54);
            this.groupBox20.Controls.Add(this.back11AwardedUp);
            this.groupBox20.Controls.Add(this.label55);
            this.groupBox20.Controls.Add(this.back11AwardedLeft);
            this.groupBox20.Location = new System.Drawing.Point(15, 54);
            this.groupBox20.Name = "groupBox20";
            this.groupBox20.Size = new System.Drawing.Size(290, 42);
            this.groupBox20.TabIndex = 9;
            this.groupBox20.TabStop = false;
            this.groupBox20.Text = "Лінія \"нагороджен___(медаллю)\"";
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Location = new System.Drawing.Point(174, 16);
            this.label54.Margin = new System.Windows.Forms.Padding(0);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(42, 13);
            this.label54.TabIndex = 5;
            this.label54.Text = "Зверху";
            // 
            // back11AwardedUp
            // 
            this.back11AwardedUp.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.back11AwardedUp.DecimalPlaces = 1;
            this.back11AwardedUp.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.back11AwardedUp.Location = new System.Drawing.Point(216, 14);
            this.back11AwardedUp.Margin = new System.Windows.Forms.Padding(0);
            this.back11AwardedUp.Maximum = new decimal(new int[] {
            210,
            0,
            0,
            0});
            this.back11AwardedUp.Name = "back11AwardedUp";
            this.back11AwardedUp.Size = new System.Drawing.Size(48, 20);
            this.back11AwardedUp.TabIndex = 4;
            this.back11AwardedUp.Enter += new System.EventHandler(this.NumericUpDown_Enter);
            this.back11AwardedUp.MouseDown += new System.Windows.Forms.MouseEventHandler(this.NumericUpDown_MouseDown);
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.Location = new System.Drawing.Point(46, 16);
            this.label55.Margin = new System.Windows.Forms.Padding(0);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(34, 13);
            this.label55.TabIndex = 3;
            this.label55.Text = "Зліва";
            // 
            // back11AwardedLeft
            // 
            this.back11AwardedLeft.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.back11AwardedLeft.DecimalPlaces = 1;
            this.back11AwardedLeft.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.back11AwardedLeft.Location = new System.Drawing.Point(80, 14);
            this.back11AwardedLeft.Margin = new System.Windows.Forms.Padding(0);
            this.back11AwardedLeft.Maximum = new decimal(new int[] {
            210,
            0,
            0,
            0});
            this.back11AwardedLeft.Name = "back11AwardedLeft";
            this.back11AwardedLeft.Size = new System.Drawing.Size(48, 20);
            this.back11AwardedLeft.TabIndex = 2;
            this.back11AwardedLeft.Enter += new System.EventHandler(this.NumericUpDown_Enter);
            this.back11AwardedLeft.MouseDown += new System.Windows.Forms.MouseEventHandler(this.NumericUpDown_MouseDown);
            // 
            // groupBox23
            // 
            this.groupBox23.Controls.Add(this.label51);
            this.groupBox23.Controls.Add(this.back11MedalUp);
            this.groupBox23.Location = new System.Drawing.Point(15, 109);
            this.groupBox23.Name = "groupBox23";
            this.groupBox23.Size = new System.Drawing.Size(290, 42);
            this.groupBox23.TabIndex = 10;
            this.groupBox23.TabStop = false;
            this.groupBox23.Text = "Лінія \"______ медаллю\"";
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Location = new System.Drawing.Point(174, 16);
            this.label51.Margin = new System.Windows.Forms.Padding(0);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(42, 13);
            this.label51.TabIndex = 5;
            this.label51.Text = "Зверху";
            // 
            // back11MedalUp
            // 
            this.back11MedalUp.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.back11MedalUp.DecimalPlaces = 1;
            this.back11MedalUp.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.back11MedalUp.Location = new System.Drawing.Point(216, 14);
            this.back11MedalUp.Margin = new System.Windows.Forms.Padding(0);
            this.back11MedalUp.Maximum = new decimal(new int[] {
            210,
            0,
            0,
            0});
            this.back11MedalUp.Name = "back11MedalUp";
            this.back11MedalUp.Size = new System.Drawing.Size(48, 20);
            this.back11MedalUp.TabIndex = 4;
            this.back11MedalUp.Enter += new System.EventHandler(this.NumericUpDown_Enter);
            this.back11MedalUp.MouseDown += new System.Windows.Forms.MouseEventHandler(this.NumericUpDown_MouseDown);
            // 
            // groupBox21
            // 
            this.groupBox21.Controls.Add(this.label56);
            this.groupBox21.Controls.Add(this.back11AchiveUp);
            this.groupBox21.Controls.Add(this.label57);
            this.groupBox21.Controls.Add(this.back11AchiveLeft);
            this.groupBox21.Location = new System.Drawing.Point(15, 6);
            this.groupBox21.Name = "groupBox21";
            this.groupBox21.Size = new System.Drawing.Size(290, 42);
            this.groupBox21.TabIndex = 8;
            this.groupBox21.TabStop = false;
            this.groupBox21.Text = "Лінія \"За_______ досягнення у\"";
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.Location = new System.Drawing.Point(174, 16);
            this.label56.Margin = new System.Windows.Forms.Padding(0);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(42, 13);
            this.label56.TabIndex = 5;
            this.label56.Text = "Зверху";
            // 
            // back11AchiveUp
            // 
            this.back11AchiveUp.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.back11AchiveUp.DecimalPlaces = 1;
            this.back11AchiveUp.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.back11AchiveUp.Location = new System.Drawing.Point(216, 14);
            this.back11AchiveUp.Margin = new System.Windows.Forms.Padding(0);
            this.back11AchiveUp.Maximum = new decimal(new int[] {
            210,
            0,
            0,
            0});
            this.back11AchiveUp.Name = "back11AchiveUp";
            this.back11AchiveUp.Size = new System.Drawing.Size(48, 20);
            this.back11AchiveUp.TabIndex = 4;
            this.back11AchiveUp.Enter += new System.EventHandler(this.NumericUpDown_Enter);
            this.back11AchiveUp.MouseDown += new System.Windows.Forms.MouseEventHandler(this.NumericUpDown_MouseDown);
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.Location = new System.Drawing.Point(25, 16);
            this.label57.Margin = new System.Windows.Forms.Padding(0);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(34, 13);
            this.label57.TabIndex = 3;
            this.label57.Text = "Зліва";
            // 
            // back11AchiveLeft
            // 
            this.back11AchiveLeft.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.back11AchiveLeft.DecimalPlaces = 1;
            this.back11AchiveLeft.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.back11AchiveLeft.Location = new System.Drawing.Point(59, 14);
            this.back11AchiveLeft.Margin = new System.Windows.Forms.Padding(0);
            this.back11AchiveLeft.Maximum = new decimal(new int[] {
            210,
            0,
            0,
            0});
            this.back11AchiveLeft.Name = "back11AchiveLeft";
            this.back11AchiveLeft.Size = new System.Drawing.Size(48, 20);
            this.back11AchiveLeft.TabIndex = 2;
            this.back11AchiveLeft.Enter += new System.EventHandler(this.NumericUpDown_Enter);
            this.back11AchiveLeft.MouseDown += new System.Windows.Forms.MouseEventHandler(this.NumericUpDown_MouseDown);
            // 
            // groupBox16
            // 
            this.groupBox16.Controls.Add(this.label43);
            this.groupBox16.Controls.Add(this.backFacultFUp);
            this.groupBox16.Controls.Add(this.label44);
            this.groupBox16.Controls.Add(this.backFacultSUp);
            this.groupBox16.Controls.Add(this.label45);
            this.groupBox16.Controls.Add(this.backFacultLeftAll);
            this.groupBox16.Location = new System.Drawing.Point(3, 64);
            this.groupBox16.Name = "groupBox16";
            this.groupBox16.Size = new System.Drawing.Size(287, 60);
            this.groupBox16.TabIndex = 13;
            this.groupBox16.TabStop = false;
            this.groupBox16.Text = "Лінія факультативів";
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Location = new System.Drawing.Point(156, 18);
            this.label43.Margin = new System.Windows.Forms.Padding(0);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(80, 13);
            this.label43.TabIndex = 5;
            this.label43.Text = "Зверху(перша)";
            // 
            // backFacultFUp
            // 
            this.backFacultFUp.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.backFacultFUp.DecimalPlaces = 1;
            this.backFacultFUp.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.backFacultFUp.Location = new System.Drawing.Point(236, 16);
            this.backFacultFUp.Margin = new System.Windows.Forms.Padding(0);
            this.backFacultFUp.Maximum = new decimal(new int[] {
            210,
            0,
            0,
            0});
            this.backFacultFUp.Name = "backFacultFUp";
            this.backFacultFUp.Size = new System.Drawing.Size(48, 20);
            this.backFacultFUp.TabIndex = 4;
            this.backFacultFUp.Enter += new System.EventHandler(this.NumericUpDown_Enter);
            this.backFacultFUp.MouseDown += new System.Windows.Forms.MouseEventHandler(this.NumericUpDown_MouseDown);
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Location = new System.Drawing.Point(147, 39);
            this.label44.Margin = new System.Windows.Forms.Padding(0);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(89, 13);
            this.label44.TabIndex = 3;
            this.label44.Text = "Зверху(остання)";
            // 
            // backFacultSUp
            // 
            this.backFacultSUp.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.backFacultSUp.DecimalPlaces = 1;
            this.backFacultSUp.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.backFacultSUp.Location = new System.Drawing.Point(236, 37);
            this.backFacultSUp.Margin = new System.Windows.Forms.Padding(0);
            this.backFacultSUp.Maximum = new decimal(new int[] {
            210,
            0,
            0,
            0});
            this.backFacultSUp.Name = "backFacultSUp";
            this.backFacultSUp.Size = new System.Drawing.Size(48, 20);
            this.backFacultSUp.TabIndex = 2;
            this.backFacultSUp.Enter += new System.EventHandler(this.NumericUpDown_Enter);
            this.backFacultSUp.MouseDown += new System.Windows.Forms.MouseEventHandler(this.NumericUpDown_MouseDown);
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Location = new System.Drawing.Point(45, 18);
            this.label45.Margin = new System.Windows.Forms.Padding(0);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(54, 13);
            this.label45.TabIndex = 1;
            this.label45.Text = "Зліва(всі)";
            // 
            // backFacultLeftAll
            // 
            this.backFacultLeftAll.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.backFacultLeftAll.DecimalPlaces = 1;
            this.backFacultLeftAll.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.backFacultLeftAll.Location = new System.Drawing.Point(99, 16);
            this.backFacultLeftAll.Margin = new System.Windows.Forms.Padding(0);
            this.backFacultLeftAll.Maximum = new decimal(new int[] {
            210,
            0,
            0,
            0});
            this.backFacultLeftAll.Name = "backFacultLeftAll";
            this.backFacultLeftAll.Size = new System.Drawing.Size(48, 20);
            this.backFacultLeftAll.TabIndex = 0;
            this.backFacultLeftAll.Enter += new System.EventHandler(this.NumericUpDown_Enter);
            this.backFacultLeftAll.MouseDown += new System.Windows.Forms.MouseEventHandler(this.NumericUpDown_MouseDown);
            // 
            // groupBox17
            // 
            this.groupBox17.Controls.Add(this.label49);
            this.groupBox17.Controls.Add(this.groupBox13);
            this.groupBox17.Controls.Add(this.backDateAllUp);
            this.groupBox17.Location = new System.Drawing.Point(6, 275);
            this.groupBox17.Name = "groupBox17";
            this.groupBox17.Size = new System.Drawing.Size(290, 105);
            this.groupBox17.TabIndex = 14;
            this.groupBox17.TabStop = false;
            this.groupBox17.Text = "Лінія дати видачі";
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Location = new System.Drawing.Point(93, 18);
            this.label49.Margin = new System.Windows.Forms.Padding(0);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(62, 13);
            this.label49.TabIndex = 1;
            this.label49.Text = "Зверху(всі)";
            // 
            // groupBox13
            // 
            this.groupBox13.Controls.Add(this.label46);
            this.groupBox13.Controls.Add(this.backDateMonth);
            this.groupBox13.Controls.Add(this.label37);
            this.groupBox13.Controls.Add(this.backDateYear);
            this.groupBox13.Controls.Add(this.label38);
            this.groupBox13.Controls.Add(this.backDateNum);
            this.groupBox13.Location = new System.Drawing.Point(2, 42);
            this.groupBox13.Name = "groupBox13";
            this.groupBox13.Size = new System.Drawing.Size(286, 42);
            this.groupBox13.TabIndex = 17;
            this.groupBox13.TabStop = false;
            this.groupBox13.Text = "Зліва лінії";
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Location = new System.Drawing.Point(106, 16);
            this.label46.Margin = new System.Windows.Forms.Padding(0);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(41, 13);
            this.label46.TabIndex = 7;
            this.label46.Text = "місяця";
            // 
            // backDateMonth
            // 
            this.backDateMonth.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.backDateMonth.DecimalPlaces = 1;
            this.backDateMonth.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.backDateMonth.Location = new System.Drawing.Point(147, 14);
            this.backDateMonth.Margin = new System.Windows.Forms.Padding(0);
            this.backDateMonth.Maximum = new decimal(new int[] {
            210,
            0,
            0,
            0});
            this.backDateMonth.Name = "backDateMonth";
            this.backDateMonth.Size = new System.Drawing.Size(48, 20);
            this.backDateMonth.TabIndex = 6;
            this.backDateMonth.Enter += new System.EventHandler(this.NumericUpDown_Enter);
            this.backDateMonth.MouseDown += new System.Windows.Forms.MouseEventHandler(this.NumericUpDown_MouseDown);
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(205, 16);
            this.label37.Margin = new System.Windows.Forms.Padding(0);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(30, 13);
            this.label37.TabIndex = 5;
            this.label37.Text = "року";
            // 
            // backDateYear
            // 
            this.backDateYear.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.backDateYear.DecimalPlaces = 1;
            this.backDateYear.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.backDateYear.Location = new System.Drawing.Point(236, 14);
            this.backDateYear.Margin = new System.Windows.Forms.Padding(0);
            this.backDateYear.Maximum = new decimal(new int[] {
            210,
            0,
            0,
            0});
            this.backDateYear.Name = "backDateYear";
            this.backDateYear.Size = new System.Drawing.Size(48, 20);
            this.backDateYear.TabIndex = 4;
            this.backDateYear.Enter += new System.EventHandler(this.NumericUpDown_Enter);
            this.backDateYear.MouseDown += new System.Windows.Forms.MouseEventHandler(this.NumericUpDown_MouseDown);
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(8, 16);
            this.label38.Margin = new System.Windows.Forms.Padding(0);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(36, 13);
            this.label38.TabIndex = 3;
            this.label38.Text = "числа";
            // 
            // backDateNum
            // 
            this.backDateNum.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.backDateNum.DecimalPlaces = 1;
            this.backDateNum.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.backDateNum.Location = new System.Drawing.Point(44, 14);
            this.backDateNum.Margin = new System.Windows.Forms.Padding(0);
            this.backDateNum.Maximum = new decimal(new int[] {
            210,
            0,
            0,
            0});
            this.backDateNum.Name = "backDateNum";
            this.backDateNum.Size = new System.Drawing.Size(48, 20);
            this.backDateNum.TabIndex = 2;
            this.backDateNum.Enter += new System.EventHandler(this.NumericUpDown_Enter);
            this.backDateNum.MouseDown += new System.Windows.Forms.MouseEventHandler(this.NumericUpDown_MouseDown);
            // 
            // backDateAllUp
            // 
            this.backDateAllUp.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.backDateAllUp.DecimalPlaces = 1;
            this.backDateAllUp.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.backDateAllUp.Location = new System.Drawing.Point(155, 16);
            this.backDateAllUp.Margin = new System.Windows.Forms.Padding(0);
            this.backDateAllUp.Maximum = new decimal(new int[] {
            210,
            0,
            0,
            0});
            this.backDateAllUp.Name = "backDateAllUp";
            this.backDateAllUp.Size = new System.Drawing.Size(48, 20);
            this.backDateAllUp.TabIndex = 0;
            this.backDateAllUp.Enter += new System.EventHandler(this.NumericUpDown_Enter);
            this.backDateAllUp.MouseDown += new System.Windows.Forms.MouseEventHandler(this.NumericUpDown_MouseDown);
            // 
            // tabPage7
            // 
            this.tabPage7.Controls.Add(this.groupBox32);
            this.tabPage7.Controls.Add(this.groupBox30);
            this.tabPage7.Controls.Add(this.groupBox31);
            this.tabPage7.Controls.Add(this.groupBox29);
            this.tabPage7.Controls.Add(this.groupBox28);
            this.tabPage7.Controls.Add(this.groupBox26);
            this.tabPage7.Location = new System.Drawing.Point(4, 22);
            this.tabPage7.Name = "tabPage7";
            this.tabPage7.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage7.Size = new System.Drawing.Size(632, 394);
            this.tabPage7.TabIndex = 2;
            this.tabPage7.Text = "Шрифти";
            this.tabPage7.UseVisualStyleBackColor = true;
            // 
            // groupBox32
            // 
            this.groupBox32.Controls.Add(this.selected6);
            this.groupBox32.Controls.Add(this.btn6);
            this.groupBox32.Location = new System.Drawing.Point(5, 330);
            this.groupBox32.Name = "groupBox32";
            this.groupBox32.Size = new System.Drawing.Size(620, 60);
            this.groupBox32.TabIndex = 10;
            this.groupBox32.TabStop = false;
            // 
            // selected6
            // 
            this.selected6.AutoSize = true;
            this.selected6.Location = new System.Drawing.Point(298, 16);
            this.selected6.Name = "selected6";
            this.selected6.Size = new System.Drawing.Size(92, 13);
            this.selected6.TabIndex = 6;
            this.selected6.Text = "Вибраний шрифт";
            // 
            // btn6
            // 
            this.btn6.Location = new System.Drawing.Point(6, 18);
            this.btn6.Name = "btn6";
            this.btn6.Size = new System.Drawing.Size(262, 32);
            this.btn6.TabIndex = 5;
            this.btn6.Text = "Факультативні предмети...";
            this.btn6.UseVisualStyleBackColor = true;
            this.btn6.Click += new System.EventHandler(this.Button_Click);
            // 
            // groupBox30
            // 
            this.groupBox30.Controls.Add(this.selected5);
            this.groupBox30.Controls.Add(this.btn5);
            this.groupBox30.Location = new System.Drawing.Point(5, 268);
            this.groupBox30.Name = "groupBox30";
            this.groupBox30.Size = new System.Drawing.Size(620, 60);
            this.groupBox30.TabIndex = 9;
            this.groupBox30.TabStop = false;
            // 
            // selected5
            // 
            this.selected5.AutoSize = true;
            this.selected5.Location = new System.Drawing.Point(298, 16);
            this.selected5.Name = "selected5";
            this.selected5.Size = new System.Drawing.Size(92, 13);
            this.selected5.TabIndex = 5;
            this.selected5.Text = "Вибраний шрифт";
            // 
            // btn5
            // 
            this.btn5.Location = new System.Drawing.Point(6, 18);
            this.btn5.Name = "btn5";
            this.btn5.Size = new System.Drawing.Size(262, 32);
            this.btn5.TabIndex = 3;
            this.btn5.Text = "Задня сторінка...";
            this.btn5.UseVisualStyleBackColor = true;
            this.btn5.Click += new System.EventHandler(this.Button_Click);
            // 
            // groupBox31
            // 
            this.groupBox31.Controls.Add(this.selected4);
            this.groupBox31.Controls.Add(this.btn4);
            this.groupBox31.Location = new System.Drawing.Point(6, 206);
            this.groupBox31.Name = "groupBox31";
            this.groupBox31.Size = new System.Drawing.Size(620, 60);
            this.groupBox31.TabIndex = 9;
            this.groupBox31.TabStop = false;
            // 
            // selected4
            // 
            this.selected4.AutoSize = true;
            this.selected4.Location = new System.Drawing.Point(297, 16);
            this.selected4.Name = "selected4";
            this.selected4.Size = new System.Drawing.Size(92, 13);
            this.selected4.TabIndex = 4;
            this.selected4.Text = "Вибраний шрифт";
            // 
            // btn4
            // 
            this.btn4.Location = new System.Drawing.Point(5, 18);
            this.btn4.Name = "btn4";
            this.btn4.Size = new System.Drawing.Size(262, 32);
            this.btn4.TabIndex = 4;
            this.btn4.Text = "Предмети та оцінки...";
            this.btn4.UseVisualStyleBackColor = true;
            this.btn4.Click += new System.EventHandler(this.Button_Click);
            // 
            // groupBox29
            // 
            this.groupBox29.Controls.Add(this.selected3);
            this.groupBox29.Controls.Add(this.btn3);
            this.groupBox29.Location = new System.Drawing.Point(7, 140);
            this.groupBox29.Name = "groupBox29";
            this.groupBox29.Size = new System.Drawing.Size(620, 60);
            this.groupBox29.TabIndex = 8;
            this.groupBox29.TabStop = false;
            // 
            // selected3
            // 
            this.selected3.AutoSize = true;
            this.selected3.Location = new System.Drawing.Point(296, 16);
            this.selected3.Name = "selected3";
            this.selected3.Size = new System.Drawing.Size(92, 13);
            this.selected3.TabIndex = 3;
            this.selected3.Text = "Вибраний шрифт";
            // 
            // btn3
            // 
            this.btn3.Location = new System.Drawing.Point(6, 18);
            this.btn3.Name = "btn3";
            this.btn3.Size = new System.Drawing.Size(262, 32);
            this.btn3.TabIndex = 2;
            this.btn3.Text = "Назва навчального закладу...";
            this.btn3.UseVisualStyleBackColor = true;
            this.btn3.Click += new System.EventHandler(this.Button_Click);
            // 
            // groupBox28
            // 
            this.groupBox28.Controls.Add(this.selected2);
            this.groupBox28.Controls.Add(this.btn2);
            this.groupBox28.Location = new System.Drawing.Point(7, 74);
            this.groupBox28.Name = "groupBox28";
            this.groupBox28.Size = new System.Drawing.Size(620, 60);
            this.groupBox28.TabIndex = 7;
            this.groupBox28.TabStop = false;
            // 
            // selected2
            // 
            this.selected2.AutoSize = true;
            this.selected2.Location = new System.Drawing.Point(296, 16);
            this.selected2.Name = "selected2";
            this.selected2.Size = new System.Drawing.Size(92, 13);
            this.selected2.TabIndex = 2;
            this.selected2.Text = "Вибраний шрифт";
            // 
            // btn2
            // 
            this.btn2.Location = new System.Drawing.Point(6, 18);
            this.btn2.Name = "btn2";
            this.btn2.Size = new System.Drawing.Size(262, 32);
            this.btn2.TabIndex = 1;
            this.btn2.Text = "Прізвище, ім\'я та по батькові...";
            this.btn2.UseVisualStyleBackColor = true;
            this.btn2.Click += new System.EventHandler(this.Button_Click);
            // 
            // groupBox26
            // 
            this.groupBox26.Controls.Add(this.selected1);
            this.groupBox26.Controls.Add(this.btn1);
            this.groupBox26.Location = new System.Drawing.Point(6, 6);
            this.groupBox26.Name = "groupBox26";
            this.groupBox26.Size = new System.Drawing.Size(620, 60);
            this.groupBox26.TabIndex = 6;
            this.groupBox26.TabStop = false;
            // 
            // selected1
            // 
            this.selected1.AutoSize = true;
            this.selected1.Location = new System.Drawing.Point(297, 16);
            this.selected1.Name = "selected1";
            this.selected1.Size = new System.Drawing.Size(92, 13);
            this.selected1.TabIndex = 1;
            this.selected1.Text = "Вибраний шрифт";
            // 
            // btn1
            // 
            this.btn1.Location = new System.Drawing.Point(6, 18);
            this.btn1.Name = "btn1";
            this.btn1.Size = new System.Drawing.Size(262, 32);
            this.btn1.TabIndex = 0;
            this.btn1.Text = "Серія та номер документа...";
            this.btn1.UseVisualStyleBackColor = true;
            this.btn1.Click += new System.EventHandler(this.Button_Click);
            // 
            // textBox1
            // 
            this.textBox1.BackColor = System.Drawing.SystemColors.Control;
            this.textBox1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox1.Cursor = System.Windows.Forms.Cursors.Default;
            this.textBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox1.ForeColor = System.Drawing.Color.Firebrick;
            this.textBox1.Location = new System.Drawing.Point(16, 12);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.ReadOnly = true;
            this.textBox1.Size = new System.Drawing.Size(374, 44);
            this.textBox1.TabIndex = 2;
            this.textBox1.Text = "Всі відстані вимірюються в мм \r\nвід лівого верхнього країв паперу до лінії";
            this.textBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBox1.Enter += new System.EventHandler(this.TextBox1_Enter);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(426, 15);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(94, 15);
            this.label2.TabIndex = 4;
            this.label2.Text = "Висота бланку:";
            // 
            // textBox2
            // 
            this.textBox2.BackColor = System.Drawing.SystemColors.Control;
            this.textBox2.Location = new System.Drawing.Point(429, 36);
            this.textBox2.Name = "textBox2";
            this.textBox2.ReadOnly = true;
            this.textBox2.Size = new System.Drawing.Size(100, 20);
            this.textBox2.TabIndex = 5;
            this.textBox2.Text = "148";
            // 
            // textBox3
            // 
            this.textBox3.BackColor = System.Drawing.SystemColors.Control;
            this.textBox3.Location = new System.Drawing.Point(548, 36);
            this.textBox3.Name = "textBox3";
            this.textBox3.ReadOnly = true;
            this.textBox3.Size = new System.Drawing.Size(100, 20);
            this.textBox3.TabIndex = 6;
            this.textBox3.Text = "210";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.Location = new System.Drawing.Point(545, 15);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(98, 15);
            this.label3.TabIndex = 7;
            this.label3.Text = "Ширина бланку:";
            // 
            // saveAs
            // 
            this.saveAs.Location = new System.Drawing.Point(93, 488);
            this.saveAs.Name = "saveAs";
            this.saveAs.Size = new System.Drawing.Size(89, 23);
            this.saveAs.TabIndex = 8;
            this.saveAs.Text = "Зберегти як...";
            this.saveAs.UseVisualStyleBackColor = true;
            this.saveAs.Click += new System.EventHandler(this.SaveAs_Click);
            // 
            // open
            // 
            this.open.Location = new System.Drawing.Point(12, 488);
            this.open.Name = "open";
            this.open.Size = new System.Drawing.Size(75, 23);
            this.open.TabIndex = 9;
            this.open.Text = "Відкрити...";
            this.open.UseVisualStyleBackColor = true;
            this.open.Click += new System.EventHandler(this.Open_Click);
            // 
            // apply
            // 
            this.apply.Location = new System.Drawing.Point(568, 488);
            this.apply.Name = "apply";
            this.apply.Size = new System.Drawing.Size(84, 23);
            this.apply.TabIndex = 10;
            this.apply.Text = "Застосувати";
            this.apply.UseVisualStyleBackColor = true;
            this.apply.Click += new System.EventHandler(this.Apply_Click);
            // 
            // cancel
            // 
            this.cancel.Location = new System.Drawing.Point(487, 488);
            this.cancel.Name = "cancel";
            this.cancel.Size = new System.Drawing.Size(75, 23);
            this.cancel.TabIndex = 11;
            this.cancel.Text = "Скасувати";
            this.cancel.UseVisualStyleBackColor = true;
            this.cancel.Click += new System.EventHandler(this.Cancel_Click);
            // 
            // OK
            // 
            this.OK.Location = new System.Drawing.Point(406, 488);
            this.OK.Name = "OK";
            this.OK.Size = new System.Drawing.Size(75, 23);
            this.OK.TabIndex = 12;
            this.OK.Text = "ОК";
            this.OK.UseVisualStyleBackColor = true;
            this.OK.Click += new System.EventHandler(this.OK_Click);
            // 
            // byDefault
            // 
            this.byDefault.Location = new System.Drawing.Point(209, 488);
            this.byDefault.Name = "byDefault";
            this.byDefault.Size = new System.Drawing.Size(172, 23);
            this.byDefault.TabIndex = 13;
            this.byDefault.Text = "Встановити за замовчуванням";
            this.byDefault.UseVisualStyleBackColor = true;
            this.byDefault.Click += new System.EventHandler(this.ByDefault_Click);
            // 
            // SettingsModel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(664, 521);
            this.Controls.Add(this.byDefault);
            this.Controls.Add(this.OK);
            this.Controls.Add(this.cancel);
            this.Controls.Add(this.apply);
            this.Controls.Add(this.open);
            this.Controls.Add(this.saveAs);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.textBox3);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.tabControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "SettingsModel";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Налаштування бланку";
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.groupBox9.ResumeLayout(false);
            this.groupBox9.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.WidthOfLine)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DividingLineSize)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DividingLineUp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.frontSR_FLDPA)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.frontSR_LLine)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.frontSR_LRate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.frontSR_LSub)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.frontSR_FLine)).EndInit();
            this.groupBox8.ResumeLayout(false);
            this.groupBox8.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.frontEduUp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.frontEduLeft)).EndInit();
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.frontSchoolEUp)).EndInit();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.frontSchoolSUp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.frontSchoolFUp)).EndInit();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.sizeOfLine)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.frontNamesFUp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.frontNamesSUp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.frontNamesLBoth)).EndInit();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.sizeOfLineRate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sizeOfLineSub)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.frontSL_LLine)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.frontSL_LRate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.frontSL_LSub)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.frontSL_FLine)).EndInit();
            this.tabControl2.ResumeLayout(false);
            this.tabPage3.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.front9YearUp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.front9YearLeft)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.front9FinishUp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.front9FinishLeft)).EndInit();
            this.tabPage4.ResumeLayout(false);
            this.groupBox10.ResumeLayout(false);
            this.groupBox10.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.front11YearUp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.front11YearLeft)).EndInit();
            this.groupBox11.ResumeLayout(false);
            this.groupBox11.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.front11FinishUp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.front11FinishLeft)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.frontNumberLeft)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.frontSeriesLeft)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.frontSerNum)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.groupBox15.ResumeLayout(false);
            this.groupBox15.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.backLearnUp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.backLearnLeft)).EndInit();
            this.tabControl3.ResumeLayout(false);
            this.tabPage5.ResumeLayout(false);
            this.groupBox18.ResumeLayout(false);
            this.groupBox18.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.back9RegNUp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.back9RegNLeft)).EndInit();
            this.groupBox14.ResumeLayout(false);
            this.groupBox14.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.back9DirectorUp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.back9DirectorLeft)).EndInit();
            this.groupBox12.ResumeLayout(false);
            this.groupBox12.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.back9AwardShortLeft)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.back9AwardBothUp)).EndInit();
            this.groupBox19.ResumeLayout(false);
            this.groupBox19.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.back9GetUp_LLow)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.back9GetUp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.back9GetLeft)).EndInit();
            this.tabPage6.ResumeLayout(false);
            this.groupBox24.ResumeLayout(false);
            this.groupBox24.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.back11DirectorUp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.back11DirectorLeft)).EndInit();
            this.groupBox27.ResumeLayout(false);
            this.groupBox27.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.back11RegNUp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.back11RegNLeft)).EndInit();
            this.groupBox22.ResumeLayout(false);
            this.groupBox22.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.back11LearnUp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.back11LearnLeft)).EndInit();
            this.groupBox25.ResumeLayout(false);
            this.groupBox25.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.back11DiplomUp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.back11DiplomLeft)).EndInit();
            this.groupBox20.ResumeLayout(false);
            this.groupBox20.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.back11AwardedUp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.back11AwardedLeft)).EndInit();
            this.groupBox23.ResumeLayout(false);
            this.groupBox23.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.back11MedalUp)).EndInit();
            this.groupBox21.ResumeLayout(false);
            this.groupBox21.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.back11AchiveUp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.back11AchiveLeft)).EndInit();
            this.groupBox16.ResumeLayout(false);
            this.groupBox16.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.backFacultFUp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.backFacultSUp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.backFacultLeftAll)).EndInit();
            this.groupBox17.ResumeLayout(false);
            this.groupBox17.PerformLayout();
            this.groupBox13.ResumeLayout(false);
            this.groupBox13.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.backDateMonth)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.backDateYear)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.backDateNum)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.backDateAllUp)).EndInit();
            this.tabPage7.ResumeLayout(false);
            this.groupBox32.ResumeLayout(false);
            this.groupBox32.PerformLayout();
            this.groupBox30.ResumeLayout(false);
            this.groupBox30.PerformLayout();
            this.groupBox31.ResumeLayout(false);
            this.groupBox31.PerformLayout();
            this.groupBox29.ResumeLayout(false);
            this.groupBox29.PerformLayout();
            this.groupBox28.ResumeLayout(false);
            this.groupBox28.PerformLayout();
            this.groupBox26.ResumeLayout(false);
            this.groupBox26.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.NumericUpDown frontSR_FLDPA;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.NumericUpDown frontSR_LLine;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.NumericUpDown frontSR_LRate;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.NumericUpDown frontSR_LSub;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.NumericUpDown frontSR_FLine;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.NumericUpDown frontEduUp;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.NumericUpDown frontEduLeft;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.NumericUpDown frontSchoolEUp;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.NumericUpDown frontSchoolSUp;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.NumericUpDown frontSchoolFUp;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.NumericUpDown frontNamesFUp;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.NumericUpDown frontNamesSUp;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.NumericUpDown frontNamesLBoth;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.NumericUpDown frontSL_LLine;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.NumericUpDown frontSL_LRate;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.NumericUpDown frontSL_LSub;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.NumericUpDown frontSL_FLine;
        private System.Windows.Forms.TabControl tabControl2;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.NumericUpDown front9YearUp;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.NumericUpDown front9YearLeft;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.NumericUpDown front9FinishUp;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.NumericUpDown front9FinishLeft;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.GroupBox groupBox10;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.NumericUpDown front11YearUp;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.NumericUpDown front11YearLeft;
        private System.Windows.Forms.GroupBox groupBox11;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.NumericUpDown front11FinishUp;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.NumericUpDown front11FinishLeft;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.NumericUpDown frontNumberLeft;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.NumericUpDown frontSeriesLeft;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown frontSerNum;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.GroupBox groupBox15;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.NumericUpDown backLearnUp;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.NumericUpDown backLearnLeft;
        private System.Windows.Forms.TabControl tabControl3;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.GroupBox groupBox18;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.NumericUpDown back9RegNUp;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.NumericUpDown back9RegNLeft;
        private System.Windows.Forms.GroupBox groupBox14;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.NumericUpDown back9DirectorUp;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.NumericUpDown back9DirectorLeft;
        private System.Windows.Forms.GroupBox groupBox12;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.NumericUpDown back9AwardShortLeft;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.NumericUpDown back9AwardBothUp;
        private System.Windows.Forms.GroupBox groupBox19;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.NumericUpDown back9GetUp_LLow;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.NumericUpDown back9GetUp;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.NumericUpDown back9GetLeft;
        private System.Windows.Forms.TabPage tabPage6;
        private System.Windows.Forms.GroupBox groupBox24;
        private System.Windows.Forms.Label label59;
        private System.Windows.Forms.NumericUpDown back11DirectorUp;
        private System.Windows.Forms.Label label60;
        private System.Windows.Forms.NumericUpDown back11DirectorLeft;
        private System.Windows.Forms.GroupBox groupBox27;
        private System.Windows.Forms.Label label65;
        private System.Windows.Forms.NumericUpDown back11RegNUp;
        private System.Windows.Forms.Label label66;
        private System.Windows.Forms.NumericUpDown back11RegNLeft;
        private System.Windows.Forms.GroupBox groupBox22;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.NumericUpDown back11LearnUp;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.NumericUpDown back11LearnLeft;
        private System.Windows.Forms.GroupBox groupBox25;
        private System.Windows.Forms.Label label61;
        private System.Windows.Forms.NumericUpDown back11DiplomUp;
        private System.Windows.Forms.Label label62;
        private System.Windows.Forms.NumericUpDown back11DiplomLeft;
        private System.Windows.Forms.GroupBox groupBox20;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.NumericUpDown back11AwardedUp;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.NumericUpDown back11AwardedLeft;
        private System.Windows.Forms.GroupBox groupBox23;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.NumericUpDown back11MedalUp;
        private System.Windows.Forms.GroupBox groupBox21;
        private System.Windows.Forms.Label label56;
        private System.Windows.Forms.NumericUpDown back11AchiveUp;
        private System.Windows.Forms.Label label57;
        private System.Windows.Forms.NumericUpDown back11AchiveLeft;
        private System.Windows.Forms.GroupBox groupBox16;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.NumericUpDown backFacultFUp;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.NumericUpDown backFacultSUp;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.NumericUpDown backFacultLeftAll;
        private System.Windows.Forms.GroupBox groupBox17;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.GroupBox groupBox13;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.NumericUpDown backDateMonth;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.NumericUpDown backDateYear;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.NumericUpDown backDateNum;
        private System.Windows.Forms.NumericUpDown backDateAllUp;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button saveAs;
        private System.Windows.Forms.Button open;
        private System.Windows.Forms.Button apply;
        private System.Windows.Forms.Button cancel;
        private System.Windows.Forms.Button OK;
        private System.Windows.Forms.CheckBox extraRightLine;
        private System.Windows.Forms.CheckBox extraLeftLine;
        private System.Windows.Forms.Button byDefault;
        private System.Windows.Forms.NumericUpDown sizeOfLine;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.NumericUpDown sizeOfLineSub;
        private System.Windows.Forms.Label label63;
        private System.Windows.Forms.Label label58;
        private System.Windows.Forms.NumericUpDown sizeOfLineRate;
        private System.Windows.Forms.CheckBox DividingLine;
        private System.Windows.Forms.NumericUpDown DividingLineUp;
        private System.Windows.Forms.Label DividingLabel;
        private System.Windows.Forms.NumericUpDown DividingLineSize;
        private System.Windows.Forms.Label DividingLabel2;
        private System.Windows.Forms.NumericUpDown WidthOfLine;
        private System.Windows.Forms.Label DividingLabel3;
        private System.Windows.Forms.TabPage tabPage7;
        private System.Windows.Forms.Button btn1;
        private System.Windows.Forms.Button btn6;
        private System.Windows.Forms.Button btn4;
        private System.Windows.Forms.Button btn5;
        private System.Windows.Forms.Button btn3;
        private System.Windows.Forms.Button btn2;
        private System.Windows.Forms.GroupBox groupBox26;
        private System.Windows.Forms.GroupBox groupBox32;
        private System.Windows.Forms.GroupBox groupBox31;
        private System.Windows.Forms.GroupBox groupBox30;
        private System.Windows.Forms.GroupBox groupBox29;
        private System.Windows.Forms.GroupBox groupBox28;
        private System.Windows.Forms.Label selected6;
        private System.Windows.Forms.Label selected5;
        private System.Windows.Forms.Label selected4;
        private System.Windows.Forms.Label selected3;
        private System.Windows.Forms.Label selected2;
        private System.Windows.Forms.Label selected1;
    }
}