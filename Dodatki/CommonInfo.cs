﻿using System;
using System.Windows.Forms;

namespace Dodatki
{
    public partial class CommonInfo : Form
    {
        public CommonInfo()
        {
            InitializeComponent();
            if ( Pupil.date != null )
            {
                date.Value = Pupil.date;
            }
            if (Pupil.year != null)
            {
                year.Text = Pupil.year;
            }
            if (Pupil.director != null)
            {
                director.Text = Pupil.director;
            }
            if (Pupil.firstSt != null)
            {
                firstSt.Text = Pupil.firstSt;
            }
            if (Pupil.secondSt != null)
            {
                secondSt.Text = Pupil.secondSt;
            }
            if (Pupil.thirdSt != null)
            {
                thirdSt.Text = Pupil.thirdSt;
            }
        }
        private void OK_Click(object sender, EventArgs e)
        {
            try
            {
                if ( date.Value == null || year == null || director == null 
                    || firstSt == null || secondSt == null )
                {
                    throw new Exception("Заповніть всі поля!");
                }
                if (year.Text.Length < 4)
                {
                    throw new Exception("Не правильний формат дати!");
                }
                Pupil.date = date.Value;
                Pupil.year = year.Text;
                Pupil.director = director.Text;
                Pupil.firstSt = firstSt.Text;
                Pupil.secondSt = secondSt.Text;
                Pupil.thirdSt = thirdSt.Text;
                DialogResult = DialogResult.OK;
            }
            catch (Exception ex)
            {
				ErrorMessage.LogError(ex);
                MessageBox.Show(ex.Message, "Помилка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void Cancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }
        private void Date_ValueChanged(object sender, EventArgs e)
        {
            DateTime dt = ((DateTimePicker)sender).Value;
            year.Text = dt.Year.ToString();
        }
    }
}
